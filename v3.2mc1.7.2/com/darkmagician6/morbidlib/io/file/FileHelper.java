package com.darkmagician6.morbidlib.io.file;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public final class FileHelper {

	public static List<String> read(final File inputFile) {
		final List<String> fileContentList = new ArrayList<String>();
		FileReader fileReader = null;
		BufferedReader bufferedFileReader = null;
		
		try {
			fileReader = new FileReader(inputFile);
			bufferedFileReader = new BufferedReader(fileReader);
			
			String currentReadLine;
			
			while ((currentReadLine = bufferedFileReader.readLine()) != null) {
				fileContentList.add(currentReadLine);
			}		
		} catch (FileNotFoundException e) {
			System.err.println("FileNotFoundException thrown, make sure the file exists.");
		} catch (IOException e) {
			System.err.println("IOException thrown, can't read the file's content.");
		} finally {
			try {
				if (bufferedFileReader != null) {
					bufferedFileReader.close();
				}
				if (fileReader != null) {
					fileReader.close();
				}
			} catch (IOException e) {
			}
		}
		
		return fileContentList;
	}
	
	public static String write(final File outputFile, final List<String> writeContent, boolean override) {
		BufferedWriter bufferedFileWriter = null;
		FileWriter fileWriter = null;
		String message = "";
		
		try {
			fileWriter = new FileWriter(outputFile, !override);
			bufferedFileWriter = new BufferedWriter(fileWriter);
			
			for (final String outputLine : writeContent) {
				bufferedFileWriter.write(outputLine);
				bufferedFileWriter.flush();
				bufferedFileWriter.newLine();
			}
			
			message = "Completed writing to the file.";
		} catch (IOException e) {
			message = "IOException thrown while attempting to write.";
		} finally {
			try {
				if (bufferedFileWriter != null) {
					bufferedFileWriter.close();
				}
				if (fileWriter != null) {
					fileWriter.close();
				}				
			} catch (IOException e) {
				message = "IOException thrown while attemping to close the writer.";
			}
		}
		
		return message;
	}
	
	public static boolean ensureExistance(File targetFile) {
		if (!targetFile.exists()) {
			try {
				targetFile.createNewFile();
			} catch (IOException e) {
				System.err.println("IOException thrown, can't create file.");
			}
		}
		
		return targetFile.exists();
	}

}
