package com.darkmagician6.morbidlib.io.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

public final class URLReader {
	
	public static List<String> read(String inputAdress) {
		final List<String> inputLineList = new LinkedList<String>();
		InputStreamReader addressStreamReader = null;
		BufferedReader bufferedAddressReader = null;
		
		try {
			final URL addressURL = new URL(inputAdress);
			addressStreamReader = new InputStreamReader(addressURL.openStream());
			bufferedAddressReader = new BufferedReader(addressStreamReader);
			
			String currentReadLine;
			
			while ((currentReadLine = bufferedAddressReader.readLine()) != null) {
				inputLineList.add(currentReadLine);
			}
		} catch (MalformedURLException e) {
			System.err.println("MalformedURLException thrown, invalid URL adress.");
		} catch (IOException e) {
			System.err.println("IOException thrown, can't read URL adress's content.");
		} finally {
			try {
				if (bufferedAddressReader != null) {
					bufferedAddressReader.close();
				}
				if (addressStreamReader != null) {
					addressStreamReader.close();
				}
			} catch (IOException e) {
			}
		}
				
		return inputLineList;
	}

}
