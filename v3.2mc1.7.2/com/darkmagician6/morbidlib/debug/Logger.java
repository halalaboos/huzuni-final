package com.darkmagician6.morbidlib.debug;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.darkmagician6.morbidlib.io.file.FileHelper;

public final class Logger {
	private final File logFile;
	private final List<String> logList;
	
	private final boolean saveEveryCall;
	
	public Logger() {
		logFile = null;
		logList = null;
		saveEveryCall = false;
	}
	
	public Logger(final File logFile, boolean saveEveryCall) {
		this.logFile = logFile;
		this.saveEveryCall = saveEveryCall;
		logList = new ArrayList<String>();
		
		FileHelper.ensureExistance(logFile);
	}
	
	public String log(String logMessage) {
		final Date currentDate = new Date();
		
		@SuppressWarnings("deprecation")
		String formattedMessage = String.format("[%s:%s:%s] %s", currentDate.getHours(), currentDate.getMinutes(), currentDate.getSeconds(), logMessage);
		
		System.out.println(formattedMessage);
		
		if (logFile != null && logList != null) {
			logList.add(formattedMessage);
			
			if (saveEveryCall) {
				saveLogs();
			}
		}
		
		return formattedMessage;
	}
	
	public void saveLogs() {
		if (logFile != null && logList != null) {
			FileHelper.write(logFile, logList, false);
			logList.clear();
		} else {
			System.err.println("Logging to file is disabled.");
		}
	}

}
