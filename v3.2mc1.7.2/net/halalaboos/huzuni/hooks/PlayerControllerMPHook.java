package net.halalaboos.huzuni.hooks;

import com.darkmagician6.eventapi.Dispatcher;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.events.game.EventAttackEntity;
import net.halalaboos.huzuni.events.game.EventBreakBlock;
import net.halalaboos.huzuni.events.game.EventClickBlock;
import net.halalaboos.huzuni.events.game.EventKillEntity;
import net.halalaboos.huzuni.events.game.EventPlaceBlock;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.PlayerControllerMP;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.StatFileWriter;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import net.minecraft.world.WorldSettings;

public class PlayerControllerMPHook extends PlayerControllerMP {
    private Minecraft mc;
    private NetHandlerPlayClient netClientHandler;

    public PlayerControllerMPHook(Minecraft minecraft,
    		NetHandlerPlayClient netClientHandler) {
        super(minecraft, netClientHandler);
        this.mc = minecraft;
        this.netClientHandler = netClientHandler;
        Huzuni.logger.log("Initialized new " + this.getClass().getSimpleName() + ".");
    }

    @Override
    public void onPlayerDamageBlock(int x, int y, int z, int side) {
    	boolean shouldReplaceHitDelay = false;
        EventBreakBlock event = new EventBreakBlock(x, y, z, side, curBlockDamageMP, blockHitDelay);
        Dispatcher.call(event);
        if (blockHitDelay != event.getBlockHitDelay()) {
        	blockHitDelay = event.getBlockHitDelay();
        	shouldReplaceHitDelay = true;
        }
        this.curBlockDamageMP = event.getCurrentBlockDamage();
        super.onPlayerDamageBlock(x, y, z, side);
        if (shouldReplaceHitDelay)
        	blockHitDelay = event.getBlockHitDelay();
    }

    @Override
    public void clickBlock(int x, int y, int z, int side) {
        Dispatcher.call(new EventClickBlock(x, y, z, side));
        super.clickBlock(x, y, z, side);
    }

    @Override
    public void attackEntity(EntityPlayer player, Entity entity) {
        EventAttackEntity event = new EventAttackEntity(entity);
        Dispatcher.call(event);
        if (event.isCancelled())
            return;
        super.attackEntity(player, entity);
        if (entity.isDead)
            Dispatcher.call(new EventKillEntity(entity));
    }
    @Override
    public boolean onPlayerRightClick(EntityPlayer player, World world, ItemStack itemStack, int x, int y, int z, int side, Vec3 offset) {
    	EventPlaceBlock event = new EventPlaceBlock(itemStack, x, y, z, side, offset);
    	Dispatcher.call(event);
        if (event.isCancelled())
            return false;
        else
        	return super.onPlayerRightClick(player, world, itemStack, x, y, z, side, offset);
    }
    
	@Override
	public EntityClientPlayerMPHook func_147493_a(World world, StatFileWriter statFileWriter) {
		return new EntityClientPlayerMPHook(this.mc, world,
				this.mc.getSession(), this.netClientHandler, statFileWriter);
	}

}
