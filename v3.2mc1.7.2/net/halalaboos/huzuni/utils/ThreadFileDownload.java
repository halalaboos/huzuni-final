package net.halalaboos.huzuni.utils;

import org.lwjgl.Sys;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Downloads a file specified once ran.
 */
public class ThreadFileDownload extends Thread {
    private final File outputFile;
    private final URL fileURL;
    private float percentage;

    /**
     * @param fileURL
     *         URL to the file.
     * @param outputFile
     *         File the bytes will be stored to.
     */
    public ThreadFileDownload(URL fileURL, File outputFile) {
        this.fileURL = fileURL;
        this.outputFile = outputFile;
    }

    @Override
    public void run() {
        BufferedInputStream in = null;
        OutputStream out = null;
        try {
            in = new BufferedInputStream(fileURL.openStream());
            out = new BufferedOutputStream(new FileOutputStream(outputFile));
            byte data[] = new byte[1024];
            int count;
            HttpURLConnection connection = (HttpURLConnection) fileURL
                    .openConnection();
            int filesize = connection.getContentLength();
            connection.disconnect();
            float progress = 0;
            while ((count = in.read(data, 0, 1024)) >= 0) {
                progress += count;
                out.write(data, 0, count);
                percentage = progress / filesize;
                try {
                    Thread.sleep(1L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {

        } finally {
            if (in != null)
                try {
                    in.close();
                } catch (IOException e) {
                }
            if (out != null)
                try {
                    out.close();
                } catch (IOException e) {
                }

            Sys.openURL(outputFile.getParentFile().getAbsolutePath());
        }
    }

    public float getPercentage() {
        return percentage;
    }

    public File getOutputFile() {
        return outputFile;
    }

    public URL getFileURL() {
        return fileURL;
    }

}
