/**
 * 
 */
package net.halalaboos.huzuni.utils;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.client.Settings;
import net.halalaboos.lib.io.HWID;
import net.minecraft.util.HttpUtil;

/**
 * @author Halalaboos
 *
 * @since Sep 30, 2013
 */
public final class ThreadCheckVip extends Thread {

	private final String vipURL = "http://halalaboos.net/client/vip.php?";
	
	public ThreadCheckVip() {
		
	}
	
	@Override
	public void run() {
		try {
			String hwid = HWID.getHwid();
			Map<String, String> map = new HashMap<String, String>();
			map.put("hwid", hwid);
			String response = HttpUtil.func_151226_a(new URL(vipURL), map, true).trim();
			Settings.setVip(response.equals("1"));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			Huzuni.modManager.load();
		}
	}
}
