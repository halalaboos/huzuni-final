package net.halalaboos.huzuni;

import com.darkmagician6.eventapi.Dispatcher;
import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.main.EventAPI;
import com.darkmagician6.morbidlib.debug.Logger;

import net.halalaboos.huzuni.client.Settings;
import net.halalaboos.huzuni.client.friends.FriendManager;
import net.halalaboos.huzuni.client.keybinds.KeybindManager;
import net.halalaboos.huzuni.client.nameprotect.ProtectionManager;
import net.halalaboos.huzuni.client.waypoints.WaypointManager;
import net.halalaboos.huzuni.client.xray.XrayManager;
import net.halalaboos.huzuni.console.CommandManager;
import net.halalaboos.huzuni.events.game.EventGameEnd;
import net.halalaboos.huzuni.events.game.EventGameStartup;
import net.halalaboos.huzuni.events.game.EventLoadWorld;
import net.halalaboos.huzuni.events.game.EventPostStartup;
import net.halalaboos.huzuni.helpers.Helper;
import net.halalaboos.huzuni.helpers.MinecraftHelper;
import net.halalaboos.huzuni.mods.ModuleManager;
import net.halalaboos.huzuni.ui.ingame.Display;
import net.halalaboos.huzuni.ui.windows.WindowManager;
import net.halalaboos.huzuni.utils.LoginUtils;
import net.halalaboos.huzuni.utils.ThreadDownloadDonators;
import net.halalaboos.huzuni.utils.ThreadDownloadUpdates;
import net.halalaboos.huzuni.utils.ThreadDownloadVersion;
import net.halalaboos.huzuni.utils.ThreadCheckVip;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.IChatComponent;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.Date;

/**
 * MODIFIED CLASSES LIST: <br>
 * <p/>
 * EVENTS <br>
 * ----------------- <br>
 * ------------------- <br>
 * Minecraft - loads of stoof. <br>
 * EntityRenderer - under renderWorld is our render event. <br>
 * Render - render nameplate event <br>
 * RenderPlayer - removed check for distance on nameplates <br>
 * ------------------- <br>
 * ----------------- <br>
 * <br>
 * X-RAY <br>
 * ----------------- <br>
 * ------------------- <br>
 * RenderBlocks - xray <br>
 * Block - lighting fix for xray <br>
 * Tessellator - opacity for x-ray <br>
 * ------------------- <br>
 * ----------------- <br>
 * <br>
 * GuiIngameMenu - options button. <br>
 * GuiIngame - Remove final modifier of persistantChatGUI and made it protected. (for our hook.) <br>
 * Session - Created setter for sessionID and username. <br>
 * NetHandlerPlayClient - Fixed instantiation of Minecraft.playerControllerMP to our hook. <br>
 * RenderGlobal - Swap TileEntitie and Entity section in the method renderEntities for temp-fix nametags (Also mess a bit with the lightening). <br>
 * PlayerControllerMP - Made blockHitDelay and curBlockDamageMP protected, since our speedmine sucked ass. <br>
 * ThreadDownloadImageData - Donator cape url. <br>
 * GuiNewChat - recieve message event
 * <br>
 * TODO: PVP targets, NameTags not rendering over signs/chests/(buggy)water
 */
public final class Huzuni implements MinecraftHelper {
	public static final Huzuni instance = new Huzuni();

	public static final String TITLE = "Huzuni",
            VERSION = "3.2";

	public static final ModuleManager modManager = new ModuleManager();
	
	public static final FriendManager friendManager = new FriendManager();
	
	public static final CommandManager commandManager = new CommandManager();
	
	public static final KeybindManager keybindManager = new KeybindManager();
	
	public static final WaypointManager waypointManager = new WaypointManager();
	
	public static final ProtectionManager protectionManager = new ProtectionManager();
	
	public static final XrayManager xrayManager = new XrayManager();
	
	public static final WindowManager windowManager = new WindowManager();
	
	public static final Display display = new Display();
	
    public static Logger logger;
    
    private Huzuni() {
    	logger = genLogger();
    }

    public void onStartup() {
        logger.log("Loading " + TITLE + ".");
        logger.log("Current EventAPI version: " + EventAPI.VERSION);
        logger.log(TITLE + " version : " + VERSION);
        modManager.loadMods();
        commandManager.loadCommands();
        display.loadThemes();
        Settings.setupDefaultSettings();
        Settings.load();
        Helper.blockHelper.setupBlocks();
        Dispatcher.call(new EventGameStartup());
        Listener.registry.removeEntry(EventGameStartup.class);
        logger.log("Finished loading " + TITLE + ".");
    }

    public void postStartup() {
    	ThreadDownloadVersion versionThread = new ThreadDownloadVersion();
    	versionThread.start();
    	ThreadDownloadUpdates updateThread = new ThreadDownloadUpdates();
    	updateThread.start();
    	ThreadDownloadDonators donatorThread = new ThreadDownloadDonators();
    	donatorThread.start();
    	ThreadCheckVip vipThread = new ThreadCheckVip();
    	vipThread.start();
        windowManager.initWindows();
        modManager.loadExternalModules();
        // Moved into vip check.
        // modManager.load();
        Dispatcher.call(new EventPostStartup());
        Listener.registry.removeEntry(EventPostStartup.class);
    }

    public void onEnd() {
        modManager.save();
        Settings.save();
        Dispatcher.call(new EventGameEnd());
        Listener.registry.removeEntry(EventGameEnd.class);
        logger.log("Stopped " + TITLE + ".");
    }

    /**
     * Invoked when a world is loaded (Connect to a server / join single player).
     */
    public void onLoadWorld() {
    	if (!Settings.config.getBoolean("Used")) {
            addChatMessage("Welcome to Huzuni!");
            addChatMessage("Press Right Shift to open up the GUI.");
            addChatMessage("Open up the pause menu to access the Huzuni screen and edit your settings.");
            addChatMessage("Type '.help' for more help.");
            Settings.config.put("Used", true);
        }
        Dispatcher.call(new EventLoadWorld());
    }

    /**
     * Adds a chat message to the in-game chat with our '[huzuni]' bull shit in front of it.
     */
    public void addChatMessage(String message) {
        if (mc.ingameGUI != null && mc.ingameGUI.getChatGui() != null)
        	mc.ingameGUI.getChatGui().addChatMessage(new ChatComponentText(EnumChatFormatting.GRAY + "[" + EnumChatFormatting.GOLD + TITLE + EnumChatFormatting.GRAY + "] " + message));
    }

    private static Logger genLogger() {
        File logDirectory = new File(getSaveDirectory(), "logs");
        if (!logDirectory.exists())
            logDirectory.mkdir();
        Date currentDate = new Date();
        File logFile = new File(logDirectory, "log" + "-" + currentDate.getMonth() + "-" + currentDate.getDay() + ".txt");
        return new Logger(logFile, true);

    }

    /**
     * @return huzuni folder we use.
     */
    public static File getSaveDirectory() {
        File saveFolder = new File(mc.mcDataDir, TITLE.toLowerCase());
        saveFolder.mkdirs();
        return saveFolder;
    }

}
