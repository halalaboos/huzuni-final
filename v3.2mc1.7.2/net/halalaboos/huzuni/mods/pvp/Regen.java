package net.halalaboos.huzuni.mods.pvp;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;

import net.halalaboos.huzuni.events.game.EventTick;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.DefaultModule;
import net.minecraft.network.play.client.C03PacketPlayer;

public class Regen extends DefaultModule implements Listener {
    private boolean isHealing;

    public Regen() {
        super("Regen");
        setDescription("Regens your health faster.");
    }

    @Override
    protected void onToggle() {
    }

    @Override
    protected void onEnable() {
        registry.registerListener(this);
    }

    @Override
    protected void onDisable() {
        registry.unregisterListener(this);
    }

    @EventTarget
    public void onTick(EventTick event) {
        boolean canHeal = mc.thePlayer.onGround || mc.thePlayer.isInWater() || mc.thePlayer.isOnLadder();
        boolean shouldHeal = mc.thePlayer.getHealth() <= 19 && mc.thePlayer.getFoodStats().getFoodLevel() > 17;

        if (canHeal && shouldHeal && !isHealing) {
            new Thread() {
                @Override
                public void run() {
                    isHealing = true;
                    for (short s = 0; s <= 3000; s++) {
                        mc.thePlayer.sendQueue.addToSendQueue(new C03PacketPlayer(mc.thePlayer.onGround));
                    }
                    isHealing = false;
                }
            }.start();
        }
    }

}
