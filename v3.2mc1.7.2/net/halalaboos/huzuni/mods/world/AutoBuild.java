/**
 * 
 */
package net.halalaboos.huzuni.mods.world;

import static org.lwjgl.opengl.GL11.glColor4f;
import static org.lwjgl.opengl.GL11.glPopMatrix;
import static org.lwjgl.opengl.GL11.glPushMatrix;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.lwjgl.util.vector.Vector3f;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;
import com.darkmagician6.eventapi.types.Priority;
import com.darkmagician6.morbidlib.utils.timer.AccurateTimer;
import com.darkmagician6.morbidlib.utils.timer.Timer;

import net.halalaboos.huzuni.client.Settings;
import net.halalaboos.huzuni.events.game.EventPlaceBlock;
import net.halalaboos.huzuni.events.game.EventPreMotionUpdate;
import net.halalaboos.huzuni.events.game.EventTick;
import net.halalaboos.huzuni.events.render.EventRenderWorld;
import net.halalaboos.huzuni.helpers.Helper;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.VipDefaultModule;
import net.halalaboos.huzuni.ui.screen.AutoBuildMenu;
import net.halalaboos.huzuni.utils.RenderUtils;
import net.halalaboos.lib.io.config.DefaultConfigListener;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;

/**
 * @author Halalaboos
 *
 */
public class AutoBuild extends VipDefaultModule implements Helper, Listener {

	private boolean[] pattern = new boolean[25];
	
	private List<Vector3f> layout = new ArrayList<Vector3f>();
	
	private Vector3f position = null;
	
	private boolean running = false;
	
	private final Timer timer = new AccurateTimer();
	
	private int direction = 0;
	
	private DefaultConfigListener<Float> delay = new DefaultConfigListener<Float>("Build Delay", 500F);

	/**
	 * @param name
	 */
	public AutoBuild() {
		super("Auto Build");
        setCategory(Category.WORLD);
        Settings.addListener(delay);
        Settings.put("Build Delay", 500F);
        setDescription("Places blocks in the pattern made.");
	}

	/**
	 * @see net.halalaboos.huzuni.mods.DefaultModule#onToggle()
	 */
	@Override
	protected void onToggle() {
		if (enabled) {
			mc.displayGuiScreen(new AutoBuildMenu(this));
		}
	}

	/**
	 * @see net.halalaboos.huzuni.mods.Module#onEnable()
	 */
	@Override
	protected void onEnable() {
		registry.registerListener(this);
		huzuni.addChatMessage("Press sneak and place a block to start building!");
	}

	/**
	 * @see net.halalaboos.huzuni.mods.Module#onDisable()
	 */
	@Override
	protected void onDisable() {
		registry.unregisterListener(this);
		if (running) {
			position = null;
			running = false;
			huzuni.addChatMessage("Cancelling..");
		}
	}
	
	@EventTarget
	public void onPlaceBlock(EventPlaceBlock event) {
		if (running) {
			event.setCancelled(true);
			return;
		}
		if (mc.gameSettings.keyBindSneak.pressed) {
			if (!running) {
				setupLayout();
				position = new Vector3f(event.x, event.y + 3, event.z);
				genDirection();
				running = true;
				timer.reset();
				huzuni.addChatMessage("Placing..");
				event.setCancelled(true);
			}
		}
	}

	@EventTarget
	public void onPreMotionUpdate(EventTick event) {
		if (running) {
			int index = getNextPiece();
			if (index == -1) {
				running = false;
				position = null;
				huzuni.addChatMessage("Done!");
				return;
			}
			
			Vector3f nextPiece = layout.get(index);
			
			int[] positions = getAdjustedVector(nextPiece);
            int x = positions[0], y = positions[1], z = positions[2];
			float offset = 0.5F;
			int side = getSide((int) position.x + x, (int) position.y + y, (int) position.z + z);
			blockHelper.faceBlock(position.x + x + offset, position.y + y + offset, position.z + z + offset);
			
			positions = getPositionsFromSide(positions, side);
			x = positions[0]; y = positions[1]; z = positions[2];
			
			if (blockHelper.canReach(position.x + x, position.y + y, position.z + z, 4.5F) && mc.thePlayer.getCurrentEquippedItem() != null && mc.thePlayer.getCurrentEquippedItem().canEditBlocks()) {
				if (timer.hasReach(delay.getValue())) {
					placeBlock(position.x + x, position.y + y, position.z + z, side);
					mc.thePlayer.swingItem();
					removeAndContinue(index);
					timer.reset();
				}
			}
		}
	}
	
	@EventTarget(Priority.HIGH)
    public void onRender3D(EventRenderWorld event) {
		if (running) {
			for (int i = 0; i < layout.size(); i++) {
				Vector3f position = layout.get(i);
				int[] positions = getAdjustedVector(position);
	            int x = positions[0], y = positions[1], z = positions[2];
					double renderX = (this.position.x + x) - RenderManager.renderPosX;
					double renderY = (this.position.y + y) - RenderManager.renderPosY;
					double renderZ = (this.position.z + z) - RenderManager.renderPosZ;

					glPushMatrix();
					AxisAlignedBB boundingBox = AxisAlignedBB.getBoundingBox(renderX, renderY, renderZ, renderX + 1, renderY + 1, renderZ + 1);
					glColor4f(0, 1, 0, 0.2F);
					RenderUtils.drawBox(boundingBox);
					glColor4f(0, 0, 0, 0.3F);
					RenderUtils.drawOutlinedBox(boundingBox);
					glPopMatrix();
				
			}
		}
    }
	
	protected void placeBlock(double x, double y, double z, int side) {
		float offset = 0F;
        mc.getNetHandler().addToSendQueue(new C08PacketPlayerBlockPlacement((int) x, (int) y, (int) z, side, mc.thePlayer.inventory.getCurrentItem(), offset, offset, offset));
        mc.thePlayer.getCurrentEquippedItem().tryPlaceItemIntoWorld(mc.thePlayer, mc.theWorld, (int) x, (int) y, (int) z, side, offset, offset, offset);
	}
	
	/**
	 * 
	 * */
	protected boolean removeAndContinue(int index) {
		if (layout.size() > index) {
			layout.remove(index);
			return false;
		} else
			return true;
	}
	
	/**
	 * 
	 * */
	protected int getNextPiece() {
		if (layout.size() < 1)
			return -1;
		else {
			for (int i = 0; i < layout.size(); i++) {
				Vector3f piece = layout.get(i);
				int[] positions = getAdjustedVector(piece);
				
	            int x = positions[0], y = positions[1], z = positions[2];
				if (getSide((int) (position.x + x), (int) (position.y + y), (int) (position.z + z)) != -1) {
					return i;
				}
			}
			return -1;
		}
	}
	
	/**
	 * Adjusts the given vector according to our direction.
	 * */
	protected int[] getAdjustedVector(Vector3f position) {
		int x = 0, y = (int) (position.y), z = 0;
        switch (direction) {
        case 0:
        	x = (int) -position.x;
        	z = (int) position.z;
        	break;
        case 1:
        	x = (int) -position.z;
        	z = (int) -position.x;
        	break;
        case 2:
        	x = (int) position.x;
        	z = (int) -position.z;
        	break;
        case 3:
        	x = (int) position.z;
        	z = (int) position.x;
        	break;
        default:
        	x = (int) position.x;
        	z = (int) position.z;
        }
        return new int[] { x, y, z };
	}
	
	/**
	 * Finds a block that is adjacent to the coordinates, if there is a block it will give the side corresponding.
	 * */
	protected int getSide(int x, int y, int z) {
		if (!isAir(x, y, z))
			return -1;
		if (!isAir(x, y - 1, z))
			return 1;
		else if (!isAir(x, y + 1, z))
			return 0;
		else if (!isAir(x - 1, y, z))
			return 5;
		else if (!isAir(x + 1, y, z))
			return 4;
		else if (!isAir(x, y, z - 1))
			return 3;
		else if (!isAir(x, y, z + 1))
			return 2;
		else
			return -1;
	}
	
	/**
	 * From constant testing, I figured out that when you place a block,
	 * the coordinates sent is the coordinates to the block corresponding to the side. 
	 * They go as follow.
	 * */
	protected int[] getPositionsFromSide(int[] positions, int side) {
		switch (side) {
		case 0:
			positions[1] += 1;
			return positions;
		case 1:
			positions[1] -= 1;
			return positions;
		case 2:
			positions[2] += 1;
			return positions;
		case 3:
			positions[2] -= 1;
			return positions;
		case 4:
			positions[0] += 1;
			return positions;
		case 5:
			positions[0] -= 1;
			return positions;
			default:
				return positions;
		}
	}

	/**
	 * @return If the coordinates are air.
	 * */
	protected boolean isAir(double x, double y, double z) {
		return mc.theWorld.getBlock((int) x, (int) y, (int) z).func_149688_o() == Material.air;
	}
	
	/**
	 * 
	 */
	protected void genDirection() {
        direction = MathHelper.floor_double((double) (mc.thePlayer.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3;
	}
	
	/**
	 * Sets up the layout from the pattern.
	 * */
	protected void setupLayout() {
		
		this.layout = genLayout();
		/*
		 * SWASTIKA
		layout.clear();

		// TOP ARM
		layout.add(new Vector3f(2, 2, 0));
		layout.add(new Vector3f(1, 2, 0));
		layout.add(new Vector3f(0, 2, 0));
		layout.add(new Vector3f(0, 1, 0));
		
		// CENTER
		layout.add(new Vector3f(0, 0, 0));
		
		// BOTTOM ARM
		layout.add(new Vector3f(0, -1, 0));
		layout.add(new Vector3f(0, -2, 0));
		layout.add(new Vector3f(-1, -2, 0));
		layout.add(new Vector3f(-2, -2, 0));
		
		// RIGHT ARM
		layout.add(new Vector3f(1, 0, 0));
		layout.add(new Vector3f(2, 0, 0));
		layout.add(new Vector3f(2, -1, 0));
		layout.add(new Vector3f(2, -2, 0));
		
		// LEFT ARM
		layout.add(new Vector3f(-1, 0, 0));
		layout.add(new Vector3f(-2, 0, 0));
		layout.add(new Vector3f(-2, 1, 0));
		layout.add(new Vector3f(-2, 2, 0));
		*/
	}
	/**
	 * LAYOUT {
	 *	1  2  3  4 	5
	 *	6  7  8  9 	10
	 *	11 12 13 14 15
	 *	16 17 18 19 20
	 *	21 22 23 24 25
	 * }
	 * Generates a list from the pattern set.
	 * @return
	 */
	private List<Vector3f> genLayout() {
		List<Vector3f> list = new ArrayList<Vector3f>();
		for (int i = 0; i < pattern.length; i++) {
			boolean enabled = pattern[i];
			if (enabled) {
				// System.out.println("layout.add(new Vector3f(" + getX(i) + ", " + getY(i) + ", 0));");
				list.add(new Vector3f(getX(i), getY(i), 0));
			}
		}
		return list;
	}

	public boolean[] getPattern() {
		return pattern;
	}
	
	/**
	 * Awful method of getting x offset from the id.
	 * */
	private int getX(int id) {
		id += 1;
		if (id == 1 || id == 6 || id == 11 || id == 16 || id == 21) {
			return -2;
		} else if (id == 2 || id == 7 || id == 12 || id == 17 || id == 22) {
			return -1;
		} else if (id == 3 || id == 8 || id == 13 || id == 18 || id == 23) {
			return 0;
		} else if (id == 4 || id == 9 || id == 14 || id == 19 || id == 24) {
			return 1;
		} else if (id == 5 || id == 10 || id == 15 || id == 20 || id == 25) {
			return 2;
		} else 
			return 6;
	}
	
	/**
	 * Really bad method of getting the y offset from the id.
	 * */
	private int getY(int id) {
		id += 1;
		if (id <= 5) {
			return 2;
		} else if (id > 5 && id <= 10) {
			return 1;
		} else if (id > 10 && id <= 15) {
			return 0;
		} else if (id > 15 && id <= 20) {
			return -1;
		} else if (id > 20 && id <= 25) {
			return -2;
		} else
			return 6;
	}
	
}
