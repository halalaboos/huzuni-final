package net.halalaboos.huzuni.mods.world;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;

import net.halalaboos.huzuni.events.game.EventAttackEntity;
import net.halalaboos.huzuni.events.game.EventClick;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.DefaultModule;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityBoat;
import net.minecraft.entity.item.EntityMinecart;
import net.minecraft.network.play.client.C02PacketUseEntity;

public class VehicleOneHit extends DefaultModule implements Listener {

    public VehicleOneHit() {
        super("Vehicle 1-Hit", -1);
        setCategory(Category.WORLD);
        setDescription("1-Hit KO's vehicles.");
    }

    @Override
    protected void onEnable() {
        registry.registerListener(this);
    }

    @Override
    protected void onDisable() {
        registry.unregisterListener(this);
    }

    @Override
    protected void onToggle() {
    }

    @EventTarget
    public void onAttackEntity(EventAttackEntity event) {
    	if (isVehicle(event.entity)) {
    		for (byte b = 0; b < 6; b++)
    			mc.getNetHandler().addToSendQueue(new C02PacketUseEntity(event.entity, C02PacketUseEntity.Action.ATTACK));
    	}
    }

    private boolean isVehicle(Entity entity) {
        return entity != null && (entity instanceof EntityBoat || entity instanceof EntityMinecart);
    }

}
