package net.halalaboos.huzuni.mods.world;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;
import com.darkmagician6.eventapi.types.Priority;
import com.darkmagician6.morbidlib.utils.timer.AccurateTimer;
import com.darkmagician6.morbidlib.utils.timer.Timer;
import net.halalaboos.huzuni.events.game.EventTick;
import net.halalaboos.huzuni.helpers.Helper;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.DefaultModule;
import net.minecraft.entity.projectile.EntityFishHook;
import net.minecraft.network.play.server.S12PacketEntityVelocity;

/**
 *  TODO: Make this better
 */
public class AutoFish extends DefaultModule implements Listener, Helper {
    private boolean castNextTick;

    private Timer timer = new AccurateTimer();

    public AutoFish() {
        super("AutoFish", -1);
        setCategory(Category.WORLD);
        setDescription("Catches fish for you.");
        setRenderWhenEnabled(true);
    }

    @Override
    protected void onEnable() {
        registry.registerListener(this);
    }

    @Override
    protected void onDisable() {
        registry.unregisterListener(this);
    }

    @Override
    protected void onToggle() {
    }

    @EventTarget(Priority.LOW)
    public void onUpdate (EventTick e) {
        if (castNextTick) {
            if (timer.hasReach(750)) {
                grabRod();
                mc.onUseItem();
                castNextTick = false;
                timer.reset();
            }
        } else {
        	if (mc.thePlayer.fishEntity != null && mc.thePlayer.fishEntity.isEntityAlive()) {
        		if (isHooked(mc.thePlayer.fishEntity)) {
                    mc.onUseItem();
                    castNextTick = true;
        		}
        	} else {
        	}
        }
    }

    /**
     * Or hook.shake > 0
     * */
    private boolean isHooked(EntityFishHook hook) {
        return hook.isInWater() && hook.motionX == 0 && hook.motionZ == 0 && hook.motionY < 0;
    }

    private void grabRod() {
        int rodSlot = inventoryHelper.findHotbarItem(346);
        if (rodSlot != -1) {
            mc.thePlayer.inventory.currentItem = rodSlot;
            mc.playerController.updateController();
        }
    }

}
