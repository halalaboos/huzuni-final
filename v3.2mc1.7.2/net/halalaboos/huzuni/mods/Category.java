package net.halalaboos.huzuni.mods;

/**
 * Used for categorizing the modules. Currently only used for the GUI.
 */
public enum Category {
    MISC("Misc"),
    MOVEMENT("Movement"),
    PLAYER("Player"),
    RENDER("Render"),
    WORLD("World"),
    PVP("PVP Modes");

    public final String formalName;

    Category(String formalName) {
        this.formalName = formalName;
    }
}
