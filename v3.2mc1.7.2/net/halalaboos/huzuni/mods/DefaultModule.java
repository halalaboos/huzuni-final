package net.halalaboos.huzuni.mods;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.utils.RenderUtils;

import org.lwjgl.input.Keyboard;

import java.awt.*;

/**
 * Default module class, given a keybind and used for most toggles.
 */
public abstract class DefaultModule extends Module implements Keybindable {

    /**
     * Keybind instance for the module.
     */
    protected int keyCode = -1;

    /**
     * Whether or not to render this module within the in-game GUI when enabled.
     */
    protected boolean renderWhenEnabled = true;

    /**
     * java.awt.Color instance held by the module. Used for rendering on the enabled mod list GUI.
     */
    protected Color color = RenderUtils.getRandomColor();

    /**
     * Constructs a module and gives it a name and default keycode for the keybind instance it's assigned. <br>
     * They default have no category, so it's recommended to assign it one so it will appear within the GUI.
     * Assigning it a keycode with a value less than 0 will not add it to the default keybind list.
     *
     * @param name
     *         Name for the module.
     * @param keyCode
     *         Default keycode assigned to the <code>Keybind</code> instance within the module.
     */
    public DefaultModule(String name, int keyCode) {
        super(name, "Huzuni Dev Team");
        this.keyCode = keyCode;
    }

    public DefaultModule(String name) {
        this(name, -1);
    }

    /**
     * Invoked when the mod is toggled.
     */
    protected abstract void onToggle();

    /**
     * Toggles the modules state.
     */
    @Override
    public void toggle() {
        super.toggle();
        Huzuni.logger.log("Toggled module: " + name + " to state: " + enabled + ".");
        color = RenderUtils.getRandomColor();
        onToggle();
    }

    /**
     * Named used when rendering the mod in the in-game GUI.
     */
    public String getRenderName() {
        return name;
    }

    public boolean shouldRenderWhenEnabled() {
        return renderWhenEnabled;
    }

    protected void setRenderWhenEnabled(boolean renderIngame) {
        this.renderWhenEnabled = renderIngame;
    }

    public Color getColor() {
        return color;
    }

    /**
     * @see net.halalaboos.huzuni.mods.Keybindable#getKeyCode()
     */
    @Override
    public int getKeyCode() {
        return keyCode;
    }

    /**
     * @see net.halalaboos.huzuni.mods.Keybindable#setKeyCode(int)
     */
    @Override
    public void setKeyCode(int keyCode) {
        this.keyCode = keyCode;
    }

    /**
     * @see net.halalaboos.huzuni.mods.Keybindable#onKeyPressed()
     */
    @Override
    public void onKeyPressed() {
        toggle();
    }

    /**
     * @see net.halalaboos.huzuni.mods.Keybindable#getKeyName()
     */
    @Override
    public String getKeyName() {
        return keyCode == -1 ? "-1" : Keyboard.getKeyName(keyCode);
    }
    
}
