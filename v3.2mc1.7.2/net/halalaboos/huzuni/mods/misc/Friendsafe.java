package net.halalaboos.huzuni.mods.misc;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.events.game.EventAttackEntity;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.DefaultModule;
import net.halalaboos.huzuni.mods.Module;

public class Friendsafe extends Module implements Listener {

    public Friendsafe() {
        super("Friendsafe", "Darkmagician6");
        setDescription("Prevents you from attacking your friends.");
        setEnabled(true);
    }

    @Override
    protected void onEnable() {
        registry.registerListener(this);
    }

    @Override
    protected void onDisable() {
        registry.unregisterListener(this);
    }

    @EventTarget
    public void onPreAttack(EventAttackEntity event) {
        if (Huzuni.friendManager.contains(event.entity.getCommandSenderName())) {
            event.setCancelled(true);
        }
    }

}
