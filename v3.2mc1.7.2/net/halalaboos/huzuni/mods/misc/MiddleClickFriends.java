package net.halalaboos.huzuni.mods.misc;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.events.game.EventClick;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.DefaultModule;
import net.halalaboos.huzuni.mods.Module;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.MovingObjectPosition.MovingObjectType;

public class MiddleClickFriends extends Module implements Listener {

    public MiddleClickFriends() {
        super("Middle Click Friends", "Halalaboos");
        setEnabled(true);
        this.setDescription("Let's middle-clicking add friends to your friendslist.");
    }

    @Override
    protected void onEnable() {
        registry.registerListener(this);
    }

    @Override
    protected void onDisable() {
        registry.unregisterListener(this);

    }
    
    @EventTarget
    public void onClickEvent(EventClick event) {
        if (event.mouseButton == 2) {
            if (mc.objectMouseOver != null) {
                if (mc.objectMouseOver.typeOfHit == MovingObjectType.ENTITY && mc.objectMouseOver.entityHit instanceof EntityPlayer) {
                    if (Huzuni.friendManager.contains(mc.objectMouseOver.entityHit.getCommandSenderName()))
                        Huzuni.friendManager.remove(mc.objectMouseOver.entityHit.getCommandSenderName());
                    else
                        Huzuni.friendManager.add(mc.objectMouseOver.entityHit.getCommandSenderName());
                }
            }
        }
    }

}
