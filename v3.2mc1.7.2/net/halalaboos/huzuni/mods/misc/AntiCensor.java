/**
 * 
 */
package net.halalaboos.huzuni.mods.misc;

import java.util.Random;

import net.halalaboos.huzuni.console.VipCommand;
import net.halalaboos.huzuni.events.game.EventSendMessage;
import net.halalaboos.huzuni.helpers.Helper;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.Module;
import net.halalaboos.huzuni.mods.VipModule;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;

/**
 * @author Halalaboos
 *
 */
public class AntiCensor extends VipModule implements Listener {

	private final Random random = new Random();
	
	private final String[] badWords = { 
		    "fuck", "shit", "ass", "bastard", "bitch", "nigger", "cock", "cunt", "dick", "damn", "douche", "penis", "vagina", "gay", "pussy" };

	
	private final String[] replacementWords = {
			"darn", "poppy", "fudge", "frick", "lint", "fiddle", "flapy", "kitty", "dong", "doorknob",
			"rebecca", "lasagna", "loo", "pamplemousse", "cat", "violin", "bacon", "powder", "ribbit",
            "fetus", "shrek", "daughter", "waffle", "rosie o donald", "obama"
	};

	/**
	 * @param name
	 * @param author
	 */
	public AntiCensor() {
		super("Anti Censor", "Halalaboos");
		setCategory(Category.MISC);
	    setDescription("Stops you from being muted on hackercraft.");
	}

	/**
	 * @see net.halalaboos.huzuni.mods.Module#onEnable()
	 */
	@Override
	protected void onEnable() {
	    registry.registerListener(this);
	}

	/**
	 * @see net.halalaboos.huzuni.mods.Module#onDisable()
	 */
	@Override
	protected void onDisable() {
	    registry.unregisterListener(this);
	}
	
	@EventTarget
	public void onSendMessage(EventSendMessage event) {
		event.setMessage(replaceBadWords(event.getMessage()));
	}

	private String replaceBadWords(String message) {
		message = message.replaceAll("(?i)douche?|fag(\b|[^e])|[s$]lut|whore|((\b)cum*(\b|(ing)))|hand[^a-z]*j[o0]b|bl[o0]w[^a-z]*j[o0]b|dildo|jack[^a-z ]*[o0]ff|pecker|pen[i!][s$]|b[o0]ner", getRandomReplacement());
		for (String badWord : this.badWords) {
			message = message.replaceAll("(?i)" + badWord, getRandomReplacement());
		}
		return message;
	}
	
	private String getRandomReplacement() {
		return replacementWords[random.nextInt(replacementWords.length)];
	}

}
