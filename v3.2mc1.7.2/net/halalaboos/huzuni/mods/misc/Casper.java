/**
 *
 */
package net.halalaboos.huzuni.mods.misc;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;
import com.darkmagician6.eventapi.types.Priority;

import net.halalaboos.huzuni.events.render.EventRenderGui;
import net.halalaboos.huzuni.helpers.Helper;
import net.halalaboos.huzuni.mods.DefaultModule;
import net.halalaboos.huzuni.mods.Keybindable;
import net.halalaboos.huzuni.mods.Module;
import net.halalaboos.huzuni.rendering.Texture;
import net.halalaboos.huzuni.utils.Timer;

import org.lwjgl.input.Keyboard;

import java.awt.*;

/**
 * @author Halalaboos
 * @since Jul 25, 2013
 */
public class Casper extends Module implements Keybindable, Listener, Helper {
    private int keyCode = Keyboard.KEY_DELETE;

	private final Timer timer = new Timer();
  
	private final Texture casper = new Texture("huzuni/casper.jpg");
    
	private boolean running = false;
    private float casperX = 0, casperY = 0;
    private final int CASPER_WIDTH = 68, CASPER_HEIGHT = 53;
    
    /**
     * @param name
     * @param keyCode
     */
    public Casper() {
        super("CASPER", "Halalaboos");
        setDescription("The single greatest mod in Minecraft to date.");

    }

    /**
     * @see net.halalaboos.huzuni.mods.DefaultModule#onEnable()
     */
    @Override
    protected void onEnable() {
        casperX = 0;
        casperY = gameHelper.getScreenHeight() / 2 - CASPER_HEIGHT / 2;
        timer.reset();
        registry.registerListener(this);
    }

    /**
     * @see net.halalaboos.huzuni.mods.DefaultModule#onDisable()
     */
    @Override
    protected void onDisable() {
        registry.unregisterListener(this);
    }
    

	/**
	 * @see net.halalaboos.huzuni.mods.Keybindable#getKeyCode()
	 */
	@Override
	public int getKeyCode() {
		return keyCode;
	}

	/**
	 * @see net.halalaboos.huzuni.mods.Keybindable#setKeyCode(int)
	 */
	@Override
	public void setKeyCode(int keyCode) {
		this.keyCode = keyCode;
	}

	/**
	 * @see net.halalaboos.huzuni.mods.Keybindable#onKeyPressed()
	 */
	@Override
	public void onKeyPressed() {
		toggle();
	}

	/**
	 * @see net.halalaboos.huzuni.mods.Keybindable#getKeyName()
	 */
	@Override
	public String getKeyName() {
        return keyCode == -1 ? "-1" : Keyboard.getKeyName(keyCode);
	}

    @EventTarget(Priority.LOWEST)
    public void onRenderGui(EventRenderGui event) {
        casperX += 20F;
        casper.renderTexture(timer.getTimePassed(), casperY, CASPER_WIDTH, CASPER_HEIGHT, Color.WHITE);
        if (timer.hasReach(gameHelper.getScreenWidth())) {
            setEnabled(false);
        }
    }

}
