package net.halalaboos.huzuni.mods.render;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;
import com.darkmagician6.eventapi.types.Priority;

import net.halalaboos.huzuni.events.render.EventRenderWorld;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.mods.DefaultModule;
import net.halalaboos.huzuni.rendering.Box;
import net.halalaboos.huzuni.utils.RenderUtils;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.tileentity.TileEntityChest;
import net.minecraft.util.AxisAlignedBB;

import org.lwjgl.input.Keyboard;

import static org.lwjgl.opengl.GL11.*;

public class ChestESP extends DefaultModule implements Listener {

	private final Box normal, left, right;
	
    public ChestESP() {
        super("Chest ESP", Keyboard.KEY_Y);
        setCategory(Category.RENDER);
        setRenderWhenEnabled(false);
        setDescription("Shows a box around chests.");
        normal = new Box(AxisAlignedBB.getBoundingBox(0, 0, 0, 1, 1, 1));
        left = new Box(AxisAlignedBB.getBoundingBox(0, 0, 0, 2, 1, 1));
        right = new Box(AxisAlignedBB.getBoundingBox(0, 0, 0, 1, 1, 2));

    }

    @Override
    protected void onEnable() {
        registry.registerListener(this);
    }

    @Override
    protected void onDisable() {
        registry.unregisterListener(this);
    }

    @Override
    protected void onToggle() {
    }

    @EventTarget(Priority.HIGHEST)
    public void onRenderEvent(EventRenderWorld event) {
        for (Object o : mc.theWorld.field_147482_g) {
            if (o instanceof TileEntityChest) {
                final TileEntityChest chest = (TileEntityChest) o;
                final double renderX = chest.xCoord - RenderManager.renderPosX;
                final double renderY = chest.yCoord - RenderManager.renderPosY;
                final double renderZ = chest.zCoord - RenderManager.renderPosZ;
                glPushMatrix();
                glTranslated(renderX, renderY, renderZ);
                glColor3f(1, 1, 0);

                if (chest.adjacentChestXPos != null) {
                	left.setOpaque(false);
                	left.render();
                	left.setOpaque(true);
                    glColor4f(1, 1, 0, 0.1F);
                    left.render();
                } else if (chest.adjacentChestZPosition != null) {
                	right.setOpaque(false);
                	right.render();
                	right.setOpaque(true);
                    glColor4f(1, 1, 0, 0.1F);
                    right.render();
                } else if (chest.adjacentChestXPos == null && chest.adjacentChestZPosition == null && chest.adjacentChestXNeg == null && chest.adjacentChestZNeg == null) {
                	normal.setOpaque(false);
                    normal.render();
                	normal.setOpaque(true);
                    glColor4f(1, 1, 0, 0.1F);
                    normal.render();
                }
                glPopMatrix();
            }
        }
    }

}
