/**
 *
 */
package net.halalaboos.huzuni.mods;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.console.Command;
import net.halalaboos.huzuni.helpers.MinecraftHelper;
import net.halalaboos.huzuni.ui.ingame.IngameTheme;

/**
 * @author Halalaboos
 * @since Sep 1, 2013
 */
public abstract class Module implements MinecraftHelper {
	protected final Huzuni huzuni = Huzuni.instance;

    protected String author = "None", name = "Untitled";

    protected String description = "No description available.";

    protected boolean enabled;

    protected Category category;

    public Module(String name, String author) {
        this.name = name;
        this.author = author;
    }

    /**
     * Invoked when the mod is enabled.
     * <p/>
     * These methods do not need to be public as they're only being used by classes that extend this class.
     */
    protected abstract void onEnable();

    /**
     * Invoked when the mod is disabled.
     * <p/>
     * These methods do not need to be public as they're only being used by classes that extend this class.
     */
    protected abstract void onDisable();

    /**
     * @return Module's state.
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * Sets the modules state.
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
        if (enabled)
            onEnable();
        else
            onDisable();
    }

    /**
     * Toggles the modules state.
     */
    public void toggle() {
        setEnabled(!enabled);
    }

    /**
     * @return Author of the module.
     */
    public String getAuthor() {
        return author;
    }

    /**
     * Set the author.
     */
    protected void setAuthor(String author) {
        this.author = author;
    }

    /**
     * @return Name of the module.
     */
    public String getName() {
        return name;
    }

    /**
     * Set the name of the module.
     */
    protected void setName(String name) {
        this.name = name;
    }

    /**
     * @return Description of the module.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description of the module.
     */
    protected void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return The category the module is in.
     */
    public Category getCategory() {
        return category;
    }

    /**
     * Sets the modules category.
     */
    protected void setCategory(Category category) {
        this.category = category;
    }

    /**
     * Registers a command into Huzuni's commands list.
     */
    protected void registerCommand(Command command) {
        Huzuni.commandManager.add(command);
    }

    /**
     * Registers a theme into Huzuni's in-game theme system.
     */
    protected void registerTheme(IngameTheme theme) {
        Huzuni.display.add(theme);
    }
}
