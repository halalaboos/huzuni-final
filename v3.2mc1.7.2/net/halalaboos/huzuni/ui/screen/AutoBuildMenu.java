/**
 * 
 */
package net.halalaboos.huzuni.ui.screen;

import java.awt.Color;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.util.vector.Vector3f;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.mods.world.AutoBuild;
import net.halalaboos.huzuni.utils.Timer;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.ItemRenderer;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;

/**
 * @author Halalaboos
 *
 */
public class AutoBuildMenu extends GuiScreen {

	private final AutoBuild autoBuild;
	
	private Block dirt = Blocks.field_150350_a,
	grass = Blocks.field_150349_c;
	
	public AutoBuildMenu(AutoBuild autoBuild) {
		this.autoBuild = autoBuild;
	}

	@Override
	public void initGui() {
		int size = 20 * 5 + 5;
		int left = width / 2 - size / 2,
		top = height / 2 - size / 2;
		buttonList.clear();
		int count = 0, yPos = 0, xPos = 0;
		for (int i = 0; i < 25; i++) {
			if (count >= 5) {
				yPos += 21;
				xPos = 0;
				count = 0;
			}
			buttonList.add(new ItemButton(i, left + xPos, top + yPos, autoBuild.getPattern()[i]));
			xPos += 21;
			count++;
		}
	}

	@Override
	protected void actionPerformed(GuiButton button) {
		autoBuild.getPattern()[button.id] = !autoBuild.getPattern()[button.id];
		((ItemButton) button).setPlace(autoBuild.getPattern()[button.id]);
	}
	
	/**
	 * @see net.minecraft.client.gui.GuiScreen#drawScreen(int, int, float)
	 */
	@Override
	public void drawScreen(int mouseX, int mouseY, float delta) {
		this.drawDefaultBackground();
        drawCenteredString(fontRenderer, "Auto Build Pattern.", width / 2, 16, 0xFFFFFF);
		super.drawScreen(mouseX, mouseY, delta);
	}
	
	private class ItemButton extends GuiButton {
		private final Minecraft mc = Minecraft.getMinecraft();

	    private final ItemStack dirt = new ItemStack((Block) Block.field_149771_c.getObject("dirt")),
	    grass = new ItemStack((Block) Block.field_149771_c.getObject("grass"));
		
	    private long lastScale;
	    private int scaledWidth = 0;
	    private int scaledHeight = 0;
	    private int alpha = 10;
	    private boolean place = false;
	    
		/**
		 * @param id
		 * @param x
		 * @param y
		 * @param width
		 * @param height
		 * @param label
		 */
		public ItemButton(int id, int x, int y, boolean place) {
			super(id, x, y, 20, 20, "");
			this.place = place;
		}
		
	    @Override
	    public void drawButton(Minecraft mc, int mouseX, int mouseY) {
	        if (this.drawButton) {
	        	int colorAddition = place ? 45 : 0;
	            Color black = (new Color(alpha + colorAddition, alpha + colorAddition, alpha + colorAddition, alpha + colorAddition));
	            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
	            this.mouseOver = mouseX >= this.x && mouseY >= this.y && mouseX < this.x + this.width && mouseY < this.y + this.height;
	            int hoverState = this.getRenderMode(this.mouseOver);
	            updateButton();
	            this.drawRect(this.x - scaledWidth, this.y - scaledHeight, this.x + this.width + scaledWidth, this.y + this.height + scaledHeight, black.getRGB());
	            this.mouseDragged(mc, mouseX, mouseY);
	            renderItem(place ? grass : dirt, x + 2, y + 2, 0);
	        }
	    }
	    
		private void renderItem(ItemStack itemStack, int x, int y,
				float delta) {
			GL11.glPushMatrix();
            GL11.glDisable(GL11.GL_BLEND);
            GL11.glEnable(GL12.GL_RESCALE_NORMAL);
            RenderHelper.enableGUIStandardItemLighting();
            GL11.glDepthMask(true);
            GL11.glDisable(GL11.GL_DEPTH_TEST);
            try {
            	itemRenderer.renderItemIntoGUI(mc.fontRenderer, mc.getTextureManager(), itemStack, x, y);
            	itemRenderer.renderItemOverlayIntoGUI(mc.fontRenderer, mc.getTextureManager(), itemStack, x, y);
            } catch (Exception e) {
            	e.printStackTrace();
            }
            RenderHelper.disableStandardItemLighting();
            GL11.glDisable(GL12.GL_RESCALE_NORMAL);
            GL11.glEnable(GL11.GL_DEPTH_TEST);
            GL11.glDepthMask(true);
            GL11.glPopMatrix();
		}

	    private void updateButton() {

	        if (Timer.getSystemTime() - lastScale >= 15) {
	            if (mouseOver && enabled)
	                alpha += 5;
	            else
	                alpha -= 5;

	            lastScale = Timer.getSystemTime();
	        }

	        if (alpha < 75)
	            alpha = 75;

	        if (alpha > 175) {
	            alpha = 175;
	        }
	    }

	    private boolean doPlace() {
			return place;
		}

		private void setPlace(boolean place) {
			this.place = place;
		}
	}
}
