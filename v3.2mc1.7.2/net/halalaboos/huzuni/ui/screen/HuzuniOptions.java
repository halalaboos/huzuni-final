/**
 *
 */
package net.halalaboos.huzuni.ui.screen;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.client.Settings;
import net.halalaboos.huzuni.ui.screen.keybinds.KeybindsManager;
import net.halalaboos.huzuni.ui.screen.module.ModuleManager;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;

import org.lwjgl.input.Keyboard;

/**
 * @author Halalaboos
 * @since Jul 25, 2013
 */
public class HuzuniOptions extends GuiScreen {
    private GuiScreen lastScreen;
    private GuiButton lastMousedButton;

    public HuzuniOptions(GuiScreen lastScreen) {
        this.lastScreen = lastScreen;
    }

    @Override
    public void initGui() {
        Keyboard.enableRepeatEvents(true);
        buttonList.clear();

        int leftAlign = this.width / 2 - 152,
                rightAlign = this.width / 2 + 2,
                bottomTop = this.height / 6 + 96 - 6,
                bottomCenter = this.height / 6 + 120 - 6,
                bottomBottom = this.height / 6 + 144 - 6,
                topExtraTop = this.height / 6 - 12,
                topTop = height / 6 - 12 + 24,
                topCenter = height / 6 - 12 + 48,
                topBottom = topCenter + 24;

        // Top portion
        buttonList.add(new GuiButton(2, leftAlign, topCenter, 150, 20, "Antialiasing: " + parseState(Settings.isAntiAlias())));
        buttonList.add(new GuiButton(4, rightAlign, topTop, 150, 20, "Theme: " + Huzuni.display.getCurrentTheme().getName()));
        buttonList.add(new GuiButton(3, rightAlign, topCenter, 150, 20, "Huzuni: " + parseState(Settings.isHuzuniEnabled())));
        buttonList.add(new GuiButton(5, leftAlign, topTop, 150, 20, "Global Custom Font: " + parseState(Settings.isGlobalFont())));
        buttonList.add(new GuiButton(10, rightAlign, topBottom, 150, 20, "Load Modules: " + parseState(Settings.shouldLoadModules())));
        buttonList.add(new GuiButton(13, leftAlign, topBottom, 150, 20, "Reset Windows"));

        // Bottom portion
        buttonList.add(new HuzuniSlider(12, rightAlign, bottomTop, "Line Size", "Line Size", 0.25F, Settings.getLineSize(), 10F));
        buttonList.add(new GuiButton(11, leftAlign, bottomTop, 150, 20, "Custom Chat: " + parseState(Settings.isCustomChat())));
        buttonList.add(new GuiButton(1, leftAlign, bottomCenter, 150, 20, "Void Fog: " + parseState(Settings.isVoidFog())));
        buttonList.add(new GuiButton(6, rightAlign, bottomCenter, 150, 20, "Font"));
        buttonList.add(new GuiButton(7, leftAlign, bottomBottom, 150, 20, "Modules"));
        buttonList.add(new GuiButton(8, rightAlign, bottomBottom, 150, 20, "Censor Long Messages " + parseState(Settings.isCensorLongMessages())));

        buttonList.add(new GuiButton(0, this.width / 2 - 100, this.height / 6 + 168, "Done"));
    }

    @Override
    protected void actionPerformed(GuiButton button) {
    	lastMousedButton = button;
        switch (button.id) {
            case 0:
                mc.displayGuiScreen(lastScreen);
                break;
            case 1:
            	Settings.setVoidFog(!Settings.isVoidFog());
                initGui();
                break;
            case 2:
                Settings.setAntialiasing(!Settings.isAntiAlias());
                initGui();
                break;
            case 3:
                Settings.setHuzuniEnabled(!Settings.isHuzuniEnabled());
                initGui();
                break;
            case 4:
                Huzuni.display.nextTheme();
                initGui();
                break;
            case 5:
                Settings.setGlobalFont(!Settings.isGlobalFont());
                initGui();
                break;
            case 6:
                mc.displayGuiScreen(new FontChanger(this));
                break;
            case 7:
                mc.displayGuiScreen(new ModuleManager(this));
                break;
            case 8:
                Settings.setCensorLongMessages(!Settings.isCensorLongMessages());
                initGui();
                break;
            case 10:
                Settings.setLoadModules(!Settings.shouldLoadModules());
                initGui();
                break;
            case 11:
            	  Settings.setCustomChat(!Settings.isCustomChat());
                  initGui();
            	break;
            case 13:
            	Huzuni.windowManager.resetWindows();
                break;
            default: {

            }
        }
    }
    
    @Override
    protected void mouseMovedOrUp(int x, int y, int mouseMoveOrUp) {
    	super.mouseMovedOrUp(x, y, mouseMoveOrUp);
    	if (this.lastMousedButton != null) {
    		if (lastMousedButton instanceof HuzuniSlider) {
    			HuzuniSlider slider = (HuzuniSlider) lastMousedButton;
    			switch (slider.id) {
    			case 12:
    				Settings.setLineSize(slider.getSliderValue());
    				break;
    			}
            	lastMousedButton = null;
    		}
    	}
    }

    private String parseState(boolean state) {
        return state ? "On" : "Off";
    }

    @Override
    public void onGuiClosed() {
        Keyboard.enableRepeatEvents(false);
    }

    @Override
    public void drawScreen(int par1, int par2, float par3) {
        super.drawDefaultBackground();
        super.drawScreen(par1, par2, par3);
        drawCenteredString(fontRenderer, Huzuni.TITLE + " Options", width / 2, 16, 0xFFFFFF);
    }

    @Override
    protected void keyTyped(char c, int keyCode) {
        super.keyTyped(c, keyCode);
    }

    @Override
    protected void mouseClicked(int x, int y, int buttonID) {
        super.mouseClicked(x, y, buttonID);
    }
}
