/**
 * 
 */
package net.halalaboos.huzuni.ui.windows.waypoints;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.client.waypoints.Waypoint;
import net.halalaboos.lib.ui.components.SlotComponent;
import net.halalaboos.lib.ui.utils.GLUtils;

/**
 * @author Halalaboos
 *
 * @since Oct 6, 2013
 */
public class SlotWaypoints extends SlotComponent<Waypoint> {
	private final WaypointWindow waypointWindow;
	
	private final Color highlightColor = new Color(0F, 0.5F, 1F, 0.75F),
	defaultColor = new Color(0.3F, 0.3F, 0.3F, 0.75F);
	
	private int selectedIndex = -1;
	
	/**
	 * @param dimensions
	 */
	public SlotWaypoints(WaypointWindow waypointWindow, Dimension dimensions) {
		super(dimensions);
		this.waypointWindow = waypointWindow;
		
	}

	/**
	 * @see net.halalaboos.lib.ui.components.SlotComponent#render(int, java.lang.Object, java.awt.Rectangle, boolean, boolean)
	 */
	@Override
	public void render(int index, Waypoint waypoint, Rectangle boundaries,
			boolean mouseOver, boolean mouseDown) {
		boolean selected = selectedIndex == index;
		GLUtils.setColor(GLUtils.getColorWithAffects(defaultColor, mouseOver, mouseDown));
		GLUtils.drawFilledRect(boundaries.x, boundaries.y, boundaries.x + boundaries.width, boundaries.y + boundaries.height);
		
		GLUtils.setColor(waypoint.shouldRender() ? highlightColor : highlightColor.darker().darker());
		GLUtils.drawFilledRect(boundaries.x + boundaries.width - 11, boundaries.y + 2, boundaries.x + boundaries.width - 2, boundaries.y + boundaries.height - 2);

		GLUtils.setColor(GLUtils.getColorWithAffects(waypoint.getColor(), mouseOver, mouseDown));
		GLUtils.drawFilledRect(boundaries.x + boundaries.width - 21, boundaries.y + 2, boundaries.x + boundaries.width - 12, boundaries.y + boundaries.height - 2);
		
		GLUtils.drawString(compact(waypoint.getName(), "..", boundaries.width - 21), boundaries.x + 2, boundaries.y + 2, 0xFFFFFF);
		if (selected) {
			GLUtils.setColor(GLUtils.getColorWithAffects(Color.RED, mouseOver, mouseDown));
			GLUtils.drawRect(boundaries.x, boundaries.y, boundaries.x + boundaries.width, boundaries.y + boundaries.height, 1F);
		}
	}
	
	private Color flip(Color color) {
		return new Color(255 - color.getRed(), 255 - color.getGreen(), 255 - color.getBlue());
	}
	
	private String compact(String text, String endLine, int maxLength) {
		String newText = "";
		char[] chars = text.toCharArray();
		for (char i = 0; i < chars.length; i++) {
			if (GLUtils.getStringWidth(newText + chars[i] + endLine) < maxLength) {
				newText += chars[i];
			} else {
				newText += endLine;
				break;
			}
		}
		return newText;
	}

	/**
	 * @see net.halalaboos.lib.ui.components.SlotComponent#onClicked(int, java.lang.Object, java.awt.Rectangle, java.awt.Point, int)
	 */
	@Override
	public void onClicked(int index, Waypoint waypoint, Rectangle boundaries,
			Point mouse, int buttonID) {
		if (buttonID == 0) {
			Rectangle renderArea = new Rectangle(boundaries.x + boundaries.width - 11, boundaries.y + 2, boundaries.x + boundaries.width - 2, boundaries.y + boundaries.height - 2);
			if (renderArea.contains(mouse)) {
				waypoint.setRender(!waypoint.shouldRender());
			} else {
				if (selectedIndex == index)
					selectedIndex = -1;
				else
					selectedIndex = index;
				waypointWindow.onIndexChanged();
			}	
		}
	}

	/**
	 * @see net.halalaboos.lib.ui.components.SlotComponent#onReleased(int, java.lang.Object, java.awt.Point)
	 */
	@Override
	public void onReleased(int index, Waypoint waypoint, Point mouse) {
	}

	/**
	 * @see net.halalaboos.lib.ui.components.SlotComponent#getElementHeight()
	 */
	@Override
	public int getElementHeight() {
		return 12;
	}

	public int getSelectedIndex() {
		return selectedIndex;
	}

	public void setSelectedIndex(int selectedIndex) {
		this.selectedIndex = selectedIndex;
	}

	public Waypoint getSelectedWaypoint() {
		if (selectedIndex >= 0 && selectedIndex < components.size())
			return components.get(selectedIndex);
		else
			return null;
	}
}
