/**
 * 
 */
package net.halalaboos.huzuni.ui.windows;

import java.awt.Rectangle;

import net.halalaboos.huzuni.ui.clickable.containers.HuzuniWindow;
import net.halalaboos.lib.ui.Component;
import net.halalaboos.lib.ui.DefaultContainer;
import net.halalaboos.lib.ui.components.Button;
import net.halalaboos.lib.ui.components.TextField;
import net.halalaboos.lib.ui.containers.Window;
import net.halalaboos.lib.ui.listeners.ActionListener;

/**
 * @author Halalaboos
 *
 */
public class WindowBuilder extends HuzuniWindow implements ActionListener {
	
	protected final WindowManager windowContainer;

	protected Window window;
	
	protected TextField name;
	
	protected Button save, cancel;
	
	public WindowBuilder(WindowManager windowContainer, Window window) {
		super("Window Builder");
		this.setMinimized(false);
		this.windowContainer = windowContainer;
		this.window = window;
		setupComponents();
		center();
	}
	
	public WindowBuilder(WindowManager windowContainer) {
		this(windowContainer, new HuzuniWindow("New Window"));
	}

	public void setupComponents() {
		name = new TextField();
		name.setText(window.getTitle());
		add(name);
		name.addActionListener(this);
		
		save = new Button("Save");
		save.addActionListener(this);
		add(save);
		
		layout();
		
		cancel = new Button("Cancel");
		cancel.addActionListener(this);
		add(cancel);
		
		save.getArea().width /= 2;
		cancel.getArea().width /= 2;
		cancel.getArea().setLocation(save.getArea().x + save.getArea().width + 1, save.getArea().y);
		cancel.getArea().setSize(save.getArea().getSize());
	}

	/**
	 * @see net.halalaboos.lib.ui.listeners.ActionListener#onAction(net.halalaboos.lib.ui.Component)
	 */
	@Override
	public void onAction(Component component) {
		if (component == save || component == name) {
			window.setTitle(name.getText());
			windowContainer.remove(this);
		} else if (component == cancel) {
			windowContainer.remove(this);
		}
	}
	
}
