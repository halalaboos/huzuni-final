/**
 * 
 */
package net.halalaboos.huzuni.ui.windows;

import java.io.FileWriter;
import java.io.IOException;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.client.Settings;
import net.halalaboos.huzuni.mods.Category;
import net.halalaboos.huzuni.ui.clickable.containers.HuzuniWindow;
import net.halalaboos.lib.ui.Component;
import net.halalaboos.lib.ui.components.Label;
import net.halalaboos.lib.ui.components.Slider;
import net.halalaboos.lib.ui.listeners.ActionListener;

/**
 * @author Halalaboos
 *
 * @since Oct 8, 2013
 */
public class ValuesWindow extends HuzuniWindow {

	/**
	 * @param title
	 */
	public ValuesWindow() {
		super("Values");
        ActionListener sliderActionListener = getSliderListener();

		/* FORMAT: name, value watermark, start value, end value, (optional) increment value */
        Object[][] sliderInformation = new Object[][] {
                {"Kill Aura Speed", "", 1F, 15F},
                {"Kill Aura Reach", "", 3F, 6F},
                {"Nuker Radius", "", 2F, 5F, 1F},
                {"Nuker Speed", "", 0F, 5F, 1F},
                {"Flight Speed", "", 1F, 10F},
                {"NameTag Scale", "%", 10F, 15F},
                {"Speedmine Speed", "", 0.1F, 1.0F},
                {"Fastplace Speed", "", 0F, 4F, 1F},
                {"Step Height", "", 1F, 10F, 0.5F},
                {"Build Delay", "MS", 100F, 1000F, 10F}

        };
        for (Object[] sliderInfo : sliderInformation) {
        	String valueName = (String) sliderInfo[0];
        	String valueWatermark = (String) sliderInfo[1];
        	float minVal = (Float) sliderInfo[2];
        	float maxVal = (Float) sliderInfo[3];
        	float currentVal = Settings.config.getFloat(valueName);
        	Slider slider;

        	if (sliderInfo.length > 4) {
        		float incrementVal = (Float) sliderInfo[4];
        		slider = new Slider(valueName, minVal, currentVal, maxVal, incrementVal);
        	} else
        		slider = new Slider(valueName, minVal, currentVal, maxVal);
        	slider.setValueWatermark(valueWatermark);
        	slider.addActionListener(sliderActionListener);
        	add(slider);
        }
        
	}
	

    protected ActionListener getSliderListener() {
        return new ActionListener() {

            @Override
            public void onAction(Component component) {
                if (component instanceof Slider) {
                    Slider slider = (Slider) component;
                    Settings.config.put(slider.getLabel(), slider.getValue());
                    Settings.save();
                }
            }

        };
    }

}
