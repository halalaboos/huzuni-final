/**
 *
 */
package net.halalaboos.huzuni.ui.ingame.themes;

import net.halalaboos.huzuni.ui.ingame.IngameTheme;
import net.minecraft.client.Minecraft;

/**
 * @author Halalaboos
 * @since Aug 3, 2013
 */
public class None implements IngameTheme {

    /**
     * @see net.halalaboos.huzuni.ui.ingame.IngameTheme#render(net.minecraft.src.Minecraft, int, int)
     */
    @Override
    public void render(Minecraft mc, int screenWidth, int screenHeight) {
    }

    /**
     * @see net.halalaboos.huzuni.ui.ingame.IngameTheme#getName()
     */
    @Override
    public String getName() {
        return "None";
    }

    /**
     * @see net.halalaboos.huzuni.ui.ingame.IngameTheme#onKeyPressed(int)
     */
    @Override
    public void onKeyPressed(int keyCode) {
    }

}
