/**
 *
 */
package net.halalaboos.huzuni.ui.ingame.themes;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.mods.DefaultModule;
import net.halalaboos.huzuni.ui.ingame.IngameTheme;
import net.halalaboos.huzuni.utils.RenderUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.util.MathHelper;
import net.minecraft.world.chunk.Chunk;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Halalaboos
 * @since Aug 3, 2013
 */
public class Informative implements IngameTheme {

    @Override
    public void render(Minecraft mc, int screenWidth, int screenHeight) {
        String coords = "XYZ: " + (int) mc.thePlayer.posX + ", " + (int) mc.thePlayer.posY + ", " + (int) mc.thePlayer.posZ;
        String biome = "Biome: " + getBiome();
        String time = "Time: " + getTime();
        String fps = "FPS: " + mc.debug.split(" ")[0];
        String[] labels = new String[] { coords, biome, time, fps};

        int currentFPS = Integer.parseInt(mc.debug.split(" ")[0]);
        int width = getWidest(labels);
        int yPos = 1;

        if(width < getWidest()) {
            width = getWidest();
        }

        RenderUtils.drawRect(0, 0, width, 40, 0x9f000000);
        yPos = renderLabel(labels, 0, yPos);
        RenderUtils.drawRect(0, yPos, width, yPos + 2, 0x5f000000);
        RenderUtils.drawRect(0, yPos, currentFPS > width ? width : currentFPS, yPos + 2, 0x8fff00ff);
        yPos += 4;
        RenderUtils.drawRect(0, yPos, width, yPos + 2, 0xffffff00);
        RenderUtils.drawRect(0, 42, width, yPos + getArrayHeight(), 0x8f000000);
        yPos += 4;

        for (DefaultModule mod : Huzuni.modManager.getDefaultModules()) {
            if (mod.shouldRenderWhenEnabled() && mod.isEnabled()) {
                mc.fontRenderer.drawStringWithShadow(mod.getRenderName(), 0, yPos, 0x525252);
                yPos += 9;
            }
        }
    }

    private int getWidest(String[] s) {
        int curWidth = 20;
        for(String string : s) {
            if(mc.fontRenderer.getStringWidth(string) > curWidth) {
                curWidth = mc.fontRenderer.getStringWidth(string) + 15;
            }
        }
        return curWidth;
    }

    private int renderLabel(String[] label, int x, int y) {
        for(String s : label) {
            mc.fontRenderer.drawStringWithShadow(s, x, y, 0xffffff);
            y += 9;
        }
        return y;
    }

    private int getWidest() {
        int width = 50;
        for (DefaultModule module : Huzuni.modManager.getDefaultModules()) {
            if (module.isEnabled()) {
                if (module.shouldRenderWhenEnabled()) {
                    if (width < mc.fontRenderer.getStringWidth(module.getRenderName()) + 6)
                        width = mc.fontRenderer.getStringWidth(module.getRenderName()) + 6;
                }
            }
        }
        return width;
    }

    private int getArrayHeight() {
        int yPos = 4;
        for (DefaultModule mod : Huzuni.modManager.getDefaultModules()) {
            if (mod.shouldRenderWhenEnabled() && mod.isEnabled()) {
                yPos += 9;
            }
        }
        return yPos;
    }

    /**
     * @return Biome name we're in.
     */
    private String getBiome() {
        int x = MathHelper.floor_double(this.mc.thePlayer.posX);
        int z = MathHelper.floor_double(this.mc.thePlayer.posZ);
        Chunk currentChunk = this.mc.theWorld.getChunkFromBlockCoords(x, z);
        return currentChunk.getBiomeGenForWorldCoords(x & 15, z & 15, this.mc.theWorld.getWorldChunkManager()).biomeName;
    }

    /**
     * @return Current formatted time.
     */
    private String getTime() {
        SimpleDateFormat date = new SimpleDateFormat("h:mm a");
        return date.format(new Date());
    }

    /**
     * @see net.halalaboos.huzuni.ui.ingame.IngameTheme#getName()
     */
    @Override
    public String getName() {
        return "Informative";
    }

    /**
     * @see net.halalaboos.huzuni.ui.ingame.IngameTheme#onKeyPressed(int)
     */
    @Override
    public void onKeyPressed(int keyCode) {
    }

}
