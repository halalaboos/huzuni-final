/**
 * 
 */
package net.halalaboos.huzuni.ui.ingame.themes;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.mods.DefaultModule;
import net.halalaboos.huzuni.ui.ingame.IngameTheme;
import net.halalaboos.huzuni.utils.RenderUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.util.StringUtils;

/**
 * @author Halalaboos
 *
 * @since Sep 22, 2013
 */
public class Iridium implements IngameTheme {
    int width, height;

	int[] colors = new int[] {
		0x777777,// MISC
		0x3399FF,// MOVEMENT
		0xF8A445,// PLAYER
		0xFC25E2,// RENDER
		0x14FF0E,// WORLD
		0xF0080E // PVP
	};
	
	/**
	 * @see net.halalaboos.huzuni.ui.ingame.IngameTheme#render(net.minecraft.src.Minecraft, int, int)
	 */
	@Override
	public void render(Minecraft mc, int screenWidth, int screenHeight) {
		mc.fontRenderer.drawStringWithShadow(Huzuni.TITLE + " v" + Huzuni.VERSION, 2, 2, 0xFFFFFF);
		int renderX = gameHelper.getScreenWidth() - width - 2;
		RenderUtils.drawRect(renderX, 2, gameHelper.getScreenWidth() - 2, 2 + height, 0x60000000);
		height = 4;
        width = 0;
        for (DefaultModule module : Huzuni.modManager.getDefaultModules()) {
            if (module.isEnabled() && module.getCategory() != null) {
                mc.fontRenderer.drawStringWithShadow(module.getRenderName(), gameHelper.getScreenWidth() - mc.fontRenderer.getStringWidth(module.getRenderName()) - 4, height, colors[module.getCategory().ordinal()]);
                height += 10;
                int stringWidth = mc.fontRenderer.getStringWidth(StringUtils.stripControlCodes(module.getRenderName())) + 4;
                if (stringWidth > width)
                    width = stringWidth;
            }
        }
	}

	/**
	 * @see net.halalaboos.huzuni.ui.ingame.IngameTheme#getName()
	 */
	@Override
	public String getName() {
		return "Iridium";
	}

	/**
	 * @see net.halalaboos.huzuni.ui.ingame.IngameTheme#onKeyPressed(int)
	 */
	@Override
	public void onKeyPressed(int keyCode) {
	}

}
