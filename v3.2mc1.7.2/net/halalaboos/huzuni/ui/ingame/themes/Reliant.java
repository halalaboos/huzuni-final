/**
 * 
 */
package net.halalaboos.huzuni.ui.ingame.themes;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.mods.DefaultModule;
import net.halalaboos.huzuni.ui.ingame.IngameTheme;
import net.minecraft.client.Minecraft;

/**
 * @author Halalaboos
 *
 * @since Sep 22, 2013
 */
public class Reliant implements IngameTheme {

    private String[] reliantNames = new String[] {
    		"Speedmine",
    		"Sprint",
    		"NoFall",
    		"Nuker",
    		"Flight",
    		"Bright",
            "Sneak",
    		"Kill Aura",
            "Annoy",
            "Xray"
    };
    
    private String[] replacementNames = new String[] {
    		"Speedy Gonzales",
    		"Speed",
    		"NoFall",
    		"Nuker",
    		"Flight",
    		"Fullbright",
            "Sneak",
    		"Kill Aura",
            "Annoy",
            "Wallhack"

    };

    private int[] reliantColors = new int[] {
    		0xFFe0a341,
    		0xFF96cc39,
    		0xff13c422,
    		0x889900,
    		0xFF7fb9d8,
    		0xFFFF00,
            0xFF009000,
    		0xFFe21418,
            0xffc456ff,
            0xff7c9e9a
    };
	
	/**
	 * @see net.halalaboos.huzuni.ui.ingame.IngameTheme#render(net.minecraft.src.Minecraft, int, int)
	 */
	@Override
	public void render(Minecraft mc, int screenWidth, int screenHeight) {
        mc.fontRenderer.drawStringWithShadow(Huzuni.TITLE + " (rel-1.7)", 2, 2, 0xFFFFFF);
        int yPos = 2;
        for (DefaultModule module : Huzuni.modManager.getDefaultModules()) {
            if (module.isEnabled()) {
            	for (int i = 0; i < reliantNames.length; i++) {
            		if (module.getName().equalsIgnoreCase(reliantNames[i])) {
                        mc.fontRenderer.drawStringWithShadow(replacementNames[i], screenWidth - 2 - mc.fontRenderer.getStringWidth(replacementNames[i]), yPos, reliantColors[i]);
                        yPos += 10;
            		}
            	}
            }
        }
    }

	/**
	 * @see net.halalaboos.huzuni.ui.ingame.IngameTheme#getName()
	 */
	@Override
	public String getName() {
		return "Reliant";
	}

	/**
	 * @see net.halalaboos.huzuni.ui.ingame.IngameTheme#onKeyPressed(int)
	 */
	@Override
	public void onKeyPressed(int keyCode) {
	}

}
