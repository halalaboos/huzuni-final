package net.halalaboos.huzuni.ui.ingame.themes;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.mods.DefaultModule;
import net.halalaboos.huzuni.ui.ingame.IngameTheme;
import net.halalaboos.huzuni.utils.RenderUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.util.StringUtils;

public class Skidded implements IngameTheme {
    int width, height;

    @Override
    public void render(Minecraft mc, int screenWidth, int screenHeight) {
        mc.fontRenderer.drawStringWithShadow(Huzuni.TITLE + " v" + Huzuni.VERSION, 2, 2, 0xFFFF);
        RenderUtils.drawRect(1, 1, width, height, 0xAF424242);
        height = 12;
        width = 0;

        for (DefaultModule module : Huzuni.modManager.getDefaultModules()) {
            if (!module.getKeyName().equals("-1")) {
                String renderString = module.getKeyName() + " = \247" + (module.isEnabled() ? "2" : "4") + module.getName() + "\247f = " + (module.isEnabled() ? "on" : "off");
                mc.fontRenderer.drawStringWithShadow(renderString, 2, height, 0xFFFF);
                height += 12;

                int stringWidth = mc.fontRenderer.getStringWidth(StringUtils.stripControlCodes(renderString)) + 4;
                if (stringWidth > width)
                    width = stringWidth;
            }
        }
    }

    @Override
    public String getName() {
        return "Skidded";
    }

    @Override
    public void onKeyPressed(int keyCode) {
    }

}
