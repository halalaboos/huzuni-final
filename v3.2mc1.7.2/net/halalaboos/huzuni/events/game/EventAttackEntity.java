/**
 *
 */
package net.halalaboos.huzuni.events.game;

import net.minecraft.entity.Entity;

import com.darkmagician6.eventapi.events.premade.EventCancellable;

/**
 * @author Halalaboos
 * @since Jul 17, 2013
 */
public class EventAttackEntity extends EventCancellable {

    public final Entity entity;

    public EventAttackEntity(Entity entity) {
        this.entity = entity;
    }

}
