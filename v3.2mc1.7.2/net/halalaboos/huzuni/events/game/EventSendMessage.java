package net.halalaboos.huzuni.events.game;

import com.darkmagician6.eventapi.events.premade.EventCancellable;

public class EventSendMessage extends EventCancellable {

    private String message;

    public EventSendMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
