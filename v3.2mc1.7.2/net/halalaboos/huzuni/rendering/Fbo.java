/**
 * 
 */
package net.halalaboos.huzuni.rendering;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.EXTFramebufferObject.*;

import java.awt.Dimension;
import java.awt.Rectangle;

import net.halalaboos.huzuni.utils.TextureUtils;

import org.lwjgl.opengl.ARBFramebufferObject;
import org.lwjgl.opengl.GLContext;

/**
 * I used a tutorial for this. This is a simple frame buffer object handle.
 * @author Halalaboos
 *
 */
public class Fbo {
	int colorTextureID;
	int framebufferID;

	private final Dimension dimensions;
	
	public Fbo(Dimension dimensions) {
		this.dimensions = dimensions;
		if (!isSupported())
			throw new NullPointerException("FBO is not supported on this system!");
		genBuffers();
	}
	
	public Fbo() {
		this(new Dimension(512, 512));
	}
	
	private void genBuffers() {
		framebufferID = glGenFramebuffersEXT();
		colorTextureID = glGenTextures();           
		 
		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, framebufferID); 
		
		// Initialize the color texture, bind it to the frame buffer.
		glBindTexture(GL_TEXTURE_2D, colorTextureID);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, dimensions.width, dimensions.height, 0,GL_RGBA, GL_INT, (java.nio.ByteBuffer) null);
		glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, colorTextureID, 0);
	}
	
	/**
	 * This switches over to the frame buffer handle and resets the texture id. 
	 * */
	public void start() {
		glViewport(0, 0, dimensions.width, dimensions.height);
		// Unbind the texture and switch to our frame buffer. Then render what you want on your buffer object.
	    ARBFramebufferObject.glBindFramebuffer('\u8ca9', framebufferID);
	    ARBFramebufferObject.glFramebufferTexture2D('\u8ca9', '\u8ce0', 3553, colorTextureID, 0);
	    
	}
	
	public void end() {
		ARBFramebufferObject.glBindFramebuffer('\u8ca9', 0);

	}
	
	public void render(Rectangle area) {
		render(area.x, area.y, area.width, area.height);
	}
	
	public void render(float x, float y, float width, float height) {
		glBindTexture(GL_TEXTURE_2D, colorTextureID);
		glColor3f(1F, 1F, 1F);
		TextureUtils.renderTexture(dimensions.width, dimensions.height, x, y, width, height, 0, 0, dimensions.width, dimensions.height);
	}
	
	public static boolean isSupported() {
		return GLContext.getCapabilities().GL_EXT_framebuffer_object;
	}
}
