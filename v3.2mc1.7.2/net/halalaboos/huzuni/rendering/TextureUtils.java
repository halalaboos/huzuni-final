package net.halalaboos.huzuni.rendering;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;

import javax.imageio.ImageIO;

import org.lwjgl.BufferUtils;

import static org.lwjgl.opengl.GL11.*;

public class TextureUtils {
	
	public static int genTexture() {
		return glGenTextures();
	}

	/**
	 * @param linear Smoothes out the texture when scaling. <br>
	 * @param repeat Will repeat the texture if the UV coordinates are outside of the 0.0F ~ 1.0F range. <br>
	 * */
	public static int applyTexture(int texId, File file, boolean linear, boolean repeat) throws IOException {
		applyTexture(texId, ImageIO.read(file), linear, repeat);
        return texId;
	}
	
	/**
	 * @param linear Smoothes out the texture when scaling. <br>
	 * @param repeat Will repeat the texture if the UV coordinates are outside of the 0.0F ~ 1.0F range. <br>
	 * */
	public static int applyTexture(int texId, BufferedImage image, boolean linear, boolean repeat) {
		int[] pixels = new int[image.getWidth() * image.getHeight()];
        image.getRGB(0, 0, image.getWidth(), image.getHeight(), pixels, 0, image.getWidth());

        ByteBuffer buffer = BufferUtils.createByteBuffer(image.getWidth() * image.getHeight() * 4);
        
        for(int y = 0; y < image.getHeight(); y++){
            for(int x = 0; x < image.getWidth(); x++){
                int pixel = pixels[y * image.getWidth() + x];
                buffer.put((byte) ((pixel >> 16) & 0xFF));
                buffer.put((byte) ((pixel >> 8) & 0xFF));
                buffer.put((byte) (pixel & 0xFF));
                buffer.put((byte) ((pixel >> 24) & 0xFF));
            }
        }

        buffer.flip();
        applyTexture(texId, image.getWidth(), image.getHeight(), buffer, linear, repeat);
        return texId;
	}
	
	/**
	 * @param linear Smoothes out the texture when scaling. <br>
	 * @param repeat Will repeat the texture if the UV coordinates are outside of the 0.0F ~ 1.0F range. <br>
	 * */
	public static int applyTexture(int texId, int width, int height, ByteBuffer pixels, boolean linear, boolean repeat) {
		glBindTexture(GL_TEXTURE_2D, texId);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, linear ? GL_LINEAR : GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, linear ? GL_LINEAR : GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, repeat ? GL_REPEAT : GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, repeat ? GL_REPEAT : GL_CLAMP);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
        return texId;
	}
}
