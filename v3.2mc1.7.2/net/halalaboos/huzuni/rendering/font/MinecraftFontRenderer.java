/**
 * 
 */
package net.halalaboos.huzuni.rendering.font;

import static org.lwjgl.opengl.GL11.*;

import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Halalaboos
 *
 * @since Nov 23, 2013
 */
public class MinecraftFontRenderer extends CFont {
    protected int boldTexID, italicTexID, boldItalicTexID;
    protected CharData[] boldChars = new CharData[256];
    protected CharData[] italicChars = new CharData[256];
    protected CharData[] boldItalicChars = new CharData[256];

    private final int[] colorCode = new int[32];
    private final String colorcodeIdentifiers = "0123456789abcdefklmnor";

	/**
	 * @param font
	 * @param antiAlias
	 */
	public MinecraftFontRenderer(Font font, boolean antiAlias, boolean fractionalMetrics) {
		super(font, antiAlias, fractionalMetrics);
		setupMinecraftColorcodes();
		setupBoldItalicIDs();
	}
	
	public float drawStringWithShadow(String text, double x, double y, int color) {
		float shadowWidth = drawString(text, x + 1, y + 1, color, true);
		return Math.max(shadowWidth, drawString(text, x, y, color, false));
	}
	
	public float drawString(String text, double x, double y, int color) {
		return drawString(text, x, y, color, false);
	}
	
	public float drawCenteredString(String text, int x, int y, int color) {
		return drawStringWithShadow(text, x - getStringWidth(text) / 2, y, color);
	}

    public float drawCenteredStringNoShadow(String text, int x, int y, int color) {
        return drawString(text, x - getStringWidth(text) / 2, y, color);
    }

	public float drawString(String text, double x, double y, int color, boolean shadow) {
		if (text == null) {
			return 0;
		}
		if (color == 553648127)
            color = 0xFFFFFF;
		
		if ((color & -67108864) == 0) {
			color |= -16777216;
		}
		
		// Shadow effect
		if (shadow) {
			color = (color & 16579836) >> 2 | color & -16777216;
		}
	
		// Current rendering information.
		CharData[] currentData = this.charData;
        float alpha = (color >> 24 & 0xff) / 255F;
        boolean randomCase = false, bold = false,
        italic = false, strikethrough = false,
        underline = false;
		
        // Multiplied positions since we'll be rendering this at half scale (to look nice :D)
		x *= 2;
		y = ((y - 3) * 2);
		
		glPushMatrix();
 		glScaled(0.5D, 0.5D, 0.5D);
		glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glColor4f((float) (color >> 16 & 255) / 255.0F, (float) (color >> 8 & 255) / 255.0F, (float) (color & 255) / 255.0F, alpha);
		int size = text.length();
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, texID);
		for (int i = 0; i < size; i++) {
			char character = text.charAt(i);
			// If the character is that funky minecraft color code thingy, we'll 
			if (character == '\247' && i < size && i + 1 < size) {
				int colorIndex = colorcodeIdentifiers.indexOf(text.charAt(i + 1));
				if (colorIndex < 16) { // coloring
                    bold = false;
                    italic = false;
                    randomCase = false;
                    underline = false;
                    strikethrough = false;
                    glBindTexture(GL_TEXTURE_2D, texID);
                   	currentData = charData;
                   	
   					if (colorIndex < 0 || colorIndex > 15) {
						colorIndex = 15;
					}
   					
					if (shadow) {
						colorIndex += 16;
					}
					
					int colorcode = colorCode[colorIndex];
			        glColor4f((float) (colorcode >> 16 & 255) / 255.0F, (float) (colorcode >> 8 & 255) / 255.0F, (float) (colorcode & 255) / 255.0F, alpha);
                } else if (colorIndex == 16) { // random case
                    randomCase = true;
				} else if (colorIndex == 17) { // bold
					bold = true;
					if (italic) {
						glBindTexture(GL_TEXTURE_2D, boldItalicTexID);
						currentData = boldItalicChars;
					} else {
						glBindTexture(GL_TEXTURE_2D, boldTexID);
						currentData = boldChars;
					}
				} else if (colorIndex == 18) { // strikethrough
					strikethrough = true;
				} else if (colorIndex == 19) { // underline
					underline = true;
				} else if (colorIndex == 20) { // italic
					italic = true;
					if (bold) {
						glBindTexture(GL_TEXTURE_2D, boldItalicTexID);
						currentData = boldItalicChars;
					} else {
						glBindTexture(GL_TEXTURE_2D, italicTexID);
						currentData = italicChars;
					}
				} else if (colorIndex == 21) { // reset
                    bold = false;
                    italic = false;
                    randomCase = false;
                    underline = false;
                    strikethrough = false;
                    glColor4f((float) (color >> 16 & 255) / 255.0F, (float) (color >> 8 & 255) / 255.0F, (float) (color & 255) / 255.0F, alpha);
                   	glBindTexture(GL_TEXTURE_2D, texID);
                   	currentData = charData;
                }
				i ++;
			} else {
				if (character < currentData.length && character >= 0) {
					if (randomCase) {
						char newChar = 0;
						while (currentData[newChar].width != currentData[character].width) {
							newChar = (char) (Math.random() * 256);
						}
						character = newChar;
					}
					glBegin(GL_TRIANGLES);
					drawChar(currentData, character, (float) x, (float) y);
					glEnd();
					if (strikethrough) {
						drawLine(x, y + currentData[character].height / 2, x + currentData[character].width, y + currentData[character].height / 2, 1F);
					}
					if (underline) {
						drawLine(x, y + currentData[character].height - 2, x + currentData[character].width, y + currentData[character].height - 2, 1F);
					}
					x += currentData[character].width + kerning;
				}
			}
		}
		glHint(GL_POLYGON_SMOOTH_HINT, GL_DONT_CARE);
		glPopMatrix();
        return (float) x / 2F;
	}
	
	@Override
	public int getStringWidth(String text) {
		if (text == null) {
			return 0;
		}
		int width = 0;
		CharData[] currentData = this.charData;
        boolean bold = false, italic = false;
		int size = text.length();
		
		for (int i = 0; i < size; i++) {
			char character = text.charAt(i);
			if (character == '\247' && i < size) {
				int colorIndex = colorcodeIdentifiers.indexOf(character);
				if (colorIndex < 16) { // coloring
					bold = false;
					italic = false;
				} else if (colorIndex == 17) { // bold
					bold = true;
					if (italic) {
						currentData = boldItalicChars;
					} else {
						currentData = boldChars;
					}
				} else if (colorIndex == 20) { // italic
					italic = true;
					if (bold) {
						currentData = boldItalicChars;
					} else {
						currentData = italicChars;
					}
				} else if (colorIndex == 21) { // reset
					bold = false;
					italic = false;
					currentData = charData;
				}
				i++;
			} else {
				if (character < currentData.length && character >= 0) {
					width += currentData[character].width + kerning;
				}
			}
		}
        return width / 2;
	}
	
	@Override
	public void setFont(Font font) {
		super.setFont(font);
		setupBoldItalicIDs();
	}
	
	@Override
	public void setAntiAlias(boolean antiAlias) {
		super.setAntiAlias(antiAlias);
		setupBoldItalicIDs();
	}
	
	@Override
	public void setFractionalMetrics(boolean fractionalMetrics) {
		super.setFractionalMetrics(fractionalMetrics);
		setupBoldItalicIDs();
	}
	
	private void setupBoldItalicIDs() {
		glDeleteTextures(boldTexID);
		glDeleteTextures(italicTexID);
		glDeleteTextures(boldItalicTexID);
		this.boldTexID = this.setupTexture(font.deriveFont(Font.BOLD), antiAlias, fractionalMetrics, boldChars);
		this.italicTexID = this.setupTexture(font.deriveFont(Font.ITALIC), antiAlias, fractionalMetrics, italicChars);
		this.boldItalicTexID = this.setupTexture(font.deriveFont(Font.BOLD | Font.ITALIC), antiAlias, fractionalMetrics, boldItalicChars);
	}
	
	private void drawLine(double x, double y, double x1, double y1, float width) {
        glDisable(GL_TEXTURE_2D);
        glLineWidth(width);
        glBegin(GL_LINES);
        glVertex2d(x, y);
        glVertex2d(x1, y1);
        glEnd();
        glEnable(GL_TEXTURE_2D);
    }
	
    public List<String> wrapWords(String text, double width) {
        List<String> finalWords = new ArrayList<String>();
        if (getStringWidth(text) > width) {
            String[] words = text.split(" ");
            String currentWord = "";
            char lastColorCode = (char) -1;

            for (String word : words) {
                for (int i = 0; i < word.toCharArray().length; i++) {
                    char c = word.toCharArray()[i];

                    if (c == '\247' && i < word.toCharArray().length - 1) {
                        lastColorCode = word.toCharArray()[i + 1];
                    }
                }
                if (getStringWidth(currentWord + word + " ") < width) {
                    currentWord += word + " ";
                } else {
                    finalWords.add(currentWord);
                    currentWord = (lastColorCode == -1 ? word + " " : "\247" + lastColorCode + word + " ");
                }
            }
            if (currentWord.length() > 0) {
                if (getStringWidth(currentWord) < width) {
                	finalWords.add((lastColorCode == -1 ? currentWord + " " : "\247" + lastColorCode + currentWord + " "));
                	currentWord = "";
                } else {
                	for (String s : formatString(currentWord, width))
                		finalWords.add(s);
                }
            }
        } else
        	finalWords.add(text);
        return finalWords;
    }

    public List<String> formatString(String string, double width) {
    	List<String> finalWords = new ArrayList<String>();
    	String currentWord = "";
    	char lastColorCode = (char) -1;
    	char[] chars = string.toCharArray();
    	for (int i = 0; i < chars.length; i++) {
    		char c = chars[i];

    		if (c == '\247' && i < chars.length - 1) {
    			lastColorCode = chars[i + 1];
    		}

    		if (getStringWidth(currentWord + c) < width) {
    			currentWord += c;
    		} else {
    			finalWords.add(currentWord);
    			currentWord = (lastColorCode == -1 ? String.valueOf(c) : "\247" + lastColorCode + String.valueOf(c));
    		}
    	}

    	if (currentWord.length() > 0) {
    		finalWords.add(currentWord);
    	}

    	return finalWords;
    }

    private void setupMinecraftColorcodes() {
    	for (int index = 0; index < 32; ++index) {
    		int noClue = (index >> 3 & 1) * 85;
    		int red = (index >> 2 & 1) * 170 + noClue;
    		int green = (index >> 1 & 1) * 170 + noClue;
    		int blue = (index >> 0 & 1) * 170 + noClue;

    		if (index == 6) {
    			red += 85;
    		}

    		if (index >= 16) {
    			red /= 4;
    			green /= 4;
    			blue /= 4;
    		}

    		this.colorCode[index] = (red & 255) << 16 | (green & 255) << 8 | blue & 255;
    	}
    }
}
