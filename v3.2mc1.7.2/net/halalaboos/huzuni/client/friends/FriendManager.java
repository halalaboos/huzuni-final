package net.halalaboos.huzuni.client.friends;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.events.game.EventGameEnd;
import net.halalaboos.huzuni.events.game.EventGameStartup;
import net.halalaboos.lib.io.FileUtils;
import net.halalaboos.lib.manager.Manager;
import net.minecraft.util.StringUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;

public final class FriendManager extends Manager<String> implements Listener {
    private final File saveFile = new File(Huzuni.getSaveDirectory(), "Friends.txt");

    public FriendManager() {
        super();
        registry.registerListener(this);
    }

    public boolean add(String username) {
        username = StringUtils.stripControlCodes(username);
        if (contains(username))
            return false;
        else
            return super.add(username);
    }

    public boolean remove(String username) {
        username = StringUtils.stripControlCodes(username);
        if (!contains(username))
            return false;
        else
            return super.remove(username);
    }

    public boolean contains(String username) {
        username = StringUtils.stripControlCodes(username);
        for (String user : this.getList())
            if (user.equalsIgnoreCase(username))
                return true;
        return super.contains(username);
    }

    /**
     * @return input with regex replaced; ignoring all casing.
     */
    public String replaceIgnoreCase(String input, String regex, String replacement) {
        return input.replaceAll("(?i)" + regex, replacement);
    }

    @EventTarget
    public void onGameStartup(EventGameStartup event) {
    	loadFriends();
    }
    
    @EventTarget
    public void onGameEnd(EventGameEnd event) {
    	saveFriends();
    }
    
    public void loadFriends() {
        if (!saveFile.exists()) {
            try {
                saveFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return;
        }
        clear();
        for (String username : FileUtils.readFile(saveFile)) {
            add(username);
        }
    }
    
    public void saveFriends() {
        if (!saveFile.exists())
            try {
                saveFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        List<String> list = this.getList();
        FileUtils.writeFile(saveFile, list);
    }
}
