/**
 * 
 */
package net.halalaboos.huzuni.client.waypoints;

import static org.lwjgl.opengl.GL11.*;
import org.lwjgl.opengl.GL11;

import net.halalaboos.huzuni.events.render.EventRenderWorld;
import net.halalaboos.huzuni.helpers.MinecraftHelper;
import net.halalaboos.huzuni.utils.RenderUtils;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.util.AxisAlignedBB;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;

/**
 * @author Halalaboos
 *
 * @since Oct 6, 2013
 */
public final class WaypointRenderer implements Listener, MinecraftHelper {

	private final float RENDER_CUTOFF = 10F;
	
	private final WaypointManager waypointManager;
	
	public WaypointRenderer(WaypointManager waypointManager) {
		this.waypointManager = waypointManager;
		registry.registerListener(this);
	}
	
	@EventTarget
	public void renderWaypoints(EventRenderWorld event) {
        for (Waypoint waypoint : waypointManager.getList()) {
        	if (waypoint.isOnServer() && waypoint.shouldRender()) {
    	        float distance = (float) mc.thePlayer.getDistance(waypoint.getX(), waypoint.getY(), waypoint.getZ());
        		float renderCutoff = (float) distance / RENDER_CUTOFF;
        		if (renderCutoff > 1)
        			renderCutoff = 1;
        		
	            double renderX = waypoint.getX() - RenderManager.renderPosX;
	            double renderY = waypoint.getY() - RenderManager.renderPosY;
	            double renderZ = waypoint.getZ() - RenderManager.renderPosZ;
	            double renderSize = 1F;
	            glColor4f(waypoint.getColor().getRed() / 255F, waypoint.getColor().getGreen() / 255F, waypoint.getColor().getBlue() / 255F, renderCutoff);
	            
	            // Line
	            glBegin(GL_LINES);
	            glVertex3d(0, 0, 0);
	            glVertex3d(renderX, renderY - renderSize / 2, renderZ);
	            glEnd();
	            
	            AxisAlignedBB box = AxisAlignedBB.getBoundingBox(renderX - renderSize / 2, renderY - renderSize / 2, renderZ - renderSize / 2, renderX + renderSize / 2, renderY + renderSize / 2, renderZ + renderSize / 2);
	            
	            // Outline
	            RenderUtils.drawOutlinedBox(box);
	           
	            glColor4f(waypoint.getColor().getRed() / 255F, waypoint.getColor().getGreen() / 255F, waypoint.getColor().getBlue() / 255F, 0.1F * renderCutoff);
	            
	            // Inside
	            RenderUtils.drawBox(box);
	            
	            glPushMatrix();
	            
	            float scale = 4;
	            if ((distance / scale) > 1) {
	                glScaled((distance / scale), (distance / scale), (distance / scale));
	            }
	            RenderUtils.draw3DStringCentered(mc.fontRenderer, waypoint.getName() + " (" + (int) distance + ")", (float) renderX, (float) renderY + (float) renderSize / 2F, (float) renderZ, 0xFFFFFF);
	            glPopMatrix();
        	}
        }
    
	}
	
}
