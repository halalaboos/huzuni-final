package net.halalaboos.huzuni.client.xray;

import com.darkmagician6.eventapi.Listener;
import com.darkmagician6.eventapi.annotation.EventTarget;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.client.Settings;
import net.halalaboos.huzuni.events.game.EventGameEnd;
import net.halalaboos.huzuni.events.game.EventGameStartup;
import net.halalaboos.huzuni.helpers.Helper;
import net.halalaboos.huzuni.mods.world.Xray;
import net.halalaboos.lib.io.FileUtils;
import net.minecraft.block.Block;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public final class XrayManager implements Listener {
    final File saveFile = new File(Huzuni.getSaveDirectory(), "XrayBlocks.txt");
    public static final boolean[] xrayBlocks = new boolean[4096];
    private final Xray xrayModule;

    private float opacity = 50;
    
    public XrayManager() {
        super();
        // Diamond ore.
        xrayBlocks[56] = true;
        // TNT Block.
        xrayBlocks[46] = true;
        registry.registerListener(this);
        xrayModule = new Xray();
        Huzuni.modManager.add(xrayModule);
    }

    public Xray getModule() {
        return xrayModule;
    }

    public boolean shouldRender(Block block) {
        return xrayBlocks[Helper.blockHelper.getBlockID(block)];
    }

    public boolean shouldRender(int id) {
        return xrayBlocks[id];
    }
    
    /**
     * @return opacity in a 0 - 255 float.
     * */
    public float getOpacity() {
        return (opacity / 100F) * 255F;
    }
    
    public void setOpacity(float opacity) {
    	this.opacity = opacity;
    }

    @EventTarget
    public void onGameStartup(EventGameStartup event) {
    	load();
    }
    

    @EventTarget
    public void onGameEnd(EventGameEnd event) {
    	save();
    }

    public boolean contains(int id) {
    	return xrayBlocks[id];
    }

    public void add(int id) {
    	xrayBlocks[id] = true;

    }

    public void remove(int id) {
    	xrayBlocks[id] = false;

    }

    public void clear() {
    	for (int i = 0; i < xrayBlocks.length; i++)
    		xrayBlocks[i] = false;
    }

    public void load() {
        if (!saveFile.exists()) {
            try {
                saveFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return;
        }
        clear();
        for (String content : FileUtils.readFile(saveFile)) {
            try {
            	if (content.startsWith("opacity:")) {
            		this.opacity = Float.parseFloat(content.substring("opacity:".length()));
            	} else
            		xrayBlocks[Integer.parseInt(content)] = true;
            } catch (Exception e) {

            }
        }
    }
    
    public void save() {
        if (!saveFile.exists())
            try {
                saveFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        List<String> lines = new ArrayList<String>();
        for (int i = 0; i < xrayBlocks.length; i++) {
            if (xrayBlocks[i])
                lines.add(i + "");
        }
        lines.add("opacity:" + this.opacity);
        FileUtils.writeFile(saveFile, lines);
    }
    
}
