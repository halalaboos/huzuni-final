/**
 * 
 */
package net.halalaboos.huzuni.client.keybinds;

import net.halalaboos.huzuni.Huzuni;

/**
 * @author Halalaboos
 *
 * @since Oct 17, 2013
 */
public class CommandKeybind implements Keybind {

	private final String command;
	
	public CommandKeybind(String command) {
		this.command = command;
	}
	
	/**
	 * @see net.halalaboos.huzuni.client.keybinds.Keybind#onKeyPressed()
	 */
	@Override
	public void onKeyPressed() {
		Huzuni.commandManager.processCommand(command);
	}

	/**
	 * @see net.halalaboos.huzuni.client.keybinds.Keybind#getCommand()
	 */
	@Override
	public String getCommand() {
		return command;
	}

}
