/**
 * 
 */
package net.halalaboos.huzuni.console.commands;

import net.halalaboos.huzuni.console.Command;
import net.halalaboos.huzuni.helpers.Helper;
import net.minecraft.entity.player.EntityPlayer;

/**
 * @author Halalaboos
 *
 */
public class Find implements Command {

	/**
	 * @see net.halalaboos.huzuni.console.Command#getAliases()
	 */
	@Override
	public String[] getAliases() {
		return new String[] { "find", "f" };
	}

	/**
	 * @see net.halalaboos.huzuni.console.Command#getHelp()
	 */
	@Override
	public String[] getHelp() {
		return new String[] { "find <username>" };
	}

	/**
	 * @see net.halalaboos.huzuni.console.Command#getDescription()
	 */
	@Override
	public String getDescription() {
		return "Helps you find other players who are in a crowded room, easier.";
	}

	/**
	 * @see net.halalaboos.huzuni.console.Command#run(java.lang.String, java.lang.String[])
	 */
	@Override
	public void run(String input, String[] args) {
		String player = Helper.stringHelper.getAfter(input, 1);
		for (EntityPlayer entityPlayer : Helper.entityHelper.getPlayers()) {
			if (Helper.entityHelper.isAliveNotUs(entityPlayer)) {
				if (entityPlayer.getCommandSenderName().equalsIgnoreCase(player)) {
					Helper.entityHelper.faceEntity(entityPlayer);
					return;
				}
			}
		}
		huzuni.addChatMessage("We could not find '" + player + "'! Maybe they're not in render distance?");
	}

}
