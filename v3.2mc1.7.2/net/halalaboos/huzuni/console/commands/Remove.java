package net.halalaboos.huzuni.console.commands;

import net.halalaboos.huzuni.console.Command;

public class Remove implements Command {

    @Override
    public String[] getAliases() {
        return new String[] {"remove"};
    }

    @Override
    public String[] getHelp() {
        return new String[] {"remove <username>"};
    }

    @Override
    public String getDescription() {
        return "Removes users from the friends list.";
    }

    @Override
    public void run(String input, String[] args) {
        boolean sucessful = huzuni.friendManager.remove(args[0]);
        huzuni.addChatMessage("Friend '" + args[0] + "' " + (sucessful ? "removed. " + (new java.util.Random().nextBoolean() ? "He was a bastard anyway." : "") : "is not in your friends list!"));
        huzuni.friendManager.saveFriends();
    }
}
