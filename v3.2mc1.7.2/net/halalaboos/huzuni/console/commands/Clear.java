/**
 * 
 */
package net.halalaboos.huzuni.console.commands;

import net.halalaboos.huzuni.console.Command;

/**
 * @author Halalaboos
 *
 * @since Nov 23, 2013
 */
public class Clear implements Command {

	/**
	 * @see net.halalaboos.huzuni.console.Command#getAliases()
	 */
	@Override
	public String[] getAliases() {
		return new String[] { "clear", "cl" };
	}

	/**
	 * @see net.halalaboos.huzuni.console.Command#getHelp()
	 */
	@Override
	public String[] getHelp() {
		return new String[] { "clear" };
	}

	/**
	 * @see net.halalaboos.huzuni.console.Command#getDescription()
	 */
	@Override
	public String getDescription() {
		return "Clears the chat.";
	}

	/**
	 * @see net.halalaboos.huzuni.console.Command#run(java.lang.String, java.lang.String[])
	 */
	@Override
	public void run(String input, String[] args) {
		mc.ingameGUI.getChatGui().clearChatMessages();
	}

}
