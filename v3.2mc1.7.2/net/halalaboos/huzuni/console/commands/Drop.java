/**
 *
 */
package net.halalaboos.huzuni.console.commands;

import net.halalaboos.huzuni.console.Command;
import net.minecraft.item.ItemStack;

/**
 * @author Halalaboos
 * @since Aug 31, 2013
 */
public class Drop implements Command {

    /**
     * @see net.halalaboos.huzuni.console.Command#getAliases()
     */
    @Override
    public String[] getAliases() {
        return new String[] {"drop"};
    }

    /**
     * @see net.halalaboos.huzuni.console.Command#getHelp()
     */
    @Override
    public String[] getHelp() {
        return new String[] {"drop"};
    }

    /**
     * @see net.halalaboos.huzuni.console.Command#getDescription()
     */
    @Override
    public String getDescription() {
        return "Drops all items from hotbar.";
    }

    /**
     * @see net.halalaboos.huzuni.console.Command#run(java.lang.String, java.lang.String[])
     */
    @Override
    public void run(String input, String[] args) {
        new DropThread().start();
    }

    private class DropThread extends Thread {

        public void run() {
            for (int o = 36; o < 45; o++) {
                ItemStack item = mc.thePlayer.inventoryContainer.getSlot(o).getStack();
                if (item == null)
                    continue;
                else {
                    clickSlot(o, false);
                    clickSlot(-999, false);
                }
            }
            huzuni.addChatMessage("Dropped!");
        }

        private void clickSlot(int slot, boolean shiftClick) {
            mc.playerController.windowClick(
                    mc.thePlayer.inventoryContainer.windowId, slot, 0, shiftClick ? 1 : 0,
                    mc.thePlayer);
            try {
                Thread.sleep(35L);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
