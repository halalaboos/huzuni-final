/**
 *
 */
package net.halalaboos.huzuni.console.commands;

import net.halalaboos.huzuni.console.Command;
import net.halalaboos.huzuni.mods.Module;

/**
 * @author Halalaboos
 * @since Jul 17, 2013
 */
public class AllOff implements Command {

    /**
     * @see net.halalaboos.huzuni.console.Command#getAliases()
     */
    @Override
    public String[] getAliases() {
        return new String[] {"alloff", "off"};
    }

    /**
     * @see net.halalaboos.huzuni.console.Command#getHelp()
     */
    @Override
    public String[] getHelp() {
        return new String[] {"alloff"};
    }

    /**
     * @see net.halalaboos.huzuni.console.Command#getDescription()
     */
    @Override
    public String getDescription() {
        return "Turns all enabled mods off.";
    }

    /**
     * @see net.halalaboos.huzuni.console.Command#run(java.lang.String, java.lang.String[])
     */
    @Override
    public void run(String input, String[] args) {
        for (Module mod : huzuni.modManager.getList()) {
            if (mod.isEnabled())
                mod.setEnabled(false);
        }
        huzuni.addChatMessage("All mods turned off.");
    }

}
