/**
 *
 */
package net.halalaboos.huzuni.console.commands;

import net.halalaboos.huzuni.console.Command;
import net.halalaboos.huzuni.utils.RenderUtils;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import static org.lwjgl.opengl.GL11.*;

/**
 * @author Halalaboos
 * @since Sep 1, 2013
 */
public class WaypointCommand implements Command {

    /**
     * @see net.halalaboos.huzuni.console.Command#getAliases()
     */
    @Override
    public String[] getAliases() {
        return new String[] {"waypoint", "wp"};
    }

    /**
     * @see net.halalaboos.huzuni.console.Command#getHelp()
     */
    @Override
    public String[] getHelp() {
        return new String[] {"waypoint <name> (At your location)",
                "waypoint <x> <y> <z> <name>"};
    }

    /**
     * @see net.halalaboos.huzuni.console.Command#getDescription()
     */
    @Override
    public String getDescription() {
        return "Traces a waypoint.";
    }

    /**
     * @see net.halalaboos.huzuni.console.Command#run(java.lang.String, java.lang.String[])
     */
    @Override
    public void run(String input, String[] args) {
        if (args.length == 1) {
            String name = input.split(input.split(" ")[0] + " ")[1];
            huzuni.waypointManager.add(new net.halalaboos.huzuni.client.waypoints.Waypoint(name, mc.thePlayer.posX, mc.thePlayer.posY, mc.thePlayer.posZ));
            huzuni.addChatMessage(name + " added at " + (int) mc.thePlayer.posX + ", " + (int) mc.thePlayer.posY + ", " + (int) mc.thePlayer.posZ + ".");
        } else if (args.length >= 4) {
            int x = Integer.parseInt(args[0]),
                    y = Integer.parseInt(args[1]),
                    z = Integer.parseInt(args[2]);
            String name = input.split(x + " " + y + " " + z + " ")[1];
            huzuni.waypointManager.add(new net.halalaboos.huzuni.client.waypoints.Waypoint(name, x, y, z));
            huzuni.addChatMessage(name + " added at " + x + ", " + y + ", " + z + ".");
        }
    }
}
