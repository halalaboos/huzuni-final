package net.halalaboos.huzuni.console;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.helpers.MinecraftHelper;

public interface Command extends MinecraftHelper {

	Huzuni huzuni = Huzuni.instance;
	
    /**
     * @return An array of aliases available for the command.
     */
    public String[] getAliases();

    /**
     * @return An array of helpful information about the command.
     */
    public String[] getHelp();

    /**
     * @return Description of the command.
     */
    public String getDescription();

    /**
     * Handles processing the commands.
     */
    public void run(String input, String[] args);

}
