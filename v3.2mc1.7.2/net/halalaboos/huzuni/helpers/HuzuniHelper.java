/**
 * 
 */
package net.halalaboos.huzuni.helpers;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author Halalaboos
 *
 * @since Sep 30, 2013
 */
public class HuzuniHelper implements Helper {
	private int versionCode = -2;
	
	private final List<String> updates = new CopyOnWriteArrayList<String>();
	

    /**
     * @return 0 If we're all GOOD-TO-GO <br>
     *         1 If we're outdated. <br>
     *         -2
     * @throws IOException
     *         If we're unable to connect.
     */
    public int getUpdateCode() {
        return versionCode;
    }

	public void setUpdateCode(int versionCode) {
		this.versionCode = versionCode;
	}

	public List<String> getUpdates() {
		return updates;
	}
}
