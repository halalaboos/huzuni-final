package net.halalaboos.huzuni.helpers;

import java.util.Set;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MathHelper;
import net.minecraft.util.StatCollector;

import org.lwjgl.util.vector.Vector3f;

public final class BlockHelper implements MinecraftHelper {
	
	private final Block[] blocks = new Block[4096];
	
	public BlockHelper() {
	}
	
	public void setupBlocks() {
		for (Object o : Block.field_149771_c) {
			Block block = (Block) o;
			if (block != null) {
				blocks[Block.func_149682_b(block)] = block;
			}
		}
	}
	
    /**
     * Forces the player to face the specified X, Y, Z positions.
     */
    public void faceBlock(double posX, double posY, double posZ) {
        double diffX = posX - mc.thePlayer.posX;
        double diffZ = posZ - mc.thePlayer.posZ;
        double diffY = posY - (mc.thePlayer.posY + (double) mc.thePlayer.getEyeHeight());


        double dist = (double) MathHelper.sqrt_double(diffX * diffX + diffZ * diffZ);
        float yaw = (float) (Math.atan2(diffZ, diffX) * 180.0D / Math.PI) - 90.0F;
        float pitch = (float) (-(Math.atan2(diffY, dist) * 180.0D / Math.PI));
        mc.thePlayer.rotationPitch = mc.thePlayer.rotationPitch + MathHelper.wrapAngleTo180_float(pitch - mc.thePlayer.rotationPitch);
        mc.thePlayer.rotationYaw = mc.thePlayer.rotationYaw + MathHelper.wrapAngleTo180_float(yaw - mc.thePlayer.rotationYaw);
    }

    /**
     * @return True if you can actually see the vector.
     */
    public boolean rayTrace(double posX, double posY, double posZ) {
        // TODO Fix this.
        return true;
        /*Vec3 player = Vec3.createVectorHelper(mc.thePlayer.posX, mc.thePlayer.posY, mc.thePlayer.posZ),
		block = Vec3.createVectorHelper(posX, posY, posZ);

		MovingObjectPosition result = mc.theWorld.rayTraceBlocks_do_do(player, block, true, true);
		if(result != null) {
			if(result.typeOfHit == EnumMovingObjectType.TILE) {
				return (int) block.xCoord == result.blockX && (int) block.yCoord == result.blockY && (int) block.zCoord == result.blockZ;
			}
		}
		return false;*/

    }

    /**
     * @return True if the coordinates specified are within the distance specified.
     */
    public boolean canReach(double posX, double posY, double posZ, float distance) {
        double blockDistance = getDistance(posX, posY, posZ);
        return blockDistance < distance && blockDistance > -distance;
    }

    public double getDistance(Vector3f vector) {
        return getDistance(vector.x, vector.y, vector.z);
    }

    private double getDistance(double x, double y, double z) {
        float xDiff = (float) (mc.thePlayer.posX - x);
        float yDiff = (float) (mc.thePlayer.posY - y);
        float zDiff = (float) (mc.thePlayer.posZ - z);
        return MathHelper.sqrt_float(xDiff * xDiff + yDiff * yDiff + zDiff * zDiff);
    }
    

    public int getBlockIDFromInput(String input) {
    	if (stringHelper.isInteger(input))
			return Integer.parseInt(input);
		for (int i = 0; i < blocks.length; i++) {
			Block block = blocks[i];
			if (block != null) {
				ItemStack itemStack = new ItemStack(block);
				if (itemStack.getDisplayName().toLowerCase().equals(input.toLowerCase()))
					return i;
			}
		}
		return 0;
    }
    
    public Block getBlockFromInput(String input) {
		if (stringHelper.isInteger(input))
			return blocks[Integer.parseInt(input)];
		for (Block block : blocks) {
			if (block != null) {
				ItemStack itemStack = new ItemStack(block);
				if (itemStack.getItem() != null) {
					String name = itemStack.getDisplayName();
					if (input.equalsIgnoreCase(name)) {
						return block;
					}
				}
			}
		}
		return null;
	}

    public Block getBlock(int id) {
		return blocks[id];
    }
    
    public int getBlockID(Block block) {
    	for (int i = 0; i < blocks.length; i++) {
    		if (blocks[i] != null) {
    			if (blocks[i] == block) {
    				return i;
    			}
    		}
    	}
    	return 0;
    }
    
}
