/**
 * 
 */
package net.halalaboos.lib.crypto;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/**
 * @author Halalaboos
 *
 * @since Oct 9, 2013
 */
public final class CryptoUtils {
	
	/**
	 * Encrypts the file with the cipher given. Outputted into output file.
	 * */
	public static void encryptFile(Cipher cipher, File file, File outputFile) {
		FileOutputStream outputStream = null;
		DataInputStream inputStream = null;
		try {
			outputStream = new FileOutputStream(outputFile);
			inputStream = new DataInputStream(new FileInputStream(file));
			byte[] data = new byte[(int) file.length()];
			inputStream.readFully(data);
			if (data != null) {
				outputStream.write(cipher.doFinal(data));				
			}
			inputStream.close();
			outputStream.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (outputStream != null)
				try {
					outputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			if (inputStream != null)
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
    }
    
	/**
	 * Decrypts the file with the cipher given. Outputted into output file.
	 * */
    public static void decryptFile(Cipher cipher, File file, File outputFile) {
    	FileOutputStream outputStream = null;
		DataInputStream inputStream = null;
		try {
			outputStream = new FileOutputStream(outputFile);
			inputStream = new DataInputStream(new FileInputStream(file));
			byte[] data = new byte[(int) file.length()];
			inputStream.readFully(data);
			if (data != null) {
				outputStream.write(cipher.doFinal(data));				
			}
			inputStream.close();
			outputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (outputStream != null)
				try {
					outputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			if (inputStream != null)
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
    }
    
	
	/**
	 * @return AES Cipher
	 * @param mode
	 * 		Cipher.DECRYPT_MODE or Cipher.ENCRYPT_MODE
	 * */
	public static Cipher getAESCipher(int mode, String key) throws Exception {
		MessageDigest sha = MessageDigest.getInstance("SHA-1");
		byte[] keyBytes = key.getBytes("UTF-8");
		keyBytes = sha.digest(keyBytes);
		keyBytes = Arrays.copyOf(keyBytes, 16);
		SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
		cipher.init(mode, keySpec);
		return cipher;
	}
}
