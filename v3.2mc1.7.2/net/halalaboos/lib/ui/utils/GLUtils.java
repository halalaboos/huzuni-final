/**
 *
 */
package net.halalaboos.lib.ui.utils;

import static org.lwjgl.opengl.GL11.*;
import java.awt.Color;
import java.awt.Font;
import java.util.List;
import net.halalaboos.huzuni.rendering.font.MinecraftFontRenderer;
import net.minecraft.client.Minecraft;
import org.lwjgl.input.Mouse;

/**
 * @author Halalaboos
 * @since Aug 18, 2013
 */
public class GLUtils {
    private static final Minecraft mc = Minecraft.getMinecraft();
    private static final MinecraftFontRenderer fontRenderer = new MinecraftFontRenderer(new Font("Verdana", Font.TRUETYPE_FONT, 18), true, false);
    
    private GLUtils() {
    	
    }
    
    public static void drawString(String text, int x, int y, int hexColor) {
    	fontRenderer.drawString(text, x, y, hexColor);
    }

    public static void drawStringWithShadow(String text, int x, int y, int hexColor) {
    	fontRenderer.drawStringWithShadow(text, x, y, hexColor);
    }

    public static int getStringWidth(String text) {
        return fontRenderer.getStringWidth(text);
    }

    public static int getStringHeight(String text) {
        return fontRenderer.getStringHeight(text);
    }

    public static int getStringHeight() {
        return fontRenderer.getHeight();
    }


    public static int getMouseX() {
        return (int) ((Mouse.getX() * getScreenWidth() / mc.displayWidth));
    }

    public static int getMouseY() {
        return (int) ((getScreenHeight() - Mouse.getY() * getScreenHeight() / mc.displayHeight - 1));
    }

    public static int getScreenWidth() {
        return mc.displayWidth / 2;
    }

    public static int getScreenHeight() {
        return mc.displayHeight / 2;
    }


    public static void drawFilledRect(float x, float y, float x1, float y1) {
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glDisable(GL_TEXTURE_2D);
        glBegin(GL_QUADS);
        glVertex2f(x, y1);
        glVertex2f(x1, y1);
        glVertex2f(x1, y);
        glVertex2f(x, y);
        glEnd();
        glEnable(GL_TEXTURE_2D);
        glDisable(GL_BLEND);
    }

    public static void drawRect(float x, float y, float x1, float y1, float thickness) {
        drawFilledRect(x + thickness, y, x1 - thickness, y + thickness);
        drawFilledRect(x, y, x + thickness, y1);
        drawFilledRect(x1 - thickness, y, x1, y1);
        drawFilledRect(x + thickness, y1 - thickness, x1 - thickness, y1);
    }

    public static void setColor(Color color) {
        setColor((float) color.getRed() / 255F, (float) color.getGreen() / 255F, (float) color.getBlue() / 255F, (float) color.getAlpha() / 255F);
    }

    public static void setColor(float red, float green, float blue, float alpha) {
        glColor4f(red, green, blue, alpha);
    }

    public static void setColorOpaque(float red, float green, float blue) {
        glColor3f(red, green, blue);
    }

    public static void setColorOpaque(Color color) {
        setColorOpaque((float) color.getRed() / 255F, (float) color.getGreen() / 255F, (float) color.getBlue() / 255F);
    }

    public static Color getColorWithAffects(Color color, boolean mouseOver,
                                     boolean mouseDown) {
    	return mouseOver ? (mouseDown ? color.darker() : color.brighter()) : color;
    }
    
    public static List<String> wrapWords(String text, float width) {
    	return fontRenderer.wrapWords(text, width);
    }
}
