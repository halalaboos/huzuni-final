/**
 * 
 */
package net.halalaboos.lib.ui;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.List;

import net.halalaboos.lib.ui.layout.Layout;
import net.halalaboos.lib.ui.theme.Theme;

/**
 * @author Halalaboos
 *
 */
public interface Container {
	
	public void renderComponents(Theme theme, Point mouse);
	
	public Rectangle getArea();
	
	public void setArea(Rectangle area);
	
	public boolean isPointInside(Point point);
	
	public boolean isActive();
	
    public boolean onMousePressed(int x, int y, int buttonId);
 
	public void onKeyTyped(int keyCode, char c);
	
    public void onMouseReleased(int x, int y, int buttonId);

    public List<Component> getComponents();
    
    public void add(Component component);
    
    public void remove(Component component);
    
    public void setMouseOver(boolean mouseOver);
    
    public boolean isMouseOver();
    
    public void setMouseDown(boolean mouseDown);
    
    public boolean isMouseDown();
    
    public void layout();
    
    public void setLayout(Layout layout);
    
    public Layout getLayout();

	/**
	 * 
	 */
	public void update(Point mouse);
	
	public String getTooltip();
    
}
