/**
 *
 */
package net.halalaboos.lib.ui.theme;

import net.halalaboos.lib.ui.Component;
import net.halalaboos.lib.ui.Container;
import java.awt.*;

/**
 * @author Halalaboos
 * @since Aug 30, 2013
 */
public interface ComponentRenderer<T extends Component> {

    public void render(Container container, Point offset, T component, Point mouse);

}
