/**
 * 
 */
package net.halalaboos.lib.ui.layout;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.List;

import net.halalaboos.lib.ui.Component;
import net.halalaboos.lib.ui.Container;

/**
 * @author Halalaboos
 *
 * @since Sep 24, 2013
 */
public class GridLayout implements Layout {
    private final int COMPONENT_PADDING = 1;

	private int columns;
	
	public GridLayout(int columns) {
		this.columns = columns;
	}
	
	/**
	 * @see net.halalaboos.lib.ui.layout.Layout#layoutComponents(net.halalaboos.lib.ui.Container, java.util.List)
	 */
	@Override
	public Rectangle layoutComponents(Container container,
			List<Component> components) {
        Point lastComponent = new Point(2, 2);
        int columnCount = 0, longestInColumn = 0;

        for (Component component : components) {
        	if (columnCount > columns) {
        		lastComponent.x = 2;
        		columnCount = 0;
        	}
        	if (component.getArea().y + component.getArea().height > longestInColumn) {
        		longestInColumn = component.getArea().x + component.getArea().width;
        	}
        	columnCount++;
            // data.setArea(new Rectangle(2, lastComponent.y, componentSize.width, componentSize.height));
        }
        return new Rectangle(0, 0, lastComponent.x + 1, lastComponent.y + 1);
	}

	public int getColumns() {
		return columns;
	}

	public void setColumns(int columns) {
		this.columns = columns;
	}

}
