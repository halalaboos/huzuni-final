/**
 *
 */
package net.halalaboos.lib.ui.layout;

import net.halalaboos.lib.ui.Component;
import net.halalaboos.lib.ui.Container;

import java.awt.*;
import java.util.List;

/**
 * @author Halalaboos
 * @since Aug 27, 2013
 */
public final class DefaultLayout implements Layout {
    private final int COMPONENT_PADDING = 1;

    /**
     * @see net.halalaboos.lib.ui.layout.Layout#layoutComponent(net.halalaboos.lib.ui.Container, net.halalaboos.lib.ui.Component, java.util.List)
     */
    @Override
    public Rectangle layoutComponents(Container container, List<Component> components) {
        int biggestWidth = container.getArea().width;        
    	Point lastComponent = new Point(COMPONENT_PADDING, COMPONENT_PADDING);
    	
        for (Component component : components) {
        	Dimension componentSize = component.getDimensions();
            if (biggestWidth < componentSize.width) {
                biggestWidth = componentSize.width;
            }
        }
        
        
        for (Component component : components) {
        	Dimension componentSize = component.getDimensions();
            component.setArea(new Rectangle(COMPONENT_PADDING, lastComponent.y, biggestWidth - COMPONENT_PADDING, componentSize.height));
            Rectangle componentArea = component.getArea();
            if (componentArea.y + componentSize.height + COMPONENT_PADDING > lastComponent.y)
                lastComponent.y = componentArea.y + componentSize.height + COMPONENT_PADDING;
        }
        return new Rectangle(0, 0, biggestWidth + COMPONENT_PADDING, lastComponent.y);
    }

}
