/**
 * 
 */
package net.halalaboos.lib.ui.components;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.lwjgl.input.Keyboard;

import net.halalaboos.lib.ui.DefaultComponent;
import net.halalaboos.lib.ui.modes.Scrollable;
import net.halalaboos.lib.ui.utils.GLUtils;
import net.minecraft.util.ChatAllowedCharacters;

/**
 * @author Halalaboos
 *
 * @since Oct 19, 2013
 */
public class TextArea extends DefaultComponent implements Scrollable {
	protected List<String> lines = new CopyOnWriteArrayList<String>();
	
	protected boolean editing = false, editable = true, modified = false;
	
	protected int lineIndex = 0, stringIndex = 0, linePadding = 1;
	
    protected float scrollPercentage = 0;
    
    protected Color backgroundColor = new Color(0.3F, 0.3F, 0.3F, 0.75F),
    	textColor = Color.WHITE;
    
	/**
	 * @param dimensions
	 */
	public TextArea(Dimension dimensions) {
		super(dimensions);
		
	}

	/**
	 * @see net.halalaboos.lib.ui.Component#update(java.awt.Rectangle, boolean, boolean)
	 */
	@Override
	public void update(Point mouse) {
		keepSafe();
		wrapLines(area);
	}

	/**
	 * @see net.halalaboos.lib.ui.Component#onClicked(java.awt.Rectangle, boolean, java.awt.Point, int)
	 */
	@Override
	public boolean onMousePressed(int x, int y, int buttonId) {
		if (editable) {
			editing = mouseOver;
			if (editing) {
				if (isNotEmpty()) {
					findClickedIndex(area, x, y);
				}
			}
		}
		return mouseOver;
	}
	
	/**
	 * Attempts to find you just clicked.
	 * */
	private void findClickedIndex(Rectangle area, int x, int y) {
		int yPos = linePadding;
		int scrolledIndex = getScrolledIndex();
		for (int i = 0; scrolledIndex + i < lines.size() && i < getMaxRenderableObjects() && scrolledIndex + i >= 0; i++) {
			String line = lines.get(scrolledIndex + i);
			if (scrolledIndex + i == lineIndex) {
				if (editing)
					line = line.substring(0, stringIndex) + "|" + line.substring(stringIndex);
			}
			int lineWidth = GLUtils.getStringWidth(line),
			lineHeight = GLUtils.getStringHeight(line);
			Rectangle textArea = new Rectangle(area.x + 3, area.y + yPos + 1, lineWidth, lineHeight);
			if (textArea.contains(x, y)) {
				lineIndex = scrolledIndex + i;

				char[] chars = line.toCharArray();
				int charXPos = 0;
				for (int j = 0; j < chars.length; j++) {
					char c = chars[j];
					int charWidth = GLUtils.getStringWidth("" + c);
					int charHeight = GLUtils.getStringHeight("" + c);
					Rectangle charArea = new Rectangle(textArea.x + charXPos, textArea.y, charWidth, charHeight);
					if (charArea.contains(x, y)) {
						int diffFromRight = (charArea.x + charArea.width) - x,
						distFromMiddleOfChar = charArea.width / 2;
						// if we're closer to the right of the character, go to the right side. otherwise we're going to the left side of the character.
						if (diffFromRight > distFromMiddleOfChar) {
							stringIndex = j;
						} else {
							stringIndex = j + 1;
						}
						keepSafe();
						break;
					}
					charXPos+= charWidth;
				}
				break;
			}
			yPos += getLineHeight() + linePadding;
		}
	}
	
	/**
	 * @see net.halalaboos.lib.ui.Component#onReleased(boolean)
	 */
	@Override
	public void onMouseReleased(int x, int y, int buttonId) {
	}
	
	public void renderText(Rectangle area, int padding) {
		if (isNotEmpty()) {
			int yPos = linePadding;
	        int scrolledIndex = getScrolledIndex();
	        for (int i = 0; scrolledIndex + i < lines.size() && i < getMaxRenderableObjects() && scrolledIndex + i >= 0; i++) {
	        	String line = lines.get(scrolledIndex + i);
	        	if (scrolledIndex + i == lineIndex) {
	        		if (editing)
	        			line = line.substring(0, stringIndex) + "|" + line.substring(stringIndex);
	        	}
	        	GLUtils.drawString(line, area.x + 3, area.y + yPos + 1, textColor.getRGB());
	            yPos += getLineHeight() + linePadding;
	        }
		}
	}
	
	@Override
    public void onMouseScroll(Point mouse, int amount) {
        if (hasEnoughToScroll()) {
        	scrollPercentage += ((float) amount / (float) getMaxRenderSize());
        	keepSafe();
        }
    }
	
	/**
	 * @see net.halalaboos.lib.ui.DefaultComponent#onTyped(int)
	 */
	@Override
	public void onKeyTyped(int keyCode, char c) {
		if (lines.isEmpty())
			lines.add("");
		if (editing && editable && isNotEmpty()) {
			if (keyCode == Keyboard.KEY_BACK) {
				minus(1);
				modified = true;
			} else if (keyCode == Keyboard.KEY_LEFT) {
				move(-1);
			} else if (keyCode == Keyboard.KEY_RIGHT) {
				move(1);
			} else if (keyCode == Keyboard.KEY_UP) {
				lineIndex--;
			} else if (keyCode == Keyboard.KEY_DOWN) {
				lineIndex++;
			} else if (keyCode == Keyboard.KEY_RETURN) {
				newLine();
				modified = true;
			} else if (ChatAllowedCharacters.isAllowedCharacter(Keyboard.getEventCharacter())) {
				type(Keyboard.getEventCharacter());
				modified = true;
			}
		}
		keepSafe();
	}
	
	protected void type(char c) {
		// Puts the character in the index we have selected.
		String line = lines.get(lineIndex);
		line = line.substring(0, stringIndex) + c + line.substring(stringIndex);
		stringIndex++;
		lines.set(lineIndex, line);
	}
	
	protected void move(int amount) {
		String line = lines.get(lineIndex);
		stringIndex += amount;
		if (stringIndex > line.length()) {
			lineIndex++;
			if (isLineIndexSafe())
				stringIndex = 0;
			
		} else if (stringIndex < 0) {
			lineIndex--;
			if (isLineIndexSafe()) {
				stringIndex = lines.get(lineIndex).length();
			} else
				stringIndex = 0;
		}
	}
	
	private boolean isLineIndexSafe() {
		return lineIndex >= 0 && lineIndex < lines.size();
	}
	
	protected void newLine() {
		// Grab what's left on our line and push it down.
		String whatsLeftOfLine = lines.get(lineIndex).substring(stringIndex);
		lines.set(lineIndex, lines.get(lineIndex).substring(0, stringIndex));
		lineIndex++;
		lines.add(lineIndex, whatsLeftOfLine);
		stringIndex = 0;
	}
	
	protected void minus(int amount) {
		String line = lines.get(lineIndex);
		// If we go back $amount more and it leaves the length of the line to zero, remove the line and go up. If we can, go to the end of the next line.
		if (line.length() - amount <= 0) {
			lines.remove(lineIndex);
			lineIndex --;
			if (isLineIndexSafe()) {
				stringIndex = lines.get(lineIndex).length();
			} else
				stringIndex = 0;
		} else {
			// Check if we're inside of a line, not at the beginning of a line.
			if (stringIndex > 0) {
				// Minus from the line we're on.
				line = line.substring(0, stringIndex - amount) + line.substring(stringIndex);
				stringIndex -= amount;
				lines.set(lineIndex, line);
			} else {
				// Get current line, set it to nothing, go up to the line above us and merge them together.
				String currentLine = lines.get(lineIndex);
				lines.set(lineIndex, "");
				lineIndex --;
				keepSafe();
				String newLine = lines.get(lineIndex) + currentLine;
				stringIndex = lines.get(lineIndex).length();
				lines.set(lineIndex, newLine);
			}
		}
	}
	
	/**
	 * Logic to prevent us from crashing.
	 * */
	private void keepSafe() {
		if (isNotEmpty()) {
			if (lineIndex >= lines.size())
				lineIndex = lines.size() - 1;
			
			if (lineIndex < 0)
				lineIndex = 0;
			
			String line = lines.get(lineIndex);
			
			if (line.length() > 0) {
				if (stringIndex > line.length())
					stringIndex = line.length();
				
				if (stringIndex < 0)
					stringIndex = 0;
			} else {
				stringIndex = 0;
			}
		} else {
			stringIndex = 0;
			lineIndex = 0;
		}
		if (scrollPercentage > 1)
            scrollPercentage = 1;
        if (scrollPercentage < 0)
            scrollPercentage = 0;
	}

	/**
	 * 
	 */
	private void wrapLines(Rectangle area) {
		if (modified) {
			/*List<String> tempLines = new CopyOnWriteArrayList<String>();
			for (int i = 0; i < lines.size(); i++) {
				String line = lines.get(i);
				List<String> formattedLines = GLUtils.wrapWords(line, area.width);
				for (String formattedLine : formattedLines)
					tempLines.add(formattedLine);
				if (lineIndex == i)
					lineIndex += formattedLines.size() - 1;
			}
			lines = tempLines;
			keepSafe();*/
			modified = false;
		}
	}

	private int getScrolledIndex() {
		int maxSize = getMaxRenderSize();
		if (maxSize > 0)
			return (int) (scrollPercentage * maxSize);
		return 0;
	}

	private boolean hasEnoughToScroll() {
		return getMaxRenderableObjects() < lines.size();
	}

	private int getMaxRenderSize() {
		return lines.size() - getMaxRenderableObjects();
	}

	private int getMaxRenderableObjects() {
		return (int) Math.floor(area.height / (getLineHeight() + linePadding));
	}
	
	protected float getLineHeight() {
		return GLUtils.getStringHeight();
	}

	public List<String> getLines() {
		return lines;
	}

	public void setLines(List<String> lines) {
		this.lines = lines;
	}

	public boolean isEditing() {
		return editing;
	}

	public void setEditing(boolean editing) {
		this.editing = editing;
	}

	public boolean isEditable() {
		return editable;
	}

	public void setEditable(boolean editable) {
		this.editable = editable;
	}

	public Color getBackgroundColor() {
		return backgroundColor;
	}

	public void setBackgroundColor(Color backgroundColor) {
		this.backgroundColor = backgroundColor;
	}

	public Color getTextColor() {
		return textColor;
	}

	public void setTextColor(Color textColor) {
		this.textColor = textColor;
	}
	
	private boolean isNotEmpty() {
		return !lines.isEmpty();
	}
	
	public void addLine(String line) {
		lines.add(line);
		modified = true;
	}
	
	public void clearLines() {
		lines.clear();
	}
	
	public void setLine(int index, String line) {
		lines.set(index, line);
		modified = true;
	}
	
}
