/**
 *
 */
package net.halalaboos.lib.ui.components;

import net.halalaboos.lib.ui.DefaultComponent;

import java.awt.*;

/**
 * @author Halalaboos
 * @since Sep 7, 2013
 */
public class ProgressBar extends DefaultComponent {
    protected Color textColor = Color.WHITE,
            barColor = new Color(0F, 0.5F, 1F, 0.75F),
            backgroundColor = new Color(0.3F, 0.3F, 0.3F, 0.75F);

    protected float minimalVal = 0, maximumVal = 100, progress = 0;

    protected String title, progressWatermark = "%";

    /**
     * @param dimensions
     */
    public ProgressBar(String title) {
        super(new Dimension(110, 13));
        this.title = title;
    }

    /**
     * @see net.halalaboos.lib.ui.Component#update(java.awt.Rectangle, boolean, boolean)
     */
    @Override
    public void update(Point mouse) {
        if (progress > maximumVal) {
            progress = maximumVal;
            this.invokeActionListeners();
        }
        if (progress < minimalVal)
            progress = minimalVal;
    }

    /**
     * @see net.halalaboos.lib.ui.Component#onClicked(java.awt.Rectangle, java.awt.Point, int)
     */
    @Override
    public boolean onMousePressed(int x, int y, int buttonId) {
    	return mouseOver;
    }

    /**
     * @see net.halalaboos.lib.ui.Component#onReleased(boolean)
     */
    @Override
    public void onMouseReleased(int x, int y, int buttonId) {
    }

    /**
     * @see net.halalaboos.lib.ui.DefaultComponent#onTyped(int)
     */
    @Override
    public void onKeyTyped(int keyCode, char c) {
    }

    public Color getTextColor() {
        return textColor;
    }

    public void setTextColor(Color textColor) {
        this.textColor = textColor;
    }

    public Color getBarColor() {
        return barColor;
    }

    public void setBarColor(Color barColor) {
        this.barColor = barColor;
    }

    public Color getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(Color backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getProgressWatermark() {
        return progressWatermark;
    }

    public void setProgressWatermark(String progressWatermark) {
        this.progressWatermark = progressWatermark;
    }

    public float getMinimalVal() {
        return minimalVal;
    }

    public void setMinimalVal(float minimalVal) {
        this.minimalVal = minimalVal;
    }

    public float getMaximumVal() {
        return maximumVal;
    }

    public void setMaximumVal(float maximumVal) {
        this.maximumVal = maximumVal;
    }

    public float getProgress() {
        return progress;
    }

    public void setProgress(float progress) {
        this.progress = progress;
    }

}
