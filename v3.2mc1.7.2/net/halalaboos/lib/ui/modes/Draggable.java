/**
 *
 */
package net.halalaboos.lib.ui.modes;

import java.awt.*;

/**
 * @author Halalaboos
 * @since Aug 16, 2013
 */
public interface Draggable {

	/**
	 * Area in your component that is draggable.
	 * */
    public Rectangle getDraggableArea();

    /**
     * Getter for your dragging field.
     * */
    public boolean isDragging();

    /**
     * Setter for your dragging field.
     * */
    public void setDragging(boolean dragging);

    /**
     * All of your dragging calculations should go in here. Make sure to invoke it in a safe place (i.e. update(java.awt.Point))
     * */
    public void handleDragging(Point mouse);

}
