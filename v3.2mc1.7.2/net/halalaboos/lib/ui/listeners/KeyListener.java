/**
 * 
 */
package net.halalaboos.lib.ui.listeners;

import net.halalaboos.lib.ui.Component;

/**
 * @author Halalaboos
 *
 * @since Sep 23, 2013
 */
public interface KeyListener <T extends Component> {

	public void onKeyTyped(T component, char c, int keyCode);
	
}
