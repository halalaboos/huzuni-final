package net.halalaboos.lib.manager;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Simple class to hold a collection of objects. 'Manages' them.
 */
public class Manager<T> {
    protected final List<T> list = new CopyOnWriteArrayList<T>();

    public T get(int index) {
        return list.get(index);
    }

    public boolean add(T object) {
        return list.add(object);
    }

    public boolean remove(T object) {
        return list.remove(object);
    }

    public boolean contains(T object) {
        return list.contains(object);
    }

    public int length() {
        return list.size();
    }

    public void clear() {
        list.clear();
    }

    public int indexOf(T object) {
        return list.indexOf(object);
    }

    public List<T> getList() {
        return list;
    }

    public void add(int index, T object) {
        list.add(index, object);
    }

    public boolean addAll(Collection<? extends T> collection) {
        return this.list.addAll(collection);
    }

    public boolean addAll(int startIndex, Collection<? extends T> collection) {
        return this.list.addAll(startIndex, collection);
    }

    public boolean containsAll(Collection<?> collection) {
        return this.list.containsAll(collection);
    }

    public boolean isEmpty() {
        return list.isEmpty();
    }

    public Iterator<T> iterator() {
        return list.iterator();
    }

    public int lastIndexOf(Object object) {
        return list.lastIndexOf(object);
    }

    public ListIterator<T> listIterator() {
        return list.listIterator();
    }

    public ListIterator<T> listIterator(int startIndex) {
        return list.listIterator(startIndex);
    }

    public T remove(int index) {
        return list.remove(index);
    }

    public boolean removeAll(Collection<?> collection) {
        return this.list.removeAll(collection);
    }

    public boolean retainAll(Collection<?> collection) {
        return this.list.retainAll(collection);
    }

    public T set(int index, T object) {
        return list.set(index, object);
    }

    public int size() {
        return list.size();
    }

    public List<T> subList(int startIndex, int endIndex) {
        return list.subList(startIndex, endIndex);
    }

    public Object[] toArray() {
        return list.toArray();
    }

    @SuppressWarnings("hiding")
    public <T> T[] toArray(T[] array) {
        return list.toArray(array);
    }

    /**
     * Loads an instance of T from an existing jar file.
     *
     * @param jarFile
     *         The (Expected jar file) we're instantiating our instance of T from.
     * @param classPath
     *         The class path of the T we're attempting to instantiate.
     * @param args
     *         The arguments passed through the constructor of the object.
     */
    public T loadJar(File jarFile, String classPath, Object... args) throws Exception {
        ClassLoader loader = URLClassLoader.newInstance(new URL[] {jarFile.toURI().toURL()}, getClass().getClassLoader());
        Class loadedClass = Class.forName(classPath, true, loader);
        return (T) loadedClass.getConstructor().newInstance(args);
    }
}
