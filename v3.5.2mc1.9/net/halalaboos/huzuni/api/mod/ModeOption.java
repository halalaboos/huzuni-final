package net.halalaboos.huzuni.api.mod;

import org.w3c.dom.Element;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.gui.render.Theme;
import net.halalaboos.huzuni.util.GLUtils;
import net.halalaboos.huzuni.util.MathUtils;
import net.halalaboos.huzuni.util.Timer;

public class ModeOption extends Option {

	private String[] modes;
	
	private int selected = 0;
	
	private final Timer timer = new Timer();
	
	private boolean carot = true;
	
	public ModeOption(String name, String description, String... modes) {
		super(name, description);
		this.modes = modes;
		this.area[3] = 24;
	}

	@Override
	public boolean mouseClicked(int x, int y, int buttonId) {
		return this.isPointInside(x, y);
	}

	@Override
	public void mouseReleased(int x, int y, int buttonId) {
		if (this.isPointInside(x, y)) {
			float size = 12;
			float[] leftArrow = new float[] { area[0], area[1] + size, size, size };
			float[] center = new float[] { area[0] + size + 1, area[1] + size, area[2] - size * 2 - 2, size };
			float[] rightArrow = new float[] { area[0] + area[2] - size, area[1] + size, size, size };
			if (MathUtils.inside(x, y, leftArrow)) {
				swapModes(-1);				
			} else if (MathUtils.inside(x, y, rightArrow)) {
				swapModes(1);
			} else if (MathUtils.inside(x, y, center)) {
				swapModes(1);				
			}
		}
	}

	@Override
	public void render(Theme theme, int index, float[] area, boolean mouseOver, boolean mouseDown) {
		if (mouseOver)
			toggleCarot();
		float size = 12, mouseX = GLUtils.getMouseX(), mouseY = GLUtils.getMouseY();
		Huzuni.drawString(name, area[0] + (area[2] / 2) - (Huzuni.getStringWidth(name) / 2), area[1] + 2, 0xFFFFFF);

		float[] leftArrow = new float[] { area[0], area[1] + size, size, size };
		float[] center = new float[] { area[0] + size + 1, area[1] + size, area[2] - size * 2 - 2, size };
		float[] rightArrow = new float[] { area[0] + area[2] - size, area[1] + size, size, size };
		
		theme.renderSlot(0, 0, index, leftArrow, false, mouseOver && MathUtils.inside(mouseX, mouseY, leftArrow), mouseDown);
		Huzuni.drawString("<", leftArrow[0] + 2, leftArrow[1] + 2, 0xFFFFFF);
		theme.renderSlot(0, 0, index, center, false, mouseOver && MathUtils.inside(mouseX, mouseY, center), mouseDown);
		theme.renderSlot(0, 0, index, rightArrow, false, mouseOver && MathUtils.inside(mouseX, mouseY, rightArrow), mouseDown);
		Huzuni.drawString(">", rightArrow[0] + 2, rightArrow[1] + 2, 0xFFFFFF);
		Huzuni.drawString(modes[selected], center[0] + (center[2] / 2) - (Huzuni.getStringWidth(modes[selected]) / 2), center[1] + 2, 0xFFFFFF);
	}
	
	public boolean toggleCarot() {
		if (timer.hasReach(450)) {
			carot = !carot;
			timer.reset();
		}
        return carot;
	}
	
	private void swapModes(int direction) {
		selected += direction;
		if (selected >= modes.length)
			selected = 0;
		if (selected < 0)
			selected = modes.length - 1;
	}

	public String[] getModes() {
		return modes;
	}

	public void setModes(String[] modes) {
		this.modes = modes;
	}

	public int getSelected() {
		return selected;
	}

	public void setSelected(int selected) {
		this.selected = selected;
	}

	@Override
	public void load(Element element) {
		int value = Integer.parseInt(element.getAttribute(name.replaceAll(" ", "_")));
		this.selected = value;
	}

	@Override
	public void save(Element element) {
		element.setAttribute(name.replaceAll(" ", "_"), "" + selected);
	}

	

}
