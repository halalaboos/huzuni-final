package net.halalaboos.huzuni.event.tick;

import net.halalaboos.huzuni.api.event.Event;

/**
 * @author herlololerbers
 * @version 1.0
 * @since 3/25/14
 */
public class MotionUpdateEvent extends Event {

	private final Type type;

	public MotionUpdateEvent(Type type) {
		this.type = type;
	}

	public Type getType() {
		return type;
	}

	public enum Type {
		PRE,
		POST
	}
}
