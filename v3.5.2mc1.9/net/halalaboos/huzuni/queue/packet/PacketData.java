package net.halalaboos.huzuni.queue.packet;

import net.minecraft.network.Packet;

public class PacketData {
	
	public final Packet packet;
	public final int waitTime;
	
	public PacketData(Packet packet, int waitTime) {
		this.packet = packet;
		this.waitTime = waitTime;
	}
	
}
