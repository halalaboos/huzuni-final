package net.halalaboos.huzuni.queue.rotation;

import net.halalaboos.huzuni.api.request.Applicant;
import net.halalaboos.huzuni.api.request.RequestQueue;
import net.minecraft.client.Minecraft;

public class RotationQueue extends RequestQueue <Applicant<?>, RotationData> {
	
	private static final Minecraft mc = Minecraft.getMinecraft();
	
	private float yaw, pitch;
	
	public void savePlayerRotations() {
		this.yaw = mc.thePlayer.rotationYaw;
		this.pitch = mc.thePlayer.rotationPitch;
	}
	
	public void fakePlayerRotations() {
		if (hasRequest()) {
			mc.thePlayer.rotationYaw = this.data.yaw;
			mc.thePlayer.rotationPitch = this.data.pitch;
			this.requested.onRequestGranted(null);
		}
	}
	
	public void resetPlayerRotations() {
		if (hasRequest()) {
			if (this.data.reset) {
				mc.thePlayer.rotationYaw = this.yaw;
				mc.thePlayer.rotationPitch = this.pitch;
			}
			reset();
		}
	}

	public void onCancelled() {
		if (this.hasRequest())
			this.requested.onRequestCancelled();
		reset();
	}
	
}
