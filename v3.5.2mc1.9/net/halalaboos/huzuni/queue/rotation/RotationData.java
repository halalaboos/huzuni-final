package net.halalaboos.huzuni.queue.rotation;

public class RotationData {
	
	protected float yaw, pitch;
	protected boolean reset = true;
	
	public RotationData(float yaw, float pitch, boolean reset) {
		this.yaw = yaw;
		this.pitch = pitch;
		this.reset = reset;
	}
	
	public RotationData(float yaw, float pitch) {
		this(yaw, pitch, true);
	}
	
	public float getYaw() {
		return yaw;
	}
	
	public void setYaw(float yaw) {
		this.yaw = yaw;
	}
	
	public float getPitch() {
		return pitch;
	}
	
	public void setPitch(float pitch) {
		this.pitch = pitch;
	}
	
	public boolean isReset() {
		return reset;
	}
	
	public void setReset(boolean reset) {
		this.reset = reset;
	}
}
