package net.halalaboos.huzuni.queue;

import java.util.ArrayList;
import java.util.List;

import net.halalaboos.huzuni.util.Timer;
import net.minecraft.client.Minecraft;
import net.minecraft.inventory.ClickType;

public class ClickQueue {
	
    private static final Timer timer = new Timer();

    private static final List<int[]> clicks = new ArrayList<int[]>();

    
    public static void add(int slot, int mouse, int shift) {
    	clicks.add(new int[] { slot, mouse, shift });
    }
    
    public static void add(int windowId, int slot, int mouse, int shift) {
    	clicks.add(new int[] { windowId, slot, mouse, shift });
    }
    
    public static void runClicks() {
    	if (hasQueue()) {
    		if (timer.hasReach(80)) {
    			int[] clickData = clicks.get(0);
    			if (clickData.length > 3)
    				clickSlot(clickData[0], clickData[1], clickData[2], clickData[3]);
    			else
    				clickSlot(Minecraft.getMinecraft().thePlayer.inventoryContainer.windowId, clickData[0], clickData[1], clickData[2]);
    			clicks.remove(clickData);
    			timer.reset();
    		}
    	} else
    		timer.reset();
    }
    
    public static boolean hasQueue() {
    	return !clicks.isEmpty();
    }
    
    private static void clickSlot(int windowId, int slot, int mouse, int shift) {
        Minecraft.getMinecraft().playerController.func_187098_a(windowId, slot, mouse, ClickType.values()[shift], Minecraft.getMinecraft().thePlayer);
    }
}
