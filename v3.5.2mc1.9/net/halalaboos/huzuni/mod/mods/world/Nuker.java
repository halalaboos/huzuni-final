package net.halalaboos.huzuni.mod.mods.world;

import static org.lwjgl.opengl.GL11.glPopMatrix;
import static org.lwjgl.opengl.GL11.glPushMatrix;
import static org.lwjgl.opengl.GL11.glTranslated;
import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.mod.Mod;
import net.halalaboos.huzuni.api.mod.ModeOption;
import net.halalaboos.huzuni.api.mod.Category;
import net.halalaboos.huzuni.api.mod.ToggleOption;
import net.halalaboos.huzuni.api.mod.ValueOption;
import net.halalaboos.huzuni.api.request.Applicant;
import net.halalaboos.huzuni.event.render.RenderEvent;
import net.halalaboos.huzuni.event.render.Renderable;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent;
import net.halalaboos.huzuni.event.tick.UpdateListener;
import net.halalaboos.huzuni.mod.DefaultMod;
import net.halalaboos.huzuni.rendering.Box;
import net.halalaboos.huzuni.util.GLUtils;
import net.halalaboos.huzuni.util.GameUtils;
import net.halalaboos.huzuni.util.Timer;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.init.Blocks;
import net.minecraft.network.play.client.CPacketAnimation;
import net.minecraft.network.play.client.CPacketPlayerDigging;
import net.minecraft.util.EnumFacing;

import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import org.lwjgl.input.Keyboard;

public class Nuker extends DefaultMod implements UpdateListener, Renderable, Applicant<Object> {
	
	private boolean hasRequest = false;
	
	private float curBlockDamage = 0F;
	
	private Block selectedBlock = null, block = null;
	
	private BlockPos position = new BlockPos(-1, -1, -1);
	
	private int blockHitDelay = 0;
	
	private EnumFacing facing = EnumFacing.UP;
	
	private boolean hasBlock = false;

	private final ValueOption radius = new ValueOption("Nuker radius", 2F, 5F, 5F, 1F, "Radius to nuke blocks");
	
	private final ValueOption speed = new ValueOption("Nuker speed", 0F, 3F, 5F, 1F, "Speed at which you will nuke blocks");

	private final ToggleOption renderArea = new ToggleOption("Render area", "Renders around the area you're nuking");
	
	private final Timer blockSelectTimer = new Timer();
	
	private final ModeOption mode;
	
	private final Box normal;
	
	public Nuker() {
		super("Nuker", Keyboard.KEY_L);
		setCategory(Category.WORLD);
		setAliases("nuke");
		setAuthor("Halalaboos");
		setDescription("Mines blocks. Lots of them.");
		setOptions(mode = new ModeOption("Nuker mode", null, "Real", "Instant"), radius, speed, renderArea);
		Huzuni.rotationQueue.registerApplicant(this);
		normal = new Box(AxisAlignedBB.fromBounds(0, 0, 0, 1, 1, 1));
	}

	@Override
	protected void onEnable() {
		Huzuni.registerUpdateListener(this);
		Huzuni.registerRenderable(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterUpdateListener(this);
		Huzuni.unregisterRenderable(this);
		if (hasSelectedBlock()) {
			sendPacket(1);
		}
		reset();
		selectedBlock = null;
	}
	
	private boolean isVisible(RayTraceResult result) {
		return result != null && result.typeOfHit == RayTraceResult.Type.BLOCK && result.getBlockPos().equals(position) && GameUtils.getDistance(position, 0.5F) <= mc.playerController.getBlockReachDistance();
	}
	
	@Override
	public void onPreUpdate(MotionUpdateEvent event) {
		getSelectedBlock();
		hasRequest = Huzuni.rotationQueue.hasPriority(this);
		if (hasBlock = hasSelectedBlock()) {
			RayTraceResult result = rayTrace();
			if (hasRequest && isVisible(result)) {
				facing = result.sideHit;
				GameUtils.requestFaceOffset(this, true, position, 0.5F);
			} else {
				sendPacket(1);
				reset();
			}
			return;
		}
		if (!hasRequest)
			return;
		int radius = (int) this.radius.getValue();
		
		BlockPos furthestPosition = new BlockPos(-1, -1, -1);
		EnumFacing furthestFace = null;
		Block furthestBlock = null;
		
		for (int i = radius; i >= -radius; i--) {
			for (int j = -radius; j <= radius; j++) {
				for (int k = radius; k >= -radius; k--) {
					if ((i == 0 && j <= 0 && k == 0))
						continue;
					position = new BlockPos(mc.thePlayer.posX + i, mc.thePlayer.posY + j, mc.thePlayer.posZ + k);
					block = GameUtils.getBlock(position);

					if ((block != null && GameUtils.getMaterial(position) != Material.air) && block == selectedBlock && GameUtils.getDistance(position, 0.5F) < mc.playerController.getBlockReachDistance()) {
												
						if (mode.getSelected() == 1) {
							facing = EnumFacing.UP;
							if (mc.playerController.isInCreativeMode()) {
								sendPacket(0);
								breakBlock();
							} else {
								sendPacket(0);
								sendPacket(2);
								breakBlock();
							}
						} else if (mode.getSelected() == 0) {
							RayTraceResult result = rayTrace();
							if (result != null && result.typeOfHit == RayTraceResult.Type.BLOCK && result.getBlockPos().equals(position)) {
								facing = result.sideHit;
								if (furthestBlock == null) {
									furthestBlock = block;
									furthestPosition = position;
									furthestFace = facing;
								} else {
									if (GameUtils.getDistance(position, 0.5F) < GameUtils.getDistance(furthestPosition, 0.5F)) {
										furthestBlock = block;
										furthestPosition = position;
										furthestFace = facing;
									}
								}
							}
						}
					}
				}
			}
		}
		
		if (furthestBlock != null) {
			position = furthestPosition;
			facing = furthestFace;
			block = furthestBlock;
			GameUtils.requestFaceOffset(this, true, position, 0.5F);
			curBlockDamage = 0;
			blockHitDelay = 5 - (int) speed.getValue();
			hasBlock = true;
		} else {
			position = new BlockPos(-1, -1, -1);
			block = null;
			facing = null;
			hasBlock = false;
		}
	}
	
	private void getSelectedBlock() {
		if (mc.objectMouseOver != null && mc.objectMouseOver.typeOfHit == RayTraceResult.Type.BLOCK && blockSelectTimer.hasReach(500)) {
			if (mc.gameSettings.keyBindUseItem.isKeyDown()) {
				Block block = GameUtils.getBlock(mc.objectMouseOver.getBlockPos());
				if (block == Blocks.air || block == null)
					selectedBlock = null;
				else {
					if (selectedBlock != block) {
						selectedBlock = block;
						Huzuni.addChatMessage("Now nuking " + GameUtils.getBlockName(selectedBlock));
						blockSelectTimer.reset();
					}
				}
			}
		}
	}

	@Override
	public void onPostUpdate(MotionUpdateEvent event) {
		if (hasBlock && hasRequest) {
			hasRequest = false;
			if (blockHitDelay > 0) {
				blockHitDelay--;
			} else {
	            mc.thePlayer.sendQueue.addToSendQueue(new CPacketAnimation());
				if (block == null) {
					return;
				}
				if (curBlockDamage == 0) {
					sendPacket(0);
					if (mc.playerController.isInCreativeMode() || block.getPlayerRelativeBlockHardness(GameUtils.getState(position), mc.thePlayer, mc.theWorld, position) >= 1) {
						breakBlock();
						reset();
						return;
					}
				}
				curBlockDamage += block.getPlayerRelativeBlockHardness(GameUtils.getState(position), mc.thePlayer, mc.theWorld, position);
				mc.theWorld.sendBlockBreakProgress(mc.thePlayer.getEntityId(), position, (int) (curBlockDamage * 10.0F) - 1);
				if (curBlockDamage > 1) {
					sendPacket(2);
					breakBlock();
					reset();
				}
			}
		}
	}
	
	private boolean hasSelectedBlock() {
		return mode.getSelected() == 0 && selectedBlock != null && block != null && block.getMaterial() != Material.air && block == selectedBlock;
	}
	
	private void breakBlock() {
		GameUtils.breakBlock(position);
	}
	
	private void sendPacket(int mode) {
		CPacketPlayerDigging.Action action = mode == 0 ? CPacketPlayerDigging.Action.START_DESTROY_BLOCK : (mode == 1 ? CPacketPlayerDigging.Action.ABORT_DESTROY_BLOCK : (mode == 2 ? CPacketPlayerDigging.Action.STOP_DESTROY_BLOCK : null));
		mc.getNetHandler().addToSendQueue(new CPacketPlayerDigging(action, position, facing));
	}
	
	private RayTraceResult rayTrace() {
        return GameUtils.rayTraceOffset(position, 0.5F);
	}
	
	private void reset() {
		mc.theWorld.sendBlockBreakProgress(mc.thePlayer.getEntityId(), position, -1);
		block = null;
		position = new BlockPos(-1, -1, -1);
		facing = null;
		curBlockDamage = 0;
		blockHitDelay = 5 - (int) (speed.getValue());
	}

	@Override
	public void onRequestDenied() {
		hasRequest = false;
		if (hasBlock && curBlockDamage > 0F) {
			sendPacket(1);
			reset();
		}
	}

	@Override
	public void onRequestGranted(Object data) {
		hasRequest = true;
	}

	@Override
	public void onRequestCancelled() {
		hasRequest = false;
	}

	@Override
	public Mod getMod() {
		return this;
	}
	
	@Override
	public void render(RenderEvent event) {
		if (!renderArea.isEnabled())
			return;
		int radius = (int) this.radius.getValue();
		GLUtils.glColor(Huzuni.getColorTheme(), 0.1F);
		for (int i = radius; i >= -radius; i--) {
			for (int j = -radius; j <= radius; j++) {
				for (int k = radius; k >= -radius; k--) {
					if ((i == 0 && j <= 0 && k == 0))
						continue;
					BlockPos position = new BlockPos(mc.thePlayer.posX + i, mc.thePlayer.posY + j, mc.thePlayer.posZ + k);
					Block block = GameUtils.getBlock(position);
					
					if ((block != null && block.getMaterial() != Material.air) && GameUtils.getDistance(position, 0.5F) < mc.playerController.getBlockReachDistance()) {
						if (mode.getSelected() == 0) {
							RayTraceResult result = GameUtils.rayTraceOffset(position, 0.5F);
							if (result != null && result.typeOfHit == RayTraceResult.Type.BLOCK && result.getBlockPos().equals(position)) {
								double renderX = position.getX() - mc.getRenderManager().renderPosX;
								double renderY = position.getY() - mc.getRenderManager().renderPosY;
								double renderZ = position.getZ() - mc.getRenderManager().renderPosZ;
								glPushMatrix();
								glTranslated(renderX, renderY, renderZ);
								normal.setOpaque(true);
								normal.render();
								glPopMatrix();
							}
						} else if (mode.getSelected() == 1) {
							double renderX = position.getX() - mc.getRenderManager().renderPosX;
							double renderY = position.getY() - mc.getRenderManager().renderPosY;
							double renderZ = position.getZ() - mc.getRenderManager().renderPosZ;
							glPushMatrix();
							glTranslated(renderX, renderY, renderZ);
							normal.setOpaque(true);
							normal.render();
							glPopMatrix();
						}
					}
				}
			}
		}
		
	}

}