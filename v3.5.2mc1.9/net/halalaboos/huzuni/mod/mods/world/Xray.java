package net.halalaboos.huzuni.mod.mods.world;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.mod.ToggleOption;
import net.halalaboos.huzuni.api.mod.ValueOption;
import net.halalaboos.huzuni.util.FileUtils;
import net.halalaboos.huzuni.util.GameUtils;

import org.lwjgl.input.Keyboard;

import net.halalaboos.huzuni.api.mod.Category;
import net.halalaboos.huzuni.mod.DefaultMod;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class Xray extends DefaultMod {

	public static final Xray instance = new Xray();

	private boolean enabledblocks[] = new boolean[4096];

	private float preGamma;

	private final ValueOption opacity = new ValueOption("Xray opacity", 5F, 70F, 100F, 1F, "Xray block opacity") {
		@Override
		public void mouseReleased(int x, int y, int buttonId) {
			super.mouseReleased(x, y, buttonId);
			if (isEnabled()) {
				mc.renderGlobal.loadRenderers();
			}
		}
	};
	
	private final ToggleOption airBlocks = new ToggleOption("Air pockets", "Render blocks only if there is an air pocket adjacent") {
		@Override
		public void mouseReleased(int x, int y, int buttonId) {
			super.mouseReleased(x, y, buttonId);
			if (isEnabled()) {
				mc.renderGlobal.loadRenderers();
			}
		}
	};
	private final File saveFile = new File(Huzuni.SAVE_DIRECTORY, "XrayBlocks.txt");

	/**
	 * Block.getRenderBlockPass for changing the render block pass
	 * RenderBlocks.renderStandardBlock for rendering xray blocks
	 * Tessellator.setColorOpaque_F for setting block alpha
	 * 		Right now it's just set to 140, we can add in the custom opacity once it's actually
	 * 		configurable in-game.
	 */
	private Xray() {
		super("Xray", Keyboard.KEY_X);
		setDescription("X-ray for blocks.");
		setAuthor("Halalaboos");
		setAliases("x-ray", "wallhack", "x");
		this.setCategory(Category.WORLD);
		setOptions(opacity);
	}

	@Override
	protected void onEnable() {
		preGamma = mc.gameSettings.gammaSetting;
		mc.gameSettings.gammaSetting = 10;
	}

	@Override
	protected void onDisable() {
		mc.gameSettings.gammaSetting = preGamma;
	}

	@Override
	public void toggle() {
		super.toggle();
		Minecraft.getMinecraft().renderGlobal.loadRenderers();
	}

	public boolean isBlockEnabled(int blockId) {
		return enabledblocks[blockId];
	}

	public boolean renderAsNormalBlock(Block block) {
		return isBlockEnabled(Block.getIdFromBlock(block));
	}

	public int getOpacity() {
		int opacity = (int) ((this.opacity.getValue() / 100F) * 255F);
		if (isEnabled() && opacity > 11)
			return opacity;
		else
			return 255;
	}

	private final int ignoredBlocks[] = {
			6, 31, 32, 37, 38, 83, 106, 111, 175
	};

	public boolean shouldIgnore(int id) {
		return Arrays.binarySearch(ignoredBlocks, id) >= 0;
	}
	
	protected boolean hasAirBubble(int x, int y, int z) {
		if (!isAir(x, y, z))
			return false;
		if (!isAir(x, y - 1, z))
			return true;
		else if (!isAir(x, y + 1, z))
			return true;
		else if (!isAir(x - 1, y, z))
			return true;
		else if (!isAir(x + 1, y, z))
			return true;
		else if (!isAir(x, y, z - 1))
			return true;
		else if (!isAir(x, y, z + 1))
			return true;
		else
			return false;
	}
	
	protected boolean isAir(double x, double y, double z) {
		return GameUtils.getBlock((int) x, (int) y, (int) z).getMaterial() == Material.air;
	}

	public boolean contains(int id) {
		return enabledblocks[id];
	}

	public void add(int id) {
		enabledblocks[id] = true;

	}

	public void remove(int id) {
		enabledblocks[id] = false;

	}

	public void clear() {
		for (int i = 0; i < enabledblocks.length; i++)
			enabledblocks[i] = false;
	}

	public void load() {
		if (!saveFile.exists()) {
			try {
				saveFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return;
		}
		clear();
		for (String content : FileUtils.readFile(saveFile)) {
			try {
				enabledblocks[Integer.parseInt(content)] = true;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void save() {
		if (!saveFile.exists())
			try {
				saveFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		List<String> lines = new ArrayList<String>();
		for (int i = 0; i < enabledblocks.length; i++) {
			if (enabledblocks[i])
				lines.add(i + "");
		}
		FileUtils.writeFile(saveFile, lines);
	}

	public void toggleBlock(int blockId) {
		enabledblocks[blockId] = !enabledblocks[blockId];
	}

	public int getMode() {
		return 0;
	}

}
