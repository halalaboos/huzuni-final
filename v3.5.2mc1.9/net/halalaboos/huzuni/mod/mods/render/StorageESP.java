package net.halalaboos.huzuni.mod.mods.render;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.mod.Category;
import net.halalaboos.huzuni.api.mod.ToggleOption;
import net.halalaboos.huzuni.event.render.RenderEvent;
import net.halalaboos.huzuni.event.render.Renderable;
import net.halalaboos.huzuni.mod.DefaultMod;
import net.halalaboos.huzuni.rendering.Box;
import net.halalaboos.huzuni.util.GLUtils;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.tileentity.*;
import net.minecraft.util.math.AxisAlignedBB;
import org.lwjgl.input.Keyboard;
import pw.brudin.huzuni.option.BlockOption;
import static org.lwjgl.opengl.GL11.*;

/**
 * @author brudin
 * @version 1.0
 * @since 4/10/14
 */
public class StorageESP extends DefaultMod implements Renderable {

	private final Box normal, left, right;
	
	private final ToggleOption boxes = new ToggleOption("Boxes", "Render boxes around the blocks"),
			lines = new ToggleOption("Lines", "Render lines towards blocks"), fade = new ToggleOption("Fade", "Make nearby boxes fade out"), border = new ToggleOption("Border", "Apply borders around each block");
	private final BlockOption blocks = new BlockOption(new Block[] { Blocks.chest, Blocks.furnace, Blocks.dropper, Blocks.hopper }, new String[] { "Chests", "Furnaces", "Droppers", "Hoppers" },
			"Block options",
			"Choose which blocks to render");

	public StorageESP() {
		super("Storage ESP", Keyboard.KEY_Y);
		setCategory(Category.RENDER);
		setAliases("chestfinder");
		setVisible(false);
		setDescription("Draws a box around storage-related blocks.");
		normal = new Box(AxisAlignedBB.fromBounds(0, 0, 0, 1, 1, 1));
		left = new Box(AxisAlignedBB.fromBounds(0, 0, 0, 2, 1, 1));
		right = new Box(AxisAlignedBB.fromBounds(0, 0, 0, 1, 1, 2));
		setOptions(boxes, lines, blocks, fade, border);
		boxes.setEnabled(true);
		border.setEnabled(true);
		lines.setEnabled(false);
		blocks.setEnabled(0, true);
	}

	@Override
	protected void onEnable() {
		Huzuni.registerRenderable(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterRenderable(this);
	}

	@Override
	public void render(RenderEvent event) {
		for (Object o : mc.theWorld.loadedTileEntityList) {
			TileEntity tileEntity = (TileEntity)o;
			final double renderX = tileEntity.getPos().getX() - mc.getRenderManager().renderPosX;
			final double renderY = tileEntity.getPos().getY() - mc.getRenderManager().renderPosY;
			final double renderZ = tileEntity.getPos().getZ() - mc.getRenderManager().renderPosZ;
			float dist = (float) mc.thePlayer.getDistance(tileEntity.getPos().getX(), tileEntity.getPos().getY(), tileEntity.getPos().getZ()) / 100F;
			float alpha = dist >= 0.25F ? 0.25F : dist,
					alphaBorder = dist + 0.1F >= 1F ? 1F : 0.1F + dist;
			if (o instanceof TileEntityChest && blocks.isEnabled(0)) {
				final TileEntityChest chest = (TileEntityChest) o;
				if (this.lines.isEnabled()) {
					drawLine(renderX + 0.5F, renderY, renderZ + 0.5F, 1F, chest.getBlockType() == Blocks.trapped_chest ? 0F : 1F, 0F, fade.isEnabled() ? alpha : 0.25F);
				}
				glPushMatrix();
				glTranslated(renderX, renderY, renderZ);
				if (chest.adjacentChestXPos != null) {
					if (boxes.isEnabled()) {
						colorChest(chest, fade.isEnabled() ? alpha : 0.25F);
						left.setOpaque(true);
						left.render();
					}
					if (border.isEnabled()) {
						colorChest(chest, fade.isEnabled() ? alphaBorder : 1F);
						left.setOpaque(false);
						left.render();
					}
				} else if (chest.adjacentChestZPos != null) {
					if (boxes.isEnabled()) {
						colorChest(chest, fade.isEnabled() ? alpha : 0.25F);
						right.setOpaque(true);
						right.render();
					}
					if (border.isEnabled()) {
						colorChest(chest, fade.isEnabled() ? alphaBorder : 1F);
						right.setOpaque(false);
						right.render();
					}
				} else if (chest.adjacentChestXPos == null && chest.adjacentChestZPos == null && chest.adjacentChestXNeg == null && chest.adjacentChestZNeg == null) {
					if (boxes.isEnabled()) {
						colorChest(chest, fade.isEnabled() ? alpha : 0.25F);
						normal.setOpaque(true);
						normal.render();
					}
					if (border.isEnabled()) {
						colorChest(chest, fade.isEnabled() ? alphaBorder : 1F);
						normal.setOpaque(false);
						normal.render();
					}
				}
				glPopMatrix();
			}
			if (blocks.isEnabled(0)) if (o instanceof TileEntityEnderChest) renderBox((TileEntityEnderChest) o, 1, 0.1F, 1);
			if (blocks.isEnabled(1)) if (o instanceof TileEntityFurnace) renderBox((TileEntityFurnace)o, 0.25F, 0.25F, 0.25f);
			if (blocks.isEnabled(2)) if (o instanceof TileEntityDropper || o instanceof TileEntityDispenser) renderBox((TileEntity)o, 0.5F, 0.5F, 0.5f);
			if (blocks.isEnabled(3)) if (o instanceof TileEntityHopper) renderBox((TileEntityHopper)o, 0.25F, 0.25F, 0.25f);
		}
	}

	private void colorChest(TileEntityChest tileEntity, float alpha) {
		if (tileEntity.getBlockType() == Blocks.trapped_chest)
			GLUtils.glColor(1F, 0F, 0F, fade.isEnabled() ? alpha : 0.25F);
		else
			GLUtils.glColor(1F, 1F, 0F, fade.isEnabled() ? alpha : 0.25F);
	}
	
	private void renderBox(TileEntity tileEntity, float r, float g, float b) {
		float dist = (float)mc.thePlayer.getDistance(tileEntity.getPos().getX(), tileEntity.getPos().getY(), tileEntity.getPos().getZ()) / 100F;
		float alpha = dist >= 0.25F ? 0.25F : dist,
				alphaBorder = dist + 0.1F >= 1F ? 1F : 0.1F + dist;
		final double renderX = tileEntity.getPos().getX() - mc.getRenderManager().renderPosX;
		final double renderY = tileEntity.getPos().getY() - mc.getRenderManager().renderPosY;
		final double renderZ = tileEntity.getPos().getZ() - mc.getRenderManager().renderPosZ;
		if (this.lines.isEnabled()) {
			drawLine(renderX + 0.5F, renderY, renderZ + 0.5F, r, g, b, fade.isEnabled() ? alpha : 0.25F);
		}
		glPushMatrix();
		glTranslated(renderX, renderY, renderZ);
		if (boxes.isEnabled()) {
			GLUtils.glColor(r, g, b, fade.isEnabled() ? alpha : 0.25F);
			normal.setOpaque(true);
			normal.render();
		}
		if (border.isEnabled()) {
			GLUtils.glColor(r, g, b, fade.isEnabled() ? alphaBorder : 1F);
			normal.setOpaque(false);
			normal.render();
		}
		glPopMatrix();
	}

	private void drawLine(double x, double y, double z, float r, float g, float b, float a) {
		glPushMatrix();
		GLUtils.glColor(r, g, b, a);
		glBegin(GL_LINES);
		glVertex3d(0, mc.thePlayer.getEyeHeight(), 0);
		glVertex3d(x, y, z);
		glEnd();
		glPopMatrix();
	}
}
