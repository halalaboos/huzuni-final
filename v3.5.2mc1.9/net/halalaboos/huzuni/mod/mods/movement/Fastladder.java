package net.halalaboos.huzuni.mod.mods.movement;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.mod.Category;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent;
import net.halalaboos.huzuni.event.tick.UpdateListener;
import net.halalaboos.huzuni.mod.VipMod;

/**
 * @author Halalaboos
 *
 * @since Oct 10, 2013
 */
public class Fastladder extends VipMod implements UpdateListener {

	public Fastladder() {
		super("Fast Ladder");
		setCategory(Category.MOVEMENT);
		setAuthor("brudin"); //tyga stole it from me >:(
		setDescription("Climbs ladders faster.");
		setVisible(false);
	}

	@Override
	protected void onEnable() {
		Huzuni.registerUpdateListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterUpdateListener(this);
	}

	@Override
	public void onPreUpdate(MotionUpdateEvent event) {
        float multiplier = 0.25F;
        if (mc.thePlayer.isOnLadder() && mc.thePlayer.movementInput.moveForward != 0) {
            mc.thePlayer.motionY = multiplier;
        }
    }

	@Override
	public void onPostUpdate(MotionUpdateEvent event) {
	}

}
