package net.halalaboos.huzuni.mod.mods.movement;

import net.halalaboos.huzuni.api.mod.Category;
import net.halalaboos.huzuni.mod.DefaultMod;

/**
 * @author brudin
 * @version 1.0
 * @since 12:33 AM on 8/6/2014
 */
public class Noslowdown extends DefaultMod {

	/**
	 * EntityPlayerSP.java -> onLivingUpdate()
	 */

	public static final Noslowdown instance = new Noslowdown();

	private Noslowdown() {
		super("No Slowdown");
		setAuthor("brudin");
		setCategory(Category.MOVEMENT);
		setDescription("Prevents slowing down when eating.");
	}

	@Override
	protected void onEnable() {
	}

	@Override
	protected void onDisable() {
	}
}
