package net.halalaboos.huzuni.mod.mods.movement;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.mod.Category;
import net.halalaboos.huzuni.api.mod.ValueOption;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent;
import net.halalaboos.huzuni.event.tick.UpdateListener;
import net.halalaboos.huzuni.mod.DefaultMod;

public final class Step extends DefaultMod implements UpdateListener {

	private final ValueOption height = new ValueOption("Step height", 1F, 1F, 10F, 0.5F, "Max step height");

	public Step() {
		super("Step");
		setCategory(Category.MOVEMENT);
		setDescription("Makes your step height higher.");
		setVisible(false);
		setOptions(height);
	}

	@Override
	protected void onEnable() {
		Huzuni.registerUpdateListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterUpdateListener(this);
		if (mc.thePlayer != null)
			mc.thePlayer.stepHeight = 0.5F;
	}

	@Override
	public void onPreUpdate(MotionUpdateEvent event) {
		mc.thePlayer.stepHeight = height.getValue();
	}

	@Override
	public void onPostUpdate(MotionUpdateEvent event) { }
}
