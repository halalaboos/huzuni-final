package net.halalaboos.huzuni.mod.mods.movement;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.mod.Category;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent;
import net.halalaboos.huzuni.event.tick.UpdateListener;
import net.halalaboos.huzuni.mod.DefaultMod;

import org.lwjgl.input.Keyboard;

/**
 * @author brudin
 * @version 1.0
 * @since 3/25/14
 */
public class Nofall extends DefaultMod implements UpdateListener {

	public Nofall() {
		super("NoFall", Keyboard.KEY_N);
		setAuthor("brudin");
		setCategory(Category.MOVEMENT);
		setDescription("Stops all fall damage.");
	}

	@Override
	protected void onEnable() {
		Huzuni.registerUpdateListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterUpdateListener(this);
	}

	@Override
	public void onPreUpdate(MotionUpdateEvent event) {
		if (mc.thePlayer.fallDistance > 3) {
			mc.thePlayer.onGround = true;
		}
	}

	@Override
	public void onPostUpdate(MotionUpdateEvent event) {
		if (mc.thePlayer.fallDistance > 3) {
			mc.thePlayer.onGround = false;
		}
	}
}
