/**
 * 
 */
package net.halalaboos.huzuni.mod.mods.movement;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.mod.Category;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent;
import net.halalaboos.huzuni.event.tick.UpdateListener;
import net.halalaboos.huzuni.mod.DefaultMod;

/**
 * @author Halalaboos
 *
 * @since Oct 10, 2013
 */
public class Spider extends DefaultMod implements UpdateListener {

	public Spider() {
		super("Spider");
		setCategory(Category.MOVEMENT);
		setDescription("You climb walls like a spider.");
	}

	@Override
	protected void onEnable() {
		Huzuni.registerUpdateListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterUpdateListener(this);
	}

	@Override
	public void onPreUpdate(MotionUpdateEvent event) {
		if (mc.thePlayer.isCollidedHorizontally) {
			mc.thePlayer.motionY = 0.2F;
			mc.thePlayer.onGround = true;
		}
	}

	@Override
	public void onPostUpdate(MotionUpdateEvent event) {
	}

}
