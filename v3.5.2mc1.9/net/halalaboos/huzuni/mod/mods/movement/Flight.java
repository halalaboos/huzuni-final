package net.halalaboos.huzuni.mod.mods.movement;

import net.halalaboos.huzuni.api.mod.ValueOption;
import org.lwjgl.input.Keyboard;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.mod.Category;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent;
import net.halalaboos.huzuni.event.tick.UpdateListener;
import net.halalaboos.huzuni.mod.DefaultMod;

public final class Flight extends DefaultMod implements UpdateListener {

	public static final Flight instance = new Flight();
	
	private final ValueOption speed = new ValueOption("Speed", 1F, 1F, 10F, 0.5F, "Speed you will fly at");

    private Flight() {
        super("Flight", Keyboard.KEY_F);
        setAliases("fly");
        this.setCategory(Category.MOVEMENT);
		this.setOptions(speed);
        setDescription("Allows you to fly.");
        setAuthor("Halalaboos");
    }

    @Override
    public void onPreUpdate(MotionUpdateEvent event) {
        mc.thePlayer.capabilities.isFlying = true;
		if (mc.thePlayer.fallDistance > 3) {
			mc.thePlayer.onGround = true;
		}
    }

    @Override
    public void onPostUpdate(MotionUpdateEvent event) {
		if (mc.thePlayer.fallDistance > 3) {
			mc.thePlayer.onGround = false;
		}
    }

    @Override
    protected void onEnable() {
        Huzuni.registerUpdateListener(this);
    }

    @Override
    protected void onDisable() {
        Huzuni.unregisterUpdateListener(this);
        if (mc.thePlayer != null)
            mc.thePlayer.capabilities.isFlying = false;
    }
    
    public float getSpeed() {
    	return speed.getValue();
    }

}
