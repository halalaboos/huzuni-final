package net.halalaboos.huzuni.mod.mods.chat;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.mod.Category;
import net.halalaboos.huzuni.event.packet.PacketEvent;
import net.halalaboos.huzuni.event.packet.PacketListener;
import net.halalaboos.huzuni.mod.VipMod;
import net.minecraft.network.play.client.CPacketChatMessage;

import java.util.Random;

/**
 * DESCRIPTION
 *
 * @author brudin
 * @version 1.0
 * @since 3/25/14
 */
public class Anticensor extends VipMod implements PacketListener {

	private final Random random = new Random();

	private final String[] badWords = {
			"fuck", "shit", "ass", "bastard", "bitch", "nigger", "cock", "cunt", "dick", "damn", "douche", "penis", "vagina", "gay", "pussy" };


	private final String[] replacementWords = {
			"darn", "poppy", "fudge", "frick", "lint", "fiddle", "feces", "kitty", "dong", "doorknob",
			"rebecca", "lasagna", "loo", "cat", "violin", "bacon", "powder", "refrigerator", "ribbit",
			"fetus", "shrek", "daughter", "waffle", "rosie o donald", "obama", "jonah", "memes", "dank",
			"bill cosby", "trump"
	};

	public Anticensor() {
		super("Anti Censor");
		setAuthor("Halalaboos");
		setCategory(Category.CHAT);
		setDescription("Avoids swear filters the fun way.");
	}

	@Override
	public void onPacket(PacketEvent event, PacketEvent.PacketType type) {
		if (type == PacketEvent.PacketType.SENT) {
			if (event.getPacket() instanceof CPacketChatMessage) {
				CPacketChatMessage packetChatMessage = (CPacketChatMessage) event.getPacket();
				String message = packetChatMessage.getMessage();
				event.setPacket(new CPacketChatMessage(replaceBadWords(message)));
			}
		}
	}

	@Override
	protected void onEnable() {
		Huzuni.registerPacketListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterPacketListener(this);
	}

	private String replaceBadWords(String message) {
		message = message.replaceAll("(?i)douche?|fag(\b|[^e])|[s$]lut|whore|((\b)cum*(\b|(ing)))|hand[^a-z]*j[o0]b|bl[o0]w[^a-z]*j[o0]b|dildo|jack[^a-z ]*[o0]ff|pecker|pen[i!][s$]|b[o0]ner", getRandomReplacement());
		for (String badWord : this.badWords) {
			message = message.replaceAll("(?i)" + badWord, getRandomReplacement());
		}
		return message;
	}

	private String getRandomReplacement() {
		return replacementWords[random.nextInt(replacementWords.length)];
	}
}
