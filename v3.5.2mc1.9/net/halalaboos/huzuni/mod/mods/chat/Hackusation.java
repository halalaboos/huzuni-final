package net.halalaboos.huzuni.mod.mods.chat;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.event.packet.PacketEvent;
import net.halalaboos.huzuni.event.packet.PacketEvent.PacketType;
import net.halalaboos.huzuni.event.packet.PacketListener;
import net.halalaboos.huzuni.mod.DefaultMod;
import net.minecraft.network.play.server.SPacketChat;

public class Hackusation extends DefaultMod implements PacketListener {
	
	private String regex = "(?i)h[a4](cks|x)";
	
	public Hackusation() {
		super("Hackusation");
		
	}

	@Override
	protected void onEnable() {
		Huzuni.registerPacketListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterPacketListener(this);
	}

	@Override
	public void onPacket(PacketEvent event, PacketType type) {
		if (type == PacketType.READ) {
			if (event.getPacket() instanceof SPacketChat) {
				SPacketChat packet = (SPacketChat) event.getPacket();
				String text = packet.getChatComponent().getFormattedText();
				Pattern pattern = Pattern.compile("(?i)h[a4](cks|x|er)");
			    Matcher matcher = pattern.matcher(text);
				if (matcher.find() && text.toLowerCase().contains(mc.thePlayer.getName().toLowerCase())) {
					Huzuni.addChatMessage("regex match!");
				}
			}
		}
	}

}
