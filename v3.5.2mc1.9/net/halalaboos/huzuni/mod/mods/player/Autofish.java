package net.halalaboos.huzuni.mod.mods.player;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.mod.Category;
import net.halalaboos.huzuni.event.packet.PacketEvent;
import net.halalaboos.huzuni.event.packet.PacketListener;
import net.halalaboos.huzuni.mod.DefaultMod;
import net.minecraft.item.ItemFishingRod;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.client.CPacketAnimation;
import net.minecraft.network.play.client.CPacketPlayerBlockPlacement;
import net.minecraft.network.play.server.SPacketEntityVelocity;
import net.minecraft.network.play.server.SPacketSpawnObject;
import net.minecraft.util.EnumHand;

/**
 * @author brudin
 * @version 1.0
 * @since 3/31/14
 */
public class Autofish extends DefaultMod implements PacketListener {

	private int idFish;
		
	public Autofish() {
		super("Auto Fish");
		setAliases("fish");
		setDescription("Automatically fishes for you.");
		setAuthor("brudin");
		setCategory(Category.PLAYER);
	}

	@Override
	public void onPacket(PacketEvent event, PacketEvent.PacketType type) {
		if(type == PacketEvent.PacketType.READ) {
			if(event.getPacket() instanceof SPacketSpawnObject) {
				SPacketSpawnObject packet = (SPacketSpawnObject)event.getPacket();
				if(packet.getEntityID() == 90 && packet.getData() == mc.thePlayer.getEntityId()) {
					idFish = packet.getEntityID();
				}
			} else if(event.getPacket() instanceof SPacketEntityVelocity) {
				SPacketEntityVelocity packetEntityVelocity = (SPacketEntityVelocity)event.getPacket();
				if(packetEntityVelocity.getEntityID() == idFish && packetEntityVelocity.getMotionX() == 0 && packetEntityVelocity.getMotionY() != 0 && packetEntityVelocity.getMotionZ() == 0) {
					recastRod();
					idFish = -420;
				}
			}
		}
	}

	private void recastRod() {
		if (getHand() == null) return;
		CPacketAnimation packetAnimation = new CPacketAnimation();
		CPacketPlayerBlockPlacement packetPlace = new CPacketPlayerBlockPlacement(getHand());

		mc.thePlayer.sendQueue.addToSendQueue(packetAnimation);
		mc.thePlayer.sendQueue.addToSendQueue(packetPlace);

		mc.thePlayer.sendQueue.addToSendQueue(packetAnimation);
		mc.thePlayer.sendQueue.addToSendQueue(packetPlace);
	}

	private EnumHand getHand() {
		ItemStack mainHand = mc.thePlayer.getHeldItem(EnumHand.MAIN_HAND);
		ItemStack offHand = mc.thePlayer.getHeldItem(EnumHand.OFF_HAND);
		if (mainHand != null) if (mainHand.getItem() instanceof ItemFishingRod) return EnumHand.MAIN_HAND;
		if (offHand != null) if (offHand.getItem() instanceof ItemFishingRod) return EnumHand.OFF_HAND;
		return null;
	}

	@Override
	protected void onEnable() {
		Huzuni.registerPacketListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterPacketListener(this);
	}
}
