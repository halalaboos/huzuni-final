package net.halalaboos.huzuni.mod.mods.player;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.mod.ModeOption;
import net.halalaboos.huzuni.api.mod.Category;
import net.halalaboos.huzuni.api.mod.ToggleOption;
import net.halalaboos.huzuni.api.request.SimpleApplicant;
import net.halalaboos.huzuni.event.packet.PacketEvent;
import net.halalaboos.huzuni.event.packet.PacketListener;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent;
import net.halalaboos.huzuni.event.tick.UpdateListener;
import net.halalaboos.huzuni.mod.DefaultMod;
import net.halalaboos.huzuni.queue.rotation.RotationData;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.entity.player.EnumPlayerModelParts;
import net.minecraft.network.Packet;
import net.minecraft.network.play.client.CPacketPlayer;
import net.minecraft.network.play.client.CPacketPlayerBlockPlacement;
import net.minecraft.network.play.client.CPacketPlayerDigging;
import net.minecraft.util.EnumHand;

import java.util.Random;

/**
 * @author brudin
 * @version 1.0
 * @since 4/10/14
 */
public class Retard extends DefaultMod implements UpdateListener, PacketListener {

	/**
	 * oh my god go in twerk mode and go in third person
	 * the default skin is staring into your eyes as he furiously humps the air
	 */

	private ModeOption mode;
	
	private final ToggleOption armSwing = new ToggleOption("Swing arm", "Swings the player's arm randomly.");
	private final ToggleOption skinDerp = new ToggleOption("Skin Derp", "Skin must have 1.8 layers for this");

	private final SimpleApplicant application = new SimpleApplicant(this);
	
	public Retard() {
		super("Retard");
		setAliases("derp");
		setAuthor("Halalaboos");
		setDescription("Makes your player's head go crazy.");
		setCategory(Category.PLAYER);
		setOptions(mode = new ModeOption("Retard mode", null, "Normal", "Headless", "Twerk"), armSwing, skinDerp);
		Huzuni.rotationQueue.registerApplicant(application);
	}

	@Override
	protected void onEnable() {
		Huzuni.registerPacketListener(this);
		Huzuni.registerUpdateListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterPacketListener(this);
		Huzuni.unregisterUpdateListener(this);
	}

	@Override
	public void onPreUpdate(MotionUpdateEvent event) {
		if (!Huzuni.rotationQueue.hasPriority(application))
			return;
		float yaw = mc.thePlayer.rotationYaw;
		float pitch = mc.thePlayer.rotationPitch;

		if (skinDerp.isEnabled()) { //should we add a delay to this?
			EnumPlayerModelParts part = EnumPlayerModelParts.values()[1 + random.nextInt(EnumPlayerModelParts.values().length - 1)];
			mc.gameSettings.switchModelPartEnabled(part);
		}

		if (mode.getSelected() == 0) {
			yaw = random.nextBoolean() ? random.nextInt(180)  : -random.nextInt(180);
			pitch = random.nextBoolean() ? random.nextInt(90)  : -random.nextInt(90);
		}

		if (mode.getSelected() == 2) {
			if (mc.gameSettings.keyBindSneak.isKeyDown()) {
				KeyBinding.setKeyBindState(mc.gameSettings.keyBindSneak.getKeyCode(), false);
			} else {
				KeyBinding.setKeyBindState(mc.gameSettings.keyBindSneak.getKeyCode(), true);
			}
		}
		Huzuni.rotationQueue.request(application, new RotationData(yaw, pitch, true));
		if ((!this.mc.thePlayer.isSprinting())  && (!this.mc.thePlayer.isHandActive() || !this.mc.thePlayer.isSwingInProgress || mc.thePlayer.swingProgress < 0) && armSwing.isEnabled()) {
			if ((!this.mc.thePlayer.isSwingInProgress) && (random.nextBoolean())) {
				this.mc.thePlayer.swingArm(EnumHand.MAIN_HAND);
			}
		}
	}

	@Override
	public void onPostUpdate(MotionUpdateEvent event) {
		
	}

	@Override
	public void onPacket(PacketEvent event, PacketEvent.PacketType type) {
		if (mode.getSelected() == 1) {
			if (type == PacketEvent.PacketType.SENT) {
				Packet packet = event.getPacket();
				if (packet instanceof CPacketPlayerBlockPlacement || packet instanceof CPacketPlayerDigging) {
					event.setPacket(normalHead());
				} else {
					if (packet instanceof CPacketPlayer.C05PacketPlayerLook) {
						event.setPacket(headInbody());
					} else if (packet instanceof CPacketPlayer.C06PacketPlayerPosLook) {
						event.setPacket(headInbody());
					}
				}
			}
		}
	}

	private Packet headInbody() {
		return new CPacketPlayer.C06PacketPlayerPosLook(mc.thePlayer.posX, mc.thePlayer.posY, mc.thePlayer.posZ, mc.thePlayer.rotationYaw, 180, mc.thePlayer.onGround);
	}

	private Packet normalHead() {
		return new CPacketPlayer.C06PacketPlayerPosLook(mc.thePlayer.posX, mc.thePlayer.posY, mc.thePlayer.posZ, mc.thePlayer.rotationYaw, mc.thePlayer.rotationPitch, mc.thePlayer.onGround);
	}

	private Packet changeRotation(float pitch, float yaw) {
		return new CPacketPlayer.C06PacketPlayerPosLook(mc.thePlayer.posX, mc.thePlayer.posY, mc.thePlayer.posZ, yaw, pitch, mc.thePlayer.onGround);
	}
}
