package net.halalaboos.huzuni.mod.mods.combat;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.mod.Category;
import net.halalaboos.huzuni.event.packet.PacketEvent;
import net.halalaboos.huzuni.event.packet.PacketListener;
import net.halalaboos.huzuni.mod.DefaultMod;
import net.minecraft.network.play.server.SPacketUpdateHealth;

/**
 * @author brudin
 * @version 1.0
 * @since 4/10/14
 */
public class Autodisconnect extends DefaultMod implements PacketListener {

	public Autodisconnect() {
		super("Auto Disconnect");
		setDescription("Automatically disconnects when your health is lower than 3 hearts");
		setAuthor("brudin");
		setCategory(Category.COMBAT);
		setVisible(false);
	}

	@Override
	public void onPacket(PacketEvent event, PacketEvent.PacketType type) {
		if(type == PacketEvent.PacketType.READ) {
			if(event.getPacket() instanceof SPacketUpdateHealth) {
				SPacketUpdateHealth packetUpdateHealth = (SPacketUpdateHealth)event.getPacket();
				if(packetUpdateHealth.getHealth() <= 6.0F) {
					mc.thePlayer.getEntityBoundingBox().offset(0, 42000, 0);
					setEnabled(false);
				}
			}
		}
	}

	@Override
	protected void onEnable() {
		Huzuni.registerPacketListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterPacketListener(this);
	}
}
