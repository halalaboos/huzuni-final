package net.halalaboos.huzuni.mod.mods.combat;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.mod.Category;
import net.halalaboos.huzuni.api.mod.ToggleOption;
import net.halalaboos.huzuni.api.mod.ValueOption;
import net.halalaboos.huzuni.api.request.SimpleApplicant;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent;
import net.halalaboos.huzuni.event.tick.UpdateListener;
import net.halalaboos.huzuni.mod.DefaultMod;
import net.halalaboos.huzuni.queue.rotation.RotationData;
import net.halalaboos.huzuni.util.GameUtils;
import net.minecraft.entity.EntityLivingBase;

public class Clickaimbot extends DefaultMod implements UpdateListener {
	
	private EntityLivingBase entity;
	
	private final ValueOption reach = new ValueOption("Reach", 3F, 3.8F, 6F, "Attack reach distance");
    private final ValueOption fov = new ValueOption("FOV", 10F, 25F, 180F, 1F, "FOV to attack entities inside of");
    private final ValueOption yawRate = new ValueOption("Yaw rate", 10F, 45F, 180F, 1F, "Maximum rate the yaw (horizontal rotation) will be updated");

	private final ToggleOption players = new ToggleOption("Players", "Attack players"), 
			mobs = new ToggleOption("Mobs", "Attack mobs"), 
			animals = new ToggleOption("Animals", "Attack animals");
	
	private final SimpleApplicant application = new SimpleApplicant(this);
	
	public Clickaimbot() {
		super("Click Aimbot");
		setAuthor("Halalaboos");
		setDescription("Aims at entities you're trying to kill");
		setCategory(Category.COMBAT);
		setOptions(reach, fov, yawRate, players, mobs, animals);
		players.setEnabled(true);
		Huzuni.rotationQueue.registerApplicant(application);
	}

	@Override
	public void onPreUpdate(MotionUpdateEvent event) {
		if (!Huzuni.rotationQueue.hasPriority(application))
			return;
		entity = GameUtils.getClosestEntity(reach.getValue(), 2.5F, false, mobs.isEnabled(), animals.isEnabled(), players.isEnabled());
		if (entity != null && GameUtils.isWithinDistance(entity, reach.getValue()) && mc.gameSettings.keyBindAttack.isKeyDown()) {
			float[] rotations = GameUtils.getRotationsNeeded(entity);
			float yawDifference = Math.abs(mc.thePlayer.rotationYaw - rotations[0]);
			System.out.println(yawDifference + ", " + (180 - yawDifference));
//			if (yawDifference > 180) {
//				yawDifference = -(180 - yawDifference);
//			}
			float pitchDifference = Math.abs(mc.thePlayer.rotationPitch - rotations[1]);
	        if (rotations != null) {
	        	if (yawDifference <= fov.getValue()) {
	        		float yaw = mc.thePlayer.rotationYaw, pitch = mc.thePlayer.rotationPitch;
					float distanceRatio = (reach.getValue() + 2.5F) / mc.thePlayer.getDistanceToEntity(entity);
					float yawCap = distanceRatio * entity.width * 5;
					if (yawDifference > yawCap) {
		        		float difference = cap(mc.thePlayer.rotationYaw - rotations[0], yawRate.getValue());
		        		if (difference < 0) {
		        			yaw += -difference - yawCap;
		        		} else if (difference > 0) {
		        			yaw -= difference - yawCap;
		        		}
		        	}
		        	float pitchCap = distanceRatio * entity.height * 5;
		        	if (pitchDifference > pitchCap) {
		        		float difference = mc.thePlayer.rotationPitch - rotations[1];
		        		if (difference < 0) {
		        			pitch += -difference - pitchCap;
		        		} else if (difference > 0) {
		        			pitch -= difference - pitchCap;
		        		}
		        	}
	        		Huzuni.rotationQueue.request(application, new RotationData(yaw, pitch, false));
	        	}
	        	
	        }
		}
	}
	
	private float cap(float input, float max) {
		if (input > max)
			input = max;
		if (input < -max)
			input = -max;
		return input;
	}

	@Override
	public void onPostUpdate(MotionUpdateEvent event) {
	}

	@Override
	protected void onEnable() {
		Huzuni.registerUpdateListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterUpdateListener(this);
	}

}
