package net.halalaboos.huzuni.mod.mods.combat;

import net.minecraft.item.ItemShield;
import net.minecraft.item.ItemSword;

import net.minecraft.util.EnumHand;
import org.lwjgl.input.Keyboard;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.mod.Category;
import net.halalaboos.huzuni.api.mod.ModeOption;
import net.halalaboos.huzuni.api.mod.ToggleOption;
import net.halalaboos.huzuni.api.mod.ValueOption;
import net.halalaboos.huzuni.api.request.SimpleApplicant;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent;
import net.halalaboos.huzuni.event.tick.UpdateListener;
import net.halalaboos.huzuni.mod.DefaultMod;
import net.halalaboos.huzuni.queue.rotation.RotationData;
import net.halalaboos.huzuni.util.GameUtils;
import net.halalaboos.huzuni.util.Timer;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;

import java.util.Random;

public class Killaura extends DefaultMod implements UpdateListener {
	
	private EntityLivingBase entity;
	
	private boolean hasRotationCompleted = false;
		
	private final Timer timer = new Timer();
	private Random random = new Random();
	
	private final ValueOption speed = new ValueOption("Speed", 1F, 8F, 15F, "Speed you will attack with");
	private final ValueOption reach = new ValueOption("Reach", 3F, 3.8F, 6F, "Attack reach distance");
	private final ValueOption killfov = new ValueOption("FOV", 10F, 60F, 180F, 1F, "FOV you will attack entities inside of");
	private final ValueOption rotationRate = new ValueOption("Rot. rate", 10F, 45F, 180F, 1F, "Maximum rate the rotation will be updated (the smaller, the smoother)");
	private final ValueOption speedRandomization = new ValueOption("Speed Rndm", 0F, 0F, 4F, 0.1F, "Amount you want to randomize your attack speed (set to 0 for none)");

	private final ModeOption targettingMode = new ModeOption("Targetting", "Attacks the entity closest to...", "Crosshair", "Player");
	
	private final ToggleOption silent  = new ToggleOption("Silent", "Makes the aimbot invisible client-side"), 
			players = new ToggleOption("Players", "Attack players"), 
			mobs = new ToggleOption("Mobs", "Attack mobs"), 
			animals = new ToggleOption("Animals", "Attack animals"),
			invisible = new ToggleOption("Invisible", "Attack invisible entities"),
			trigger  = new ToggleOption("Triggerbot", "Only attack entities you are looking at"),
			block = new ToggleOption("Auto Block", "Automatically block when attacking"),
			randomMisses = new ToggleOption("Random Misses", "Randomly misses to throw off servers");
	
	private final SimpleApplicant<Object> application = new SimpleApplicant<Object>(this);
	
	private String hitOrMiss = null;
	
	public Killaura() {
		super("Kill Aura", Keyboard.KEY_R);
		setAuthor("Halalaboos");
		setAliases("ka", "aura", "ff");
		setDescription("Attacks entities around you.");
		setCategory(Category.COMBAT);
		setOptions(speed, reach, killfov, rotationRate, speedRandomization, targettingMode, silent, players, mobs, animals, invisible, trigger, block, randomMisses);
		players.setEnabled(true);
		invisible.setEnabled(true);
		silent.setEnabled(true);
		Huzuni.rotationQueue.registerApplicant(application);
	}

	@Override
	public void onPreUpdate(MotionUpdateEvent event) {
		if (!(Huzuni.rotationQueue.hasPriority(application))) // even if we don't prioritize over the other request, something will be modifying the rotation, so triggerbot won't work either. 
			return;
		if (trigger.isEnabled()) {
			triggerBot();
			return;
		}
		float yaw = mc.thePlayer.rotationYaw, pitch = mc.thePlayer.rotationPitch;
		entity = targettingMode.getSelected() == 0 ? GameUtils.getClosestEntity(reach.getValue(), 2.5F, invisible.isEnabled(), mobs.isEnabled(), animals.isEnabled(), players.isEnabled()) :
			GameUtils.getClosestEntityToPlayer(mc.thePlayer, reach.getValue(), 2.5F, invisible.isEnabled(), mobs.isEnabled(), animals.isEnabled(), players.isEnabled());
		if (entity != null) {
			float[] rotations = GameUtils.getRotationsNeeded(entity);
			float yawDifference = Math.abs(mc.thePlayer.rotationYaw - rotations[0]);
			float pitchDifference = Math.abs(mc.thePlayer.rotationPitch - rotations[1]);
			if (rotations != null && yawDifference <= killfov.getValue()) {
				boolean yawComplete = yawDifference < rotationRate.getValue(), pitchComplete = pitchDifference < rotationRate.getValue();
				this.hasRotationCompleted = yawComplete && pitchComplete;
				float distanceRatio = (reach.getValue() + 2.5F) / mc.thePlayer.getDistanceToEntity(entity);
				float yawCap = distanceRatio * entity.width * 5;
				
				if (yawDifference > yawCap) {
	        		float difference = cap(mc.thePlayer.rotationYaw - rotations[0], rotationRate.getValue());
	        		if (difference < 0) {
	        			yaw += -difference - (yawComplete ? yawCap : 0);
	        		} else if (difference > 0) {
	        			yaw -= difference - (yawComplete ? yawCap : 0);
	        		}
	        	}
	        	float pitchCap = distanceRatio * entity.height * 5;
	        	if (pitchDifference > pitchCap) {
	        		float difference = cap(mc.thePlayer.rotationPitch - rotations[1], rotationRate.getValue());
	        		if (difference < 0) {
	        			pitch += -difference - (pitchComplete ? pitchCap : 0);
	        		} else if (difference > 0) {
	        			pitch -= difference - (pitchComplete ? pitchCap : 0);
	        		}
	        	}
	    		Huzuni.rotationQueue.request(application, new RotationData(yaw, pitch, silent.isEnabled()));
	        } else
	        	entity = null;
		}
	}
	
	@Override
	public String getRenderName() {
		return super.getName() + (hitOrMiss == null ? "" : " (" + hitOrMiss + ")");
	}
	
	private float cap(float input, float max) {
		if (input > max)
			input = max;
		if (input < -max)
			input = -max;
		return input;
	}
	
	private void triggerBot() {
		if (mc.objectMouseOver != null) {
			Entity entity = mc.objectMouseOver.entityHit;
			if (entity instanceof EntityLivingBase && GameUtils.checkType((EntityLivingBase) entity, invisible.isEnabled(), mobs.isEnabled(), animals.isEnabled(), players.isEnabled()) && GameUtils.isWithinDistance((EntityLivingBase) entity, reach.getValue())) {
				if (!GameUtils.checkTeam((EntityLivingBase)entity)) {
					return;
				}
				this.entity = (EntityLivingBase) entity;
				attackEntity();
			} else
				this.entity = null;
		} else
			this.entity = null;
	}

	public float getSpeed() {
		return this.speed.getValue() - (speedRandomization.getValue() == 0 ? 0 : ((random.nextFloat() * speedRandomization.getValue()) - speedRandomization.getValue() / 2));
	}

	@Override
	public void onPostUpdate(MotionUpdateEvent event) {
		if (application.hasRequest() && hasRotationCompleted) {
			application.reset();
			if (entity != null && GameUtils.isWithinDistance(entity, reach.getValue())) {
				attackEntity();
			}
		}
	}
	
	private void attackEntity() {
		if (timer.hasReach((int) (1000F / (getSpeed())))) {
			if (this.randomMisses.isEnabled()) {
				if (random.nextFloat() >= 0.25F) {
					mc.playerController.attackEntity(mc.thePlayer, entity);
					hitOrMiss = "Hit";
				} else {
					hitOrMiss = "Miss";
				}
			} else {
				mc.playerController.attackEntity(mc.thePlayer, entity);
			}
			if (hasShield() && block.isEnabled()) {
				mc.thePlayer.getHeldItem(EnumHand.OFF_HAND).useItemRightClick(mc.theWorld, mc.thePlayer, EnumHand.OFF_HAND);
			}
			GameUtils.swingItem();
			timer.reset();
		}
	}

	private boolean hasShield() {
		return mc.thePlayer.getHeldItem(EnumHand.OFF_HAND) != null && mc.thePlayer.getHeldItem(EnumHand.OFF_HAND).getItem() instanceof ItemShield;
	}

	@Override
	protected void onEnable() {
		Huzuni.registerUpdateListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterUpdateListener(this);
		hitOrMiss = null;
	}
	
}
