package net.halalaboos.huzuni.scripting.action;

public abstract class Action {

	public abstract void run();
	
}
