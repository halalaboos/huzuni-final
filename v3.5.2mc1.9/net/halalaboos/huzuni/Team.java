package net.halalaboos.huzuni;

import net.minecraft.util.text.TextFormatting;
import org.w3c.dom.Element;

import net.halalaboos.huzuni.api.gui.render.Theme;
import net.halalaboos.huzuni.api.mod.Option;
import net.halalaboos.huzuni.util.GLUtils;
import net.halalaboos.huzuni.util.MathUtils;
import net.minecraft.entity.Entity;

public final class Team extends Option {
	
	private boolean enabled;
	
	private int team = 0;
	
	private final int[] colorCode = new int[32];
    private final String colorcodeIdentifiers = "0123456789abcdef";
    
    private String originalDescription;
    
	public Team() {
		super("Team", "Avoid attacking your teammates by designating your team color");
		this.originalDescription = description;
	}
	
    public void setup() {
    	setupMinecraftColorcodes();
    }
    
	public void up() {
			team++;

		if (team < 0)
			team = 15;
		if (team > 15)
			team = 0;
	}
	
	public void down() {
		team--;
		
		if (team < 0)
			team = 15;
		if (team > 15)
			team = 0;
	}
	
	public boolean isEnabled() {
		return enabled;
	}
	
	public boolean isTeam(Entity entity) {
		return Huzuni.getTeam().hasColor(entity.getDisplayName().getFormattedText());
	}
	
	public boolean hasColor(String text) {
		return text.contains("\247" + colorcodeIdentifiers.charAt(team));
	}
	
	public int getTeamColor(Entity entity) {
		String[] split = entity.getDisplayName().getFormattedText().split("\247");
		for (String parse : split) {
			if (parse.length() > 1) {
				String colorcode = parse.substring(0, 1);
				int index = colorcodeIdentifiers.indexOf(colorcode);
				if (index == -1) {
					continue;
				} else {
					if (parse.substring(1).equalsIgnoreCase(entity.getName()))
						return colorCode[index];
					else
						continue;
				}
			}
		}
		return -1;
	}

	public boolean hasSelected() {
		return team != -1;
	}
	
	private void setupMinecraftColorcodes() {
    	for (int index = 0; index < 32; ++index) {
    		int noClue = (index >> 3 & 1) * 85;
    		int red = (index >> 2 & 1) * 170 + noClue;
    		int green = (index >> 1 & 1) * 170 + noClue;
    		int blue = (index >> 0 & 1) * 170 + noClue;

    		if (index == 6) {
    			red += 85;
    		}

    		if (index >= 16) {
    			red /= 4;
    			green /= 4;
    			blue /= 4;
    		}

    		this.colorCode[index] = (red & 255) << 16 | (green & 255) << 8 | blue & 255;
    	}
    }

	@Override
	public boolean mouseClicked(int x, int y, int buttonId) {
		return this.isPointInside(x, y);
	}

	@Override
	public void mouseReleased(int x, int y, int buttonId) {
		if (this.isPointInside(x, y)) {
			float size = area[3];
			float[] area1 = { area[0] + area[2] - size + 1, area[1] + 1, size - 2, size - 2 };
			if (MathUtils.inside(x, y, area1)) {
				if (buttonId == 0)
					up();
				else if (buttonId == 1)
					down();
				
			} else
				enabled = !enabled;
		}
	}

	@Override
	public void render(Theme theme, int index, float[] area, boolean mouseOver, boolean mouseDown) {
		float size = area[3];
		float[] area1 = { area[0] + area[2] - size + 1, area[1] + 1, size - 2, size - 2 };
		boolean downOnTeam = MathUtils.inside(GLUtils.getMouseX(), GLUtils.getMouseY(), area1);
		
		theme.renderSlot(0, 0, index, area, false, mouseOver && !downOnTeam, mouseDown);
		Huzuni.drawString("Team mode: " + (enabled ? "On" : "Off"), area[0] + 2, area[1] + 2, 0xFFFFFF);
		
		theme.renderSlot(0, 0, index, area1, false, mouseOver && downOnTeam, mouseDown);
		if (mouseOver && downOnTeam)
			this.setDescription(TextFormatting.fromColorIndex(team).getFriendlyName());
		else
			this.setDescription(originalDescription);
		if (team != -1) {
			GLUtils.drawRect(area[0] + area[2] - size + 2, area[1] + 2, area[0] + area[2] - 2, area[1] + size - 2, colorCode[team] + (167 << 24));
		}
	}

	@Override
	public void load(Element element) {
	}

	@Override
	public void save(Element element) {
	}

	public int getColor() {
		return colorCode[team];
	}
	
}
