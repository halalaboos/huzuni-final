package net.halalaboos.huzuni.hook;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.command.CommandManager;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent;
import net.halalaboos.huzuni.gui.screen.HuzuniEditSign;
import net.halalaboos.huzuni.mod.mods.combat.Antiknockback;
import net.halalaboos.huzuni.mod.mods.movement.Flight;
import net.halalaboos.huzuni.mod.mods.movement.Sprint;
import net.halalaboos.huzuni.mod.mods.player.Freecam;
import net.halalaboos.huzuni.mod.mods.world.Autosign;
import net.halalaboos.huzuni.queue.ClickQueue;
import net.halalaboos.huzuni.queue.packet.PacketQueue;
import net.halalaboos.huzuni.scripting.wrapping.ScriptWrapper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.stats.StatFileWriter;
import net.minecraft.tileentity.TileEntitySign;
import net.minecraft.world.World;

/**
 * PlayerControllerMP.func_147493_a replaced with our hook
 * */
public class EntityClientHook extends EntityPlayerSP {

	private Minecraft mc = Minecraft.getMinecraft();

	private static final MotionUpdateEvent preMotionUpdateEvent = new MotionUpdateEvent(MotionUpdateEvent.Type.PRE),
			postMotionUpdateEvent = new MotionUpdateEvent(MotionUpdateEvent.Type.POST);

	public EntityClientHook(Minecraft mc, World world, NetHandlerPlayClient netHandlerPlayClient, StatFileWriter statFileWriter) {
		super(mc, world, netHandlerPlayClient, statFileWriter);
	}
	
	@Override
	public void onUpdate() {
		super.onUpdate();
	}
	
	@Override
	public void moveEntity(double x, double y, double z) {
		if (Huzuni.isEnabled()) {
			if (Sprint.instance.isEnabled()) {
				x *= Sprint.instance.getSpeed();
				z *= Sprint.instance.getSpeed();
			}
			if (Flight.instance.isEnabled()) {
				x *= Flight.instance.getSpeed();
				y *= Flight.instance.getSpeed();
				z *= Flight.instance.getSpeed();
			}
			if (Freecam.instance.isEnabled()) {
				x *= Freecam.instance.getSpeed();
				y *= Freecam.instance.getSpeed();
				z *= Freecam.instance.getSpeed();
			}
		}
		super.moveEntity(x, y, z);
	}
	
	@Override
	public void openEditSign(TileEntitySign sign) {
		if (Huzuni.isEnabled()) {
			if (Autosign.instance.isEnabled())
				Autosign.instance.placeSign(sign);
			else
				this.mc.displayGuiScreen(new HuzuniEditSign(sign));
		} else {
			super.openEditSign(sign);
		}
	}

	@Override
	public void onUpdateWalkingPlayer() {
		if (Huzuni.isEnabled()) {
			// reset the event data
			preMotionUpdateEvent.setCancelled(false);
			postMotionUpdateEvent.setCancelled(false);

			Huzuni.rotationQueue.savePlayerRotations();
			ScriptWrapper.instance.runActions();
			PacketQueue.sendPackets();
			ClickQueue.runClicks();
			if (!ClickQueue.hasQueue()) {
				Huzuni.fireUpdateEvent(preMotionUpdateEvent);				
				if (preMotionUpdateEvent.isCancelled()) {
					Huzuni.rotationQueue.onCancelled();
					return;
				}
				Huzuni.rotationQueue.fakePlayerRotations();
				super.onUpdateWalkingPlayer();
				
				Huzuni.rotationQueue.resetPlayerRotations();
				Huzuni.fireUpdateEvent(postMotionUpdateEvent);
			} else
				super.onUpdateWalkingPlayer();
		} else
			super.onUpdateWalkingPlayer();
		
	}

	@Override
	public void sendChatMessage(String message) {
		if (message.startsWith(CommandManager.COMMAND_PREFIX) && Huzuni.isEnabled()) {
			CommandManager.processCommand(message.substring(CommandManager.COMMAND_PREFIX.length()));
		} else {
			super.sendChatMessage(message);
		}
	}

	@Override
	public boolean isEntityInsideOpaqueBlock() {
		return !(Freecam.instance.isEnabled() && Huzuni.isEnabled()) && super.isEntityInsideOpaqueBlock();
	}

	@Override
	public void onLivingUpdate() {
		super.onLivingUpdate();
	}

	@Override
	protected boolean pushOutOfBlocks(double par1, double par3, double par5) {
		return !(Freecam.instance.isEnabled() && Huzuni.isEnabled()) && super.pushOutOfBlocks(par1, par3, par5);
	}

	@Override
	public boolean isPushedByWater() {
		return Huzuni.isEnabled() ? (Antiknockback.instance.isEnabled() ? Antiknockback.instance.isPushedByWater() : super.isPushedByWater()) : super.isPushedByWater();
	}
}
