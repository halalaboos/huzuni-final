package net.halalaboos.huzuni.util;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class FileUtils {

	private FileUtils() {
		
	}
	
    public static List<String> readFile(File file) {
        List<String> tempList = new ArrayList<String>();

        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(file));
            for (String s; (s = reader.readLine()) != null; ) {
                tempList.add(s);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (reader != null)
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
        return tempList;
    }
    
    public static String readURL(URL url) {
        String temp = "";

        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(url.openConnection().getInputStream()));
            for (String s; (s = reader.readLine()) != null; ) {
            	temp += s;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (reader != null)
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
        return temp;
    }
    
    public static List<String> readURLList(URL url) {
        List<String> temp = new ArrayList<String>();

        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(url.openConnection().getInputStream()));
            for (String s; (s = reader.readLine()) != null; ) {
            	temp.add(s);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (reader != null)
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
        return temp;
    }
    
    
    public static void writeFile(File file, Collection<String> text) {
    	writeFile(file, text.toArray(new String[text.size()]));
    }

    public static void writeFile(File file, String[] text) {
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(new FileWriter(file));
            for (String s : text) {
                writer.println(s);
                writer.flush();
            }
        } catch (Exception localException) {
        } finally {
            if (writer != null)
                writer.close();
        }
    }
}
