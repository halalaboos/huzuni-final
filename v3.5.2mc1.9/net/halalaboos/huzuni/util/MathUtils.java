package net.halalaboos.huzuni.util;

public final class MathUtils {
	
	private MathUtils() {
		
	}
	
	public static boolean inside(float x, float y, float x1, float y1, float width, float height) {
		return x > x1 && x < x1 + width && y > y1 && y < y1 + height;
	}

	public static boolean inside(float x, float y, float[] rect) {
		return x > rect[0] && x < rect[0] + rect[2] && y > rect[1] && y < rect[1] + rect[3];
	}
}
