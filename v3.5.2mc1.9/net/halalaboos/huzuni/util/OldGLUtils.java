package net.halalaboos.huzuni.util;

import static org.lwjgl.opengl.GL11.*;

public final class OldGLUtils {

	private OldGLUtils() {
		
	}


	/**
	 * Draws a gradient rectangle.
	 *
	 * @param x
	 *          The start X
	 * @param y
	 *          The start Y
	 * @param x2
	 *          The end X
	 * @param y2
	 *          The end Y
	 * @param topColor
	 *          The top color
	 * @param bottomColor
	 *          The bottom color
	 */
	public static void drawGradientRect(float x, float y, float x2, float y2, int topColor, int bottomColor) {
		start2D();
		startSmooth();
		glShadeModel(GL_SMOOTH);
		glBegin(GL_QUADS);
		color(topColor);
		glVertex2d(x2, y);
		glVertex2d(x, y);
		color(bottomColor);
		glVertex2d(x, y2);
		glVertex2d(x2, y2);
		glEnd();
		end2D();
		glShadeModel(GL_FLAT);
		endSmooth();
	}
	
	public static void drawBorderedRect(double x, double y, double x1, double y1, float lineWidth, int borderColor, int insideColor) {
		drawRect(x, y, x1, y1, insideColor);

		start2D();
		startSmooth();
		color(borderColor);
		glLineWidth(lineWidth);
		glBegin(GL_LINES);
		glVertex2d(x, y);
		glVertex2d(x, y1);
		glVertex2d(x1, y1);
		glVertex2d(x1, y);
		glVertex2d(x, y);
		glVertex2d(x1, y);
		glVertex2d(x, y1);
		glVertex2d(x1, y1);
		glEnd();
		end2D();
		endSmooth();
	}
	public static void drawBorder(double x, double y, double x1, double y1, float lineWidth, int borderColor) {
		start2D();
		startSmooth();
		color(borderColor);
		glLineWidth(lineWidth);
		glBegin(GL_LINES);
		glVertex2d(x, y);
		glVertex2d(x, y1);
		glVertex2d(x1, y1);
		glVertex2d(x1, y);
		glVertex2d(x, y);
		glVertex2d(x1, y);
		glVertex2d(x, y1);
		glVertex2d(x1, y1);
		glEnd();
		end2D();
		endSmooth();
	}

	public static void drawRect(double x, double y, double x1, double y1, int color) {
		start2D();
		startSmooth();
		glBegin(GL_QUADS);
		color(color);
		glVertex2d(x1, y);
		glVertex2d(x, y);
		glVertex2d(x, y1);
		glVertex2d(x1, y1);
		glEnd();
		endSmooth();
		end2D();
	}

	public static void start2D() {
		glDisable(GL_TEXTURE_2D);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}

	public static void end2D() {
		glEnable(GL_TEXTURE_2D);
	}

	public static void startSmooth() {
		glEnable(GL_LINE_SMOOTH);
		glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	}

	public static void endSmooth() {
		glDisable(GL_LINE_SMOOTH);
		glHint(GL_LINE_SMOOTH_HINT, GL_DONT_CARE);

	}
	
	public static void color(int color) {
		float red = (float)(color >> 16 & 255) / 255.0F;
		float green = (float)(color >> 8 & 255) / 255.0F;
		float blue = (float)(color & 255) / 255.0F;
		float alpha = (float)(color >> 24 & 255) / 255.0F;
		GLUtils.glColor(red, green, blue, alpha);
	}
	
}
