/**
 * 
 */
package net.halalaboos.huzuni.util.threads;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.io.HWID;
import net.halalaboos.huzuni.mod.ModManager;
import net.minecraft.client.Minecraft;
import net.minecraft.util.HttpUtil;

/**
 * @author Halalaboos
 *
 * @since Sep 30, 2013
 */
public final class ThreadCheckVip extends Thread {

	private final String vipURL = "http://halalaboos.net/client/vip.php?";
	
	public ThreadCheckVip() {
		
	}
	
	@Override
	public void run() {
		try {
			String hwid = HWID.getHwid();
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("hwid", hwid);
			String response = HttpUtil.postMap(new URL(vipURL), map, true, Minecraft.getMinecraft().getProxy()).trim();
			Huzuni.setVip(response.equals("1"));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ModManager.loadMods();
		}
	}
}
