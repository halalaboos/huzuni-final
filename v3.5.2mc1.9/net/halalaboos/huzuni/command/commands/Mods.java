package net.halalaboos.huzuni.command.commands;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.console.Command;
import net.halalaboos.huzuni.mod.DefaultMod;
import net.halalaboos.huzuni.mod.ModManager;
import net.minecraft.util.text.TextFormatting;

/**
 * @author brudin
 * @version 1.0
 * @since 4/10/14
 */
public class Mods implements Command {

	/**
	 * Renamed to 'mods' instead of 'Mods' to avoid confusion
	 * since people are dumb and would think 'woa this makese server Mod haha!!'
	 *
	 * Though I should make an actual .pl that would list the server Mods.
	 * That would be neato!
	 */

	@Override
	public String[] getAliases() {
		return new String[] {"mods", "hacks"};
	}

	@Override
	public String[] getHelp() {
		return new String[] { "<none>" };
	}

	@Override
	public String getDescription() {
		return "Lists all of the mods.";
	}

	@Override
	public void run(String input, String[] args) {

		String allMods = "Mods (" + ModManager.getDefaultMods().size() + "): ";

		for(DefaultMod mod : ModManager.getDefaultMods()) {
			allMods += (mod.isEnabled() ? TextFormatting.GREEN : TextFormatting.RED) +
					mod.getName() + TextFormatting.RESET + ", ";
		}

		Huzuni.addChatMessage(allMods.substring(0, allMods.length() - 2));
	}
}
