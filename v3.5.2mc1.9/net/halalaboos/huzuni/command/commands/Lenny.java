package net.halalaboos.huzuni.command.commands;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.console.Command;
import net.minecraft.client.gui.GuiScreen;

public class Lenny implements Command {

	@Override
	public String[] getAliases() {
		return new String[] { "lenny", "len" };
	}

	@Override
	public String[] getHelp() {
		return new String[] {
				"( ͡° ͜ʖ ͡°)"
		};
	}

	@Override
	public String getDescription() {
		return "( ͡° ͜ʖ ͡°)";
	}

	@Override
	public void run(String input, String[] args) {
		GuiScreen.setClipboardString("( ͡° ͜ʖ ͡°)");
		Huzuni.addChatMessage("Copied to clipboard.");
	}

}
