package net.halalaboos.huzuni.command.commands;

import org.lwjgl.input.Keyboard;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.console.Command;
import net.halalaboos.huzuni.util.StringUtils;

public class MacroCmd implements Command {

	@Override
	public String[] getAliases() {
		return new String[] { "macro", "m" };
	}

	@Override
	public String[] getHelp() {
		return new String[] { "macro add <key> <command>", "macro remove <key> <command>", "macro list"};
	}

	@Override
	public String getDescription() {
		return "Bind a command to a key.";
	}

	@Override
	public void run(String input, String[] args) {
		if (args[0].toLowerCase().startsWith("a")) {
			
			int keyCode = Keyboard.getKeyIndex(args[1].toUpperCase());
			Huzuni.MACROS[keyCode] = StringUtils.getAfter(input, 3);
			Huzuni.addChatMessage("'" + StringUtils.getAfter(input, 3) + "' bound to " + args[1] + ".");
		
		} else if (args[0].toLowerCase().startsWith("r") || args[0].toLowerCase().startsWith("d")) {
			
			int keyCode = Keyboard.getKeyIndex(args[1].toUpperCase());
			Huzuni.MACROS[keyCode] = null;
			Huzuni.addChatMessage(args[1] + " removed.");
			
		} else if (args[0].toLowerCase().startsWith("l")) {
			
			for (int i = 0; i < Huzuni.MACROS.length; i++) {
				if (Huzuni.MACROS[i] != null)
					Huzuni.addChatMessage("'" + Huzuni.MACROS[i] + "' is bound to '" + Keyboard.getKeyName(i) + "'.");
			}
		
		}
	}

}
