package net.halalaboos.huzuni.command.commands;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.console.Command;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.StringUtils;

/**
 * @author brudin
 * @version 1.0
 * @since 4/14/14
 */
public class RemoteView implements Command {
	
	private Entity oldPlayer = null;
	
	@Override
	public String[] getAliases() {
		return new String[] {"remoteview", "remote", "rm"};
	}

	@Override
	public String[] getHelp() {
		return new String[] {"remoteview <player>"};
	}

	@Override
	public String getDescription() {
		return "Lets you see what the specified player sees.";
	}

	@Override
	public void run(String input, String[] args) {
		if (oldPlayer != null) {
			Huzuni.addChatMessage("No longer viewing '\2474" + Minecraft.getMinecraft().getRenderViewEntity().getName() + "\247f'.");
			Minecraft.getMinecraft().setRenderViewEntity(oldPlayer);
			oldPlayer = null;
		} else {
			String playerName = args[0];
			for (Object o : Minecraft.getMinecraft().theWorld.loadedEntityList) {
				if(o instanceof EntityPlayer) {
					EntityPlayer entityPlayer = (EntityPlayer)o;
					if (StringUtils.stripControlCodes(entityPlayer.getName()).equalsIgnoreCase(playerName)) {
						oldPlayer = Minecraft.getMinecraft().getRenderViewEntity();
						Minecraft.getMinecraft().setRenderViewEntity(entityPlayer);
						Huzuni.addChatMessage("Now viewing '\2472" + entityPlayer.getName() + "\247f'.");
						return;
					}
				}
			}
		}
	}
}
