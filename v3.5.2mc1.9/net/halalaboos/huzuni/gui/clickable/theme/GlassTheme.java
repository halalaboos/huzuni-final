package net.halalaboos.huzuni.gui.clickable.theme;

import static org.lwjgl.opengl.GL11.GL_LINES;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glLineWidth;
import static org.lwjgl.opengl.GL11.glVertex2d;

import java.awt.Color;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.gui.clickable.button.Button;
import net.halalaboos.huzuni.gui.clickable.theme.DefaultTheme;
import net.halalaboos.huzuni.util.GLUtils;
import net.halalaboos.huzuni.util.OldGLUtils;

public class GlassTheme extends DefaultTheme {

	private final Color transparentTopGradient = new Color(170, 170, 170, 112), solidTopGradient = new Color(68, 68, 68, 221);
	private final Color transparentBottomGradient = new Color(136, 136, 136, 112), solidBottomGradient = new Color(51, 51, 51, 221);

	@Override
	public String getName() {
		return "Glass";
	}
	
	private void renderButton(float x, float y, float x1, float y1, boolean on, boolean hover, boolean down, boolean transparent) {
		int gradientTop = GLUtils.getColorWithAffects(on ? Huzuni.getColorTheme() : (transparent ? transparentTopGradient : solidTopGradient), hover, down).getRGB(),
		gradientBottom = GLUtils.getColorWithAffects(on ? Huzuni.getColorTheme() : (transparent ? transparentBottomGradient : solidBottomGradient), hover, down).getRGB();
		
		GLUtils.drawBorderedRect(x, y, x1, y1, 0.5F, 0, 0xFF666666);
		OldGLUtils.drawGradientRect(x + 0.5F, y + 0.5F, x1 - 0.5F, y1 - 0.5F, gradientTop, gradientBottom);
		if (hover) {
			OldGLUtils.drawRect(x + 0.5F, y + 0.5F, x1 - 0.5F, y1 - 0.5F, 0x1F000000);
			OldGLUtils.drawBorder(x, y, x1, y1, 1F, Huzuni.getColorTheme().getRGB());
		}
	}

	@Override
	protected void renderButtonRect(float x, float y, float x1, float y1, boolean highlight, boolean mouseOver, boolean mouseDown) {
		renderButton(x, y, x1, y1, highlight, mouseOver, mouseDown, true);
	}

	@Override
	protected void renderWindowRect(float x, float y, float x1, float y1, float tabHeight, float lineWidth) {
		OldGLUtils.drawRect(x, y, x1, y + tabHeight, Huzuni.getColorTheme().getRGB());
		OldGLUtils.drawBorder(x, y, x1, y1, 0.5F, 0xFF666666);
		OldGLUtils.start2D();
		OldGLUtils.startSmooth();
		OldGLUtils.color(0xFF000000);
		glLineWidth(0.5F);
		glBegin(GL_LINES);
		glVertex2d(x, y + tabHeight);
		glVertex2d(x1, y + tabHeight);
		glEnd();
		OldGLUtils.end2D();
		OldGLUtils.endSmooth();
	}

	@Override
	protected int getButtonTextColor(Button button) {
		return 0xFFFFFF;
	}

	@Override
	protected int getWindowTextColor() {
		return 0xFFFFFF;
	}
	
	@Override
	protected int getTooltipTextColor() {
		return 0xFFFFFF;
	}

	@Override
	protected void renderBackgroundRect(float x, float y, float x1, float y1, boolean highlight, boolean mouseOver, boolean mouseDown) {
		renderButton(x, y, x1, y1, highlight, mouseOver, mouseDown, true);
	}

	@Override
	protected void renderDropdownBackgroundRect(float x, float y, float x1,
			float y1, boolean highlight, boolean mouseOver, boolean mouseDown) {
		renderButton(x, y, x1, y1, highlight, mouseOver, mouseDown, false);
	}
}
