package net.halalaboos.huzuni.gui.clickable.theme;

import static org.lwjgl.opengl.GL11.*;

import java.awt.Color;

import net.halalaboos.huzuni.gui.clickable.button.Button;
import net.halalaboos.huzuni.util.GLUtils;
import net.halalaboos.huzuni.util.OldGLUtils;

public class BmcTheme extends DefaultTheme {

	private final Color inside = new Color(70, 70, 70, 151),
			border = new Color(93, 93, 93, 151),
			buttons = new Color(0.4F, 0.4F, 0.4F, 0.75F);

	@Override
	protected void renderButtonRect(float x, float y, float x1, float y1, boolean highlight, boolean mouseOver, boolean mouseDown) {
		Color inside = GLUtils.getColorWithAffects(highlight ? buttons : this.inside, mouseOver, mouseDown);
		OldGLUtils.drawRect(x, y, x1, y1, inside.getRGB());
		OldGLUtils.start2D();
		OldGLUtils.color(highlight ? inside.getRGB() : border.getRGB());
		glLineWidth(1.0F);
		glBegin(GL_LINES);
		glVertex2d(x, y);
		glVertex2d(x, y1);
		glVertex2d(x1, y1);
		glVertex2d(x1, y);
		glVertex2d(x, y);
		glVertex2d(x1, y);
		glVertex2d(x, y1);
		glVertex2d(x1, y1);
		glEnd();
		OldGLUtils.end2D();
	}
	
	@Override
	protected int getButtonTextColor(Button button) {
		return button.isHighlight() ? 0x00FF00 : 0xFF0000;
	}

	@Override
	protected int getWindowTextColor() {
		return 0xFFFFFF;
	}
	
	@Override
	protected void renderWindowRect(float x, float y, float x1, float y1, float tabHeight, float lineWidth) {
		OldGLUtils.drawBorderedRect(x, y, x1, y1, lineWidth, border.getRGB(), inside.getRGB());
	}
	

	@Override
	public String getName() {
		return "BMC";
	}

	@Override
	protected int getTooltipTextColor() {
		return 0xFFFFFF;
	}

	@Override
	protected void renderBackgroundRect(float x, float y, float x1, float y1, boolean highlight, boolean mouseOver, boolean mouseDown) {
		Color inside = GLUtils.getColorWithAffects(highlight ? buttons : this.inside, mouseOver, mouseDown);
		OldGLUtils.drawRect(x, y, x1, y1, inside.getRGB());
		OldGLUtils.start2D();
		OldGLUtils.color(highlight ? inside.getRGB() : border.getRGB());
		glLineWidth(1.0F);
		glBegin(GL_LINES);
		glVertex2d(x, y);
		glVertex2d(x, y1);
		glVertex2d(x1, y1);
		glVertex2d(x1, y);
		glVertex2d(x, y);
		glVertex2d(x1, y);
		glVertex2d(x, y1);
		glVertex2d(x1, y1);
		glEnd();
		OldGLUtils.end2D();
	}

	@Override
	protected void renderDropdownBackgroundRect(float x, float y, float x1,
			float y1, boolean highlight, boolean mouseOver, boolean mouseDown) {
		this.renderBackgroundRect(x, y, x1, y1, highlight, mouseOver, mouseDown);
	}

}
