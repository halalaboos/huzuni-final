package net.halalaboos.huzuni.gui.clickable;

import java.util.ArrayList;
import java.util.List;

import net.halalaboos.huzuni.api.gui.Component;
import net.halalaboos.huzuni.api.gui.Container;
import net.halalaboos.huzuni.api.gui.event.ActionListener;
import net.halalaboos.huzuni.api.gui.event.KeyListener;

public abstract class DefaultComponent implements Component {

	private float x, y, width, height, offsetX, offsetY;

	private boolean mouseOver, mouseDown;
	
	private final List<ActionListener> listeners = new ArrayList<ActionListener>();
	
	private final List<KeyListener> keyListeners = new ArrayList<KeyListener>();

	private Container parent;

	private String tooltip;
	
	public DefaultComponent() {
	}
	
	@Override
	public void update() {
	}

	@Override
	public boolean mouseClicked(int x, int y, int buttonId) {
		return isPointInside(x, y);
	}

	@Override
	public void mouseReleased(int x, int y, int buttonId) {
		if (isPointInside(x, y)) {
			invokeActionListeners();
		}
	}
	
	@Override
	public void layout(float x, float y) {
		this.x = x;
		this.y = y;
		onPositionUpdated();
	}
	
	@Override
	public void layout(float x, float y, float width, float height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		onPositionUpdated();
	}

	@Override
	public float getX() {
		return x;
	}

	@Override
	public float getY() {
		return y;
	}

	@Override
	public float getWidth() {
		return width;
	}

	@Override
	public float getHeight() {
		return height;
	}

	@Override
	public boolean isPointInside(int x, int y) {
		return x > offsetX + this.x && x < offsetX + this.x + this.width && y > offsetY + this.y && y < offsetY + this.y + this.height;
	}

	@Override
	public boolean isMouseOver() {
		return mouseOver;
	}

	@Override
	public boolean isMouseDown() {
		return mouseDown;
	}

	@Override
	public void setMouseOver(boolean mouseOver) {
		this.mouseOver = mouseOver;
	}

	@Override
	public void setMouseDown(boolean mouseDown) {
		this.mouseDown = mouseDown;
	}

	@Override
	public Container getParent() {
		return parent;
	}

	@Override
	public void setParent(Container parent) {
		this.parent = parent;
	}
	
	@Override
	public String getTooltip() {
		return tooltip;
	}

	@Override
	public void setTooltip(String tooltip) {
		this.tooltip = tooltip;
	}

	@Override
	public void updateContainerOffset(float containerX, float containerY) {
		this.offsetX = containerX;
		this.offsetY = containerY;
	}
	
	@Override
	public void addActionListener(ActionListener listener) {
		listeners.add(listener);
	}

	@Override
	public void removeActionListener(ActionListener listener) {
		listeners.remove(listener);
	}

	protected void invokeActionListeners() {
		for (int i = 0; i < listeners.size(); i++) {
			listeners.get(i).onAction(this);
		}
	}
	
	public void addKeyListener(KeyListener listener) {
		keyListeners.add(listener);
	}

	public void removeKeyListener(KeyListener listener) {
		keyListeners.remove(listener);
	}
	
	protected void invokeKeyListeners(int keyCode, char c) {
		for (int i = 0; i < keyListeners.size(); i++) {
			keyListeners.get(i).keyTyped(this, keyCode, c);
		}
	}
	
	protected void onPositionUpdated() {
	}
	
	@Override
	public void keyTyped(int keyCode, char c) {
		invokeKeyListeners(keyCode, c);
	}

	@Override
	public void setX(float x) {
		this.x = x;
	}

	@Override
	public void setY(float y) {
		this.y = y;
	}

	@Override
	public void setWidth(float width) {
		this.width = width;
	}

	@Override
	public void setHeight(float height) {
		this.height = height;
	}

	protected float getOffsetX() {
		return offsetX;
	}

	protected float getOffsetY() {
		return offsetY;
	}

}
