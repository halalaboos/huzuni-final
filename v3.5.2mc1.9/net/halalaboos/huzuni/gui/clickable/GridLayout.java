package net.halalaboos.huzuni.gui.clickable;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.List;

import net.halalaboos.huzuni.api.gui.Component;
import net.halalaboos.huzuni.api.gui.Container;
import net.halalaboos.huzuni.api.gui.layout.Layout;

public class GridLayout implements Layout {
    private final int COMPONENT_PADDING = 1;

    private int rows, columns;
    
    public GridLayout(int rows, int columns) {
    	this.rows = rows;
    	this.columns = columns;
    }
    
    @Override
    public Rectangle layout(Container container, List<Component> components) {
    	int biggestTotalWidth = (int) container.getWidth(),
    			biggestTotalHeight = COMPONENT_PADDING;
    	
        for (int i = 0; i < components.size(); i++) {
        	Component component = components.get(i);
        	if (i + 1 < components.size()) {
        		Component component2 = components.get(i + 1);
        		int biggestWidth = (int) (component.getWidth() > component2.getWidth() ? component.getWidth() : component2.getWidth());
        		int biggestHeight = (int) (component.getHeight() > component2.getHeight() ? component.getHeight() : component2.getHeight());

        		component.layout(COMPONENT_PADDING, biggestTotalHeight, biggestWidth - COMPONENT_PADDING, biggestHeight);
        		component2.layout(biggestWidth + COMPONENT_PADDING, biggestTotalHeight, biggestWidth, biggestHeight);
        		
        			biggestTotalHeight += biggestHeight + COMPONENT_PADDING;
        		
        		if (biggestWidth > biggestTotalWidth)
        			biggestTotalWidth = biggestWidth;
        		i++;
        		continue;
        	} else {
        		if (component.getWidth() > biggestTotalWidth)
        			biggestTotalWidth = (int) (component.getWidth());
        		
        		component.layout(COMPONENT_PADDING, biggestTotalHeight, biggestTotalWidth - COMPONENT_PADDING, component.getHeight());
        		biggestTotalHeight += (int) (component.getHeight() + COMPONENT_PADDING);
        	}
        }
//        
//        for (Component component : components) {
//        	component.layout(COMPONENT_PADDING, lastComponent.y, biggestWidth - COMPONENT_PADDING, component.getHeight());
//            if ((int) (lastComponent.y + component.getHeight() + COMPONENT_PADDING) > lastComponent.y)
//                lastComponent.y = (int) (lastComponent.y + component.getHeight() + COMPONENT_PADDING);
//        }
        return new Rectangle(0, 0, (int) (biggestTotalWidth + COMPONENT_PADDING) * 2, biggestTotalHeight);
    }

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public int getColumns() {
		return columns;
	}

	public void setColumns(int columns) {
		this.columns = columns;
	}

}