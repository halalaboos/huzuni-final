package net.halalaboos.huzuni.gui.screen.alt;

import com.mojang.authlib.exceptions.AuthenticationException;
import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.util.GLUtils;
import net.halalaboos.huzuni.util.LoginUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.util.Session;
import net.minecraft.util.text.TextFormatting;
import org.lwjgl.input.Keyboard;
import pw.brudin.huzuni.util.screen.PanoramaRenderer;

import javax.swing.*;
import java.io.IOException;

/**
 * @author brudin
 * @version 1.0
 * @since 6:48 PM on 7/16/2014
 */
public class HuzuniLogin extends GuiScreen {

	private GuiTextField username;
	private GuiTextField password;
	private AltManager prevScreen;
	private boolean addToList;
	private static String lastLogin = "Try to login...";
	private PanoramaRenderer panoramaRenderer;

	/**
	 * An account login screen, is also used to add accounts to the alt list. Just specify.
	 */
	public HuzuniLogin(AltManager prevScreen, boolean addToList) {
		this.prevScreen = prevScreen;
		this.addToList = addToList;

	}

	@Override
	public void updateScreen() {
		this.username.updateCursorCounter();
		this.password.updateCursorCounter();
		panoramaRenderer.panoramaTick();
	}

	@Override
	public void initGui() {
		panoramaRenderer = new PanoramaRenderer(width, height);
		panoramaRenderer.init();
		Keyboard.enableRepeatEvents(true);
		this.buttonList.add(new GuiButton(0, this.width / 2 - 100, this.height / 4 + 96 + 12, 200, 20, addToList ? "Add" : "Login"));
		this.buttonList.add(new GuiButton(1, this.width / 2 - 100, this.height / 4 + 120 + 12, 200, 20, "Cancel"));
		this.username = new GuiTextField(420, this.fontRendererObj, this.width / 2 - 100, 66, 200, 20);
		this.username.setFocused(true);
		this.username.setMaxStringLength(420);
		this.username.setText("");
		this.password = new GuiTextField(422, this.fontRendererObj, this.width / 2 - 100, 106, 200, 20);
		this.password.setText("");
		this.password.setMaxStringLength(1000);
	}

	@Override
	protected void actionPerformed(GuiButton par1GuiButton) {
		if (par1GuiButton.id == 0) {
			if (addToList) {
				mc.displayGuiScreen(prevScreen);
				
				prevScreen.addAccount(username.getText() + ":" + password.getText());
			} else {
				if (username.getText().length() > 0) {
					if (password.getText().length() > 0) {
						SwingUtilities.invokeLater(new Runnable() {
							@Override
							public void run() {
								try {
									Session session = LoginUtils.loginToMinecraft(username.getText(), password.getText());
									Minecraft.getMinecraft().setSession(session);
									prevScreen.setLastLoginStatus(TextFormatting.GRAY + "Success" + TextFormatting.RESET);
								} catch (AuthenticationException e) {
									prevScreen.setLastLoginStatus(TextFormatting.GRAY +  e.getMessage() + TextFormatting.RESET);
								}
							}
						});
					} else {
						mc.setSession(new Session(username.getText(), mc.getSession().getPlayerID(), mc.getSession().getToken(), "LEGACY"));
						lastLogin = "Changed name to: " + username.getText();
						prevScreen.setLastLoginStatus(TextFormatting.GRAY + "Success" + TextFormatting.RESET);
					}
				}
			}
		} else if (par1GuiButton.id == 1) {
			mc.displayGuiScreen(prevScreen);
		}
	}

	@Override
	public void keyTyped(char ch, int key) throws IOException {
		super.keyTyped(ch, key);
		this.username.textboxKeyTyped(ch, key);
		this.password.textboxKeyTyped(ch, key);

		if (key == Keyboard.KEY_TAB) {
			if (this.username.isFocused()) {
				this.username.setFocused(false);
				this.password.setFocused(true);
			} else {
				this.username.setFocused(true);
				this.password.setFocused(false);
			}
		}

		if(key == Keyboard.KEY_RETURN) {
			this.actionPerformed((GuiButton) this.buttonList.get(0));
		}

		if (key == 13) {
			this.actionPerformed((GuiButton) this.buttonList.get(0));
		}

		((GuiButton) this.buttonList.get(0)).enabled = this.username.getText().length() >= 2;
	}

	@Override
	protected void mouseClicked(int par1, int par2, int par3) throws IOException {
		super.mouseClicked(par1, par2, par3);
		this.username.mouseClicked(par1, par2, par3);
		this.password.mouseClicked(par1, par2, par3);
	}

	public void drawScreen(int par1, int par2, float par3) {
		panoramaRenderer.renderSkybox(par1, par2, par3);
		GLUtils.drawRect(0, 0, width, 16, 0x4F000000);
		String status = AltManager.getLastLoginStatus();
		Huzuni.drawString((addToList ? "ADD AN ALT" : "DIRECT LOGIN").toUpperCase() + " // " + status, 2, 3, -1);
		Huzuni.drawString("Username", this.width / 2 - 100, 53, 10526880);
		Huzuni.drawString("Password", this.width / 2 - 100, 94, 10526880);
		this.username.drawTextBox();
		this.password.drawTextBox();
		super.drawScreen(par1, par2, par3);
		((GuiButton) this.buttonList.get(0)).enabled = this.username.getText().length() >= 2;
		panoramaRenderer.renderFade();
	}
}
