package net.halalaboos.huzuni.gui.screen;

import net.halalaboos.huzuni.mod.mods.world.Autosign;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiEditSign;
import net.minecraft.tileentity.TileEntitySign;

import java.io.IOException;

public class HuzuniEditSign extends GuiEditSign {

	private final TileEntitySign sign;
	
	 public HuzuniEditSign(TileEntitySign sign) {
		super(sign);
		this.sign = sign;
		
	}

	public void initGui() {
		super.initGui();
        this.buttonList.add(new GuiButton(1, this.width / 2 - 100, this.height / 4 + 142, "Set Auto Sign Text"));

	}
	
	protected void actionPerformed(GuiButton button) throws IOException
    {
		if (button.enabled)
			if (button.id == 1)
				Autosign.instance.setLines(sign.signText);
		
        super.actionPerformed(button);
    }
}
