package net.halalaboos.huzuni.gui.screen.builder;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.gui.render.Theme;
import net.halalaboos.huzuni.gui.clickable.slot.SlotComponent;
import net.halalaboos.huzuni.gui.clickable.window.Window;

public class WindowSlot extends SlotComponent<Window> {

	public WindowSlot() {
		super(0, 0);
		
	}

	@Override
	protected void onReleased(int index, Window component, int mouseX,
			int mouseY) {
	}

	@Override
	protected void onClicked(int index, Window component, float[] area,
			int mouseX, int mouseY, int buttonId) {
	}

	@Override
	protected void render(Theme theme, int index, Window window, float[] area, boolean highlight, boolean mouseOver,
			boolean mouseDown) {
		theme.renderSlot(this.getOffsetX(), this.getOffsetY(), index, area, highlight, mouseOver, mouseDown);
		Huzuni.drawString(window.getTitle(), area[0] + 2, area[1] + 2, 0xFFFFFF);
	}
}
