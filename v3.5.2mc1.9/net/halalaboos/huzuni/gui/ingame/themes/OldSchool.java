/**
 *
 */
package net.halalaboos.huzuni.gui.ingame.themes;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.gui.ingame.IngameTheme;
import net.halalaboos.huzuni.mod.DefaultMod;
import net.halalaboos.huzuni.mod.ModManager;
import net.minecraft.client.Minecraft;

/**
 * @author Halalaboos
 * @since Aug 3, 2013
 */
public class OldSchool implements IngameTheme {

    /**
     * @see net.halalaboos.huzuni.ui.ingame.IngameTheme#render(net.minecraft.src.Minecraft, int, int)
     */
    @Override
    public void render(Minecraft mc, int screenWidth, int screenHeight) {
        mc.fontRenderer.drawStringWithShadow(Huzuni.TITLE + "\247r v" + Huzuni.VERSION, 2, 2, 0xFFFFFF);

        int yPos = 12;
        for (DefaultMod mod : ModManager.getDefaultMods()) {
            if (mod.isBound()) {
                mc.fontRenderer.drawStringWithShadow("[" + mod.getKeyName() + "] " + mod.getRenderName(), 2, yPos, mod.isEnabled() ? 0x339900 : 0x990000);
                yPos += 11;
            }
        }
    }

    /**
     * @see net.halalaboos.huzuni.ui.ingame.IngameTheme#getName()
     */
    @Override
    public String getName() {
        return "Old School";
    }

    /**
     * @see net.halalaboos.huzuni.ui.ingame.IngameTheme#onKeyTyped(int)
     */
    @Override
    public void onKeyTyped(int keyCode) {
    }

}
