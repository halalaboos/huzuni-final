/**
 *
 */
package net.halalaboos.huzuni.gui.ingame.themes;

import net.halalaboos.huzuni.gui.ingame.IngameTheme;
import net.minecraft.client.Minecraft;

/**
 * @author Halalaboos
 * @since Aug 3, 2013
 */
public class None implements IngameTheme {

    /**
     * @see net.halalaboos.huzuni.ui.ingame.IngameTheme#render(net.minecraft.src.Minecraft, int, int)
     */
    @Override
    public void render(Minecraft mc, int screenWidth, int screenHeight) {
    }

    /**
     * @see net.halalaboos.huzuni.ui.ingame.IngameTheme#getName()
     */
    @Override
    public String getName() {
        return "None";
    }

    /**
     * @see net.halalaboos.huzuni.ui.ingame.IngameTheme#onKeyTyped(int)
     */
    @Override
    public void onKeyTyped(int keyCode) {
    }

}
