package net.halalaboos.huzuni.gui;

import java.awt.Color;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import pw.brudin.huzuni.theme.click.BrudinTheme;
import pw.brudin.huzuni.theme.ingame.*;
import net.halalaboos.huzuni.*;
import net.halalaboos.huzuni.api.gui.Component;
import net.halalaboos.huzuni.api.gui.render.*;
import net.halalaboos.huzuni.api.mod.Mod;
import net.halalaboos.huzuni.api.mod.Category;
import net.halalaboos.huzuni.gui.clickable.*;
import net.halalaboos.huzuni.gui.clickable.button.Button;
import net.halalaboos.huzuni.gui.clickable.button.ModButton;
import net.halalaboos.huzuni.gui.clickable.theme.*;
import net.halalaboos.huzuni.gui.clickable.window.Window;
import net.halalaboos.huzuni.gui.ingame.*;
import net.halalaboos.huzuni.gui.ingame.themes.*;
import net.halalaboos.huzuni.gui.ingame.themes.tabbed.Tabbed;
import net.halalaboos.huzuni.gui.xray.XrayWindow;
import net.halalaboos.huzuni.mod.ModManager;
import net.halalaboos.huzuni.util.FileUtils;
import net.halalaboos.huzuni.util.GLUtils;

public final class GuiManager {

	private static final Theme[] windowThemes = new Theme[] {
		new HuzuniTheme(),
		new BmcTheme(),
		new BrudinTheme(),
		new DarkTheme(),
		new BlastoffTheme(),
		new GlassTheme(),
		new FlashyTheme(),
		new DefaultTheme() {

			@Override
			public String getName() {
				return "Windows";
			}

			@Override
			protected int getButtonTextColor(Button button) {
				return 0xFFFFFF;
			}

			@Override
			protected int getWindowTextColor() {
				return 0xFFFFFF;
			}

			@Override
			protected int getTooltipTextColor() {
				return GLUtils.getColor(getThemeOpaque());
			}

			@Override
			protected void renderButtonRect(float x, float y, float x1, float y1, boolean highlight, boolean mouseOver, boolean mouseDown) {
				Color background = new Color(0.2F, 0.2F, 0.2F, 1F);
				GLUtils.glColor(GLUtils.getColorWithAffects(highlight ? getThemeOpaque() : background, mouseOver, mouseDown));
				GLUtils.drawRect(x, y, x1, y1);
			}

			@Override
			protected void renderBackgroundRect(float x, float y, float x1, float y1, boolean highlight, boolean mouseOver, boolean mouseDown) {
				GLUtils.drawRect(x, y, x1, y1, 0xDD000000);
			}

			@Override
			protected void renderWindowRect(float x, float y, float x1, float y1, float tabHeight, float lineWidth) {
				int color = GLUtils.getColor(getThemeOpaque());
				GLUtils.drawRect(x, y, x1, y + tabHeight, color);

				GLUtils.drawBorderedRect(x, y + tabHeight, x1, y1, lineWidth, 0xFF000000, color);
			}
			
			private Color getThemeOpaque() {
				return new Color(Huzuni.getColorTheme().getRed(),Huzuni.getColorTheme().getGreen(),Huzuni.getColorTheme().getBlue());
			}

			@Override
			protected void renderDropdownBackgroundRect(float x, float y, float x1, float y1, boolean highlight, boolean mouseOver, boolean mouseDown) {
				this.renderBackgroundRect(x, y, x1, y1, highlight, mouseOver, mouseDown);
			}
			
		}
	};
	
	private static final IngameTheme[] ingameThemes = new IngameTheme[] {
		new Default(),
		new OldSchool(),
		new Informative(),
		new Nyan(),
		new Paged(),
		new PigPlus(),
		new Morbid(),
		new Skidded(),
		new Simple(),
		new Iridium(),
		new Compact(),
		new Reliant(),
		new JClient(),
		new Reverse(),
		new Tabbed(),
		new None()
	};
	
	private static final ContainerManager<Window> windowManager = new ContainerManager<Window>(windowThemes[0]);
	
	private static int selectedTheme = 0;
	
	public static final File WINDOW_FILE = new File(Huzuni.SAVE_DIRECTORY, "Windows.txt");
	
	private GuiManager() {
		
	}
	

	public static void resetWindows() {
		windowManager.getContainers().clear();
		loadDefaultWindows();
		HubWindow hubWindow = new HubWindow();
		windowManager.add(hubWindow);
		XrayWindow xrayWindow = new XrayWindow();
		windowManager.add(xrayWindow);
		windowManager.add(new ColorWindow());
		hubWindow.copyWindows(windowManager.getContainers());
		layoutWindows();
	}
	
	public static boolean isCustomWindow(Window window) {
		return (window instanceof ColorWindow) || (window instanceof HubWindow) || (window instanceof XrayWindow);
	}
	
	private static void loadDefaultWindows() {
		for (Category type : Category.values()) {
			Window window = new Window(type.formalName);
			for (Mod mod : ModManager.getMods()) {
				if (mod.getCategory() != null && mod.getCategory().equals(type)) {
					final ModButton button = new ModButton(mod);
					window.add(button);
				}
			}
			window.layout();
			if (window.getComponents().size() > 0)
				windowManager.add(window);
		}
	}
	
	public static void layoutWindows() {
		int x = 2, y = 2, amountPerRow = 4, count = 0;
		float[] heights = new float[amountPerRow];
		for (Window window : windowManager.getContainers()) {
			count++;			
			window.setX(x);
			window.setY(y + heights[count - 1]);
			heights[count - 1] = window.getY() + window.getHeight() + window.getTabHeight();
			x = (int) (window.getX() + window.getWidth() + 2);
			if (count >= amountPerRow) {
				count = 0;
				x = 2;
			}
		}
	}
	
	public static void loadWindows() {
		windowManager.setTheme(getSelectedWindowTheme());
		selectedTheme = Huzuni.CONFIG.getInt("Ingame Theme");
		List<String> lines = FileUtils.readFile(WINDOW_FILE);
		if (lines.isEmpty()) {
			loadDefaultWindows();
			HubWindow hubWindow = new HubWindow();
			windowManager.add(hubWindow);
			XrayWindow xrayWindow = new XrayWindow();
			windowManager.add(xrayWindow);
			windowManager.add(new ColorWindow());
			hubWindow.copyWindows(windowManager.getContainers());
			layoutWindows();
		} else {
			loadDefaultWindows();
			HubWindow hubWindow = new HubWindow();
			windowManager.add(hubWindow);
			XrayWindow xrayWindow = new XrayWindow();
			windowManager.add(xrayWindow);
			windowManager.add(new ColorWindow());
			Window window = null;
			for (int i = 0; i < lines.size(); i++) {
				String line = lines.get(i);
				if (line.startsWith("WINDOW\247i")) {
					String[] split = line.split("\247i");
					if (window != null)
						window.layout();
					window = getWindow(split[1]);
					if (!windowManager.contains(window))
						windowManager.add(window);
					window.setX(Integer.parseInt(split[2]));
					window.setY(Integer.parseInt(split[3]));
					window.setActive(Boolean.parseBoolean(split[4]));
				}
				/*if (line.startsWith("BUTTON\247i")) {
					String[] split = line.split("\247i");
					Mod mod = ModManager.getMod(split[1]);
					if (window != null && mod != null)
						window.add(new ModButton(mod));
				}*/
			}
			if (window != null)
				window.layout();
			hubWindow.copyWindows(windowManager.getContainers());
		}
	}
	
	public static void saveWindows() {
		List<String> lines = new ArrayList<String>();
		for (Window window : windowManager.getContainers()) {
			lines.add("WINDOW\247i" + window.getTitle() + "\247i" + (int) window.getX() + "\247i" + (int) window.getY() + "\247i" + window.isActive());
			/*for (Component component : window.getComponents()) {
				if (component instanceof ModButton) {
					ModButton button = (ModButton) component;
					lines.add("BUTTON\247i" + button.getMod().getName());
				}
			}*/
		}
		FileUtils.writeFile(WINDOW_FILE, lines);
	}
	
	public static Window getWindow(String title) {
		for (Window window : windowManager.getContainers()) {
			if (window.getTitle().equalsIgnoreCase(title))
				return window;
		}
		return new Window(title);
	}
	
	public static ContainerManager<Window> getWindowManager() {
		return windowManager;
	}
	
	public static List<Window> getWindows() {
		return windowManager.getContainers();
	}
	
	public static List<Window> getUserWindows() {
		List<Window> windows = new ArrayList<Window>();
		for (Window window : windowManager.getContainers()) {
			if (!isCustomWindow(window))
				windows.add(window);
		}
		return windows;
	}

	public static IngameTheme getIngameTheme() {
		try {
			return ingameThemes[selectedTheme];
		} catch(Exception e) {
			selectedTheme = 0;
			return ingameThemes[selectedTheme];
		}
	}

	private static Theme getSelectedWindowTheme() {
		return windowThemes[getSelectedWindowThemeId()];
	}
	
	public static int getSelectedWindowThemeId() {
		return Huzuni.CONFIG.getInt("Window Theme");
	}
	
	public static void setSelectedWindowTheme(int theme) {
		Huzuni.CONFIG.put("Window Theme", theme);
		windowManager.setTheme(windowThemes[theme]);
	}

	public static Theme[] getWindowThemes() {
		return windowThemes;
	}

	public static IngameTheme[] getIngameThemes() {
		return ingameThemes;
	}

	public static void nextTheme() {
        int maxSize = ingameThemes.length;
		selectedTheme++;
		if (selectedTheme >= maxSize)
			selectedTheme = 0;
		if (selectedTheme < 0)
			selectedTheme = maxSize - 1;
		Huzuni.CONFIG.put("Ingame Theme", selectedTheme);

    }
	
}
