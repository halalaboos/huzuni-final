package pw.brudin.huzuni.theme.ingame;

import net.halalaboos.huzuni.gui.ingame.IngameTheme;
import net.halalaboos.huzuni.mod.DefaultMod;
import net.halalaboos.huzuni.mod.ModManager;
import net.halalaboos.huzuni.util.GLUtils;
import net.minecraft.client.Minecraft;

import org.lwjgl.opengl.GL11;

/**
 * A new Informative theme since the current one was
 * gettin' a little old.
 *
 * @author brudin
 * @version 1.0
 * @since 1/25/14
 */
public class Compact implements IngameTheme {

	private Minecraft mc = Minecraft.getMinecraft();

	@Override
	public void render(Minecraft mc, int screenWidth, int screenHeight) {
		String coords = "XYZ: " + (int) mc.thePlayer.posX + ", " + (int) mc.thePlayer.posY + ", " + (int) mc.thePlayer.posZ;
		int width = 80, yPos = 14;

		if(mc.fontRenderer.getStringWidth(coords) > width - 10) {
			width = mc.fontRenderer.getStringWidth(coords) + 10;
		}

		if(getWidest() > width - 10) {
			width = getWidest() + 10;
		}

		if(!(getArrayHeight() <= 0)) {
			GLUtils.drawRect(0, 0, width + 1, getArrayHeight() + 10, 0xFF232729);
			GLUtils.drawRect(0, 0, width, 9 + getArrayHeight(), 0xFF2e3436);
		}

		drawGradientRect(0, 10, width, 13, 0xFF191c1e, 0);
		GLUtils.drawRect(0, 0, width, 10F, 0xFF191c1e);
		mc.fontRenderer.drawStringWithShadow(coords, 2, 2, 0xFFd8d8d8);

		for (DefaultMod mod : ModManager.getDefaultMods()) {
			if (mod.getVisible() && mod.isEnabled()) {
				mc.fontRenderer.drawStringWithShadow(mod.getRenderName(), 2, yPos, 0xFFbfbfbf);
				yPos += 10;
			}
		}
	}

	private int getArrayHeight() {
		int yPos = 4;
		for (DefaultMod mod : ModManager.getDefaultMods()) {
			if (mod.getVisible() && mod.isEnabled()) {
				yPos += 10;
			}
		}
		return yPos;
	}

	private int getWidest() {
		int width = 50;
		for (DefaultMod module : ModManager.getDefaultMods()) {
			if (module.isEnabled()) {
				if (module.getVisible()) {
					if (width < mc.fontRenderer.getStringWidth(module.getRenderName()) + 6)
						width = mc.fontRenderer.getStringWidth(module.getRenderName()) + 6;
				}
			}
		}
		return width;
	}

	@Override
	public String getName() {
		return "Compact";
	}

	@Override
	public void onKeyTyped(int keyCode) {

	}

	private void drawGradientRect(float x, float y, float x2, float y2, int col1, int col2) {
		float f = (float)(col1 >> 24 & 0xFF) / 255F;
		float f1 = (float)(col1 >> 16 & 0xFF) / 255F;
		float f2 = (float)(col1 >> 8 & 0xFF) / 255F;
		float f3 = (float)(col1 & 0xFF) / 255F;

		float f4 = (float)(col2 >> 24 & 0xFF) / 255F;
		float f5 = (float)(col2 >> 16 & 0xFF) / 255F;
		float f6 = (float)(col2 >> 8 & 0xFF) / 255F;
		float f7 = (float)(col2 & 0xFF) / 255F;

		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_ALPHA_TEST);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glShadeModel(GL11.GL_SMOOTH);

		GL11.glPushMatrix();
		GL11.glBegin(GL11.GL_QUADS);
		GL11.glColor4f(f1, f2, f3, f);
		GL11.glVertex2d(x2, y);
		GL11.glVertex2d(x, y);

		GL11.glColor4f(f5, f6, f7, f4);
		GL11.glVertex2d(x, y2);
		GL11.glVertex2d(x2, y2);
		GL11.glEnd();
		GL11.glPopMatrix();

		GL11.glEnable(GL11.GL_ALPHA_TEST);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
	}

}