package pw.brudin.huzuni.util;

import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.event.ClickEvent;

/**
 * @since 10:58 PM on 7/26/2015
 */
public class ChatComponentHelper {

	public static TextComponentString multiline(String main, String... lines) {
		TextComponentString out = new TextComponentString(main);
		for (String string : lines) {
			out.appendText("\n");
			out.appendSibling(new TextComponentString(string));
		}
		return out;
	}

	public static TextComponentString link(String text, String url) {
		TextComponentString out = new TextComponentString(text);
		out.getChatStyle().setChatClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, url));
		return out;
	}
}
