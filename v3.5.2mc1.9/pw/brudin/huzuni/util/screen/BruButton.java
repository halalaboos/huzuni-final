package pw.brudin.huzuni.util.screen;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.util.GLUtils;
import net.halalaboos.huzuni.util.Timer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import org.lwjgl.opengl.GL11;

import java.awt.*;

/**
 * DESCRIPTION
 *
 * @author brudin
 * @version 1.0
 * @since 3/26/14
 */
public class BruButton extends GuiButton {

	private long lastScale;
	private int scaledWidth = 0;
	private int scaledHeight = 0;
	private int alpha = 10;

	public BruButton(int par1, int par2, int par3, int par4, int par5, String par6Str) {
		super(par1, par2, par3, par4, par5, par6Str);
	}

	public BruButton(int par1, int par2, int par3, String par6Str) {
		super(par1, par2, par3, 200, 20, par6Str);
	}

	@Override
	public void drawButton(Minecraft mc, int mouseX, int mouseY) {
		if (this.visible) {
			Color black = (new Color(0, 0, 0, this.enabled ? alpha : alpha/2));
			this.hovered = mouseX >= xPosition && mouseY >= yPosition && mouseX < xPosition + width && mouseY < yPosition + height;
			updateButton();
			GLUtils.drawRect(xPosition - scaledWidth, yPosition - scaledHeight, xPosition + width + scaledWidth, yPosition + height + scaledHeight, black.getRGB());
			this.mouseDragged(mc, mouseX, mouseY);
			int textColor = 14737632;
			if (!this.enabled) {
				textColor = 0xD6D6D6;
			}
			else if (this.hovered) {
				textColor = 0xffffff;
			}
			Huzuni.getFontRenderer().drawCenteredStringNoShadow(displayString, xPosition + width / 2 - 1, yPosition + (height - 7) / 2, textColor);
		}
	}

	private void updateButton() {
		if (Timer.getSystemTime() - lastScale >= 15) {
			if (hovered && enabled) {
				alpha += 15;
			} else {
				alpha -= 5;
			}
			lastScale = Timer.getSystemTime();
		}

		if (alpha < 75) {
			alpha = 75;
		}

		if (alpha > 175) {
			alpha = 175;
		}
	}

}
