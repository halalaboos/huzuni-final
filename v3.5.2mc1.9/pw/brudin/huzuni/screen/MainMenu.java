package pw.brudin.huzuni.screen;

import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glEnable;

import java.io.IOException;

import net.halalaboos.huzuni.gui.screen.ChangelogScreen;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.VertexBuffer;
import net.minecraft.util.math.MathHelper;
import pw.brudin.huzuni.util.screen.BruButton;
import pw.brudin.huzuni.util.screen.PanoramaRenderer;
import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.gui.clickable.ContainerManager;
import net.halalaboos.huzuni.gui.clickable.menu.Menu;
import net.halalaboos.huzuni.gui.clickable.slot.SlotText;
import net.halalaboos.huzuni.gui.clickable.theme.HuzuniTheme;
import net.halalaboos.huzuni.gui.particle.ParticleEngine;
import net.halalaboos.huzuni.gui.screen.HuzuniDonorFeatures;
import net.halalaboos.huzuni.gui.screen.HuzuniOptions;
import net.halalaboos.huzuni.gui.screen.alt.AltManager;
import net.halalaboos.huzuni.rendering.Texture;
import net.halalaboos.huzuni.util.GLUtils;
import net.halalaboos.huzuni.util.Timer;
import net.minecraft.client.gui.*;
import net.minecraft.client.resources.I18n;

import org.lwjgl.Sys;
import org.lwjgl.opengl.GL11;

/**
 * The new main menu.
 *
 * @author brudin
 * @version 1.0
 * @since 1/4/14
 */
public class MainMenu extends GuiScreen {

    private PanoramaRenderer panoramaRenderer;
    private static final Texture TITLE = new Texture("huzuni/title.png");
    private static final ParticleEngine PARTICLE_ENGINE = new ParticleEngine(false);
	private final ContainerManager<Menu> menuManager = new ContainerManager<Menu>(new HuzuniTheme());
    private final Menu menu;
    
    
	private String[] todo = {
//		"todo list:", "fix fly + freecam fly"
	};

    public MainMenu() {
    	this.menu = new Menu("");
		menuManager.add(menu);
    }

    @Override
    public boolean doesGuiPauseGame() {
        return false;
    }

    @Override
    public void initGui() {
        panoramaRenderer = new PanoramaRenderer(width, height);
        panoramaRenderer.init();
        buttonList.clear();
		int size = width / 6 + 1;
		int i = 0;
		buttonList.add(new BruButton(1, i, 0, size, 20, I18n.format("menu.singleplayer")));
		buttonList.add(new BruButton(2, i += size, 0, size, 20, I18n.format("menu.multiplayer")));
		buttonList.add(new BruButton(3, i += size, 0, size, 20, "Accounts"));
		buttonList.add(new BruButton(7, i += size, 0, size, 20, Huzuni.isDonator() ? "Donator Perks" : "VIP/Donate"));
		buttonList.add(new BruButton(5, i += size, 0, size, 20, "Huzuni"));
		buttonList.add(new BruButton(10, i += size, 0, size, 20, "Options"));
		buttonList.add(new BruButton(4, width - 80, height - 20, 80, 20, I18n.format("menu.quit")));
		if (Huzuni.getUpdateCode() == 1)
			buttonList.add(new BruButton(6, width / 2 + width / 4 - 45, this.height / 2 - 12, 45, 20, "Update"));
		
		this.updateChangelog();
    }

    @Override
    protected void actionPerformed(GuiButton button) {
        switch (button.id) {
            case 10:
                this.mc.displayGuiScreen(new GuiOptions(this, this.mc.gameSettings));
                break;

            case 1:
                this.mc.displayGuiScreen(new GuiWorldSelection(this));
                break;

            case 2:
                this.mc.displayGuiScreen(new GuiMultiplayer(this));
                break;

            case 3:
                this.mc.displayGuiScreen(new AltManager(this));
                break;

            case 4:
                this.mc.shutdown();
                break;

            case 5:
               this.mc.displayGuiScreen(new HuzuniOptions(this));
                break;

            case 6:
                Sys.openURL("http://halalaboos.net/huzuni.html");
                break;

            case 7:
                if (Huzuni.isDonator())
                    mc.displayGuiScreen(new HuzuniDonorFeatures(this));
                else
                    Sys.openURL("http://halalaboos.net/donate.html");
                break;
            case 8:
                Sys.openURL("http://halalaboos.net/huzuni.html");
            	break;
            case 0:
				mc.displayGuiScreen(new ChangelogScreen(this));
				break;
        }
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		float titleX = width / 2 - 150, titleY = height / 2 - 100;
		String versionStatus = getReturnCodeStatus(Huzuni.getUpdateCode());

		GlStateManager.disableAlpha();
		panoramaRenderer.renderSkybox(mouseX, mouseY, partialTicks);
		GlStateManager.enableAlpha();
		GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);

		GL11.glPushMatrix();
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(770, 771);
		TITLE.render(titleX, titleY + 10, 300, 100);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glPopMatrix();

		Tessellator var4 = Tessellator.getInstance();
		VertexBuffer var5 = var4.getBuffer();

		GlStateManager.pushMatrix();

		int y = 25;
		for(String s : todo) {
			mc.fontRenderer.drawStringWithShadow(s, 5, y, -1);
			y += 10;
		}
		Huzuni.drawString(mc.getSession().getUsername() + " - " + "v" + Huzuni.VERSION + (Huzuni.isVip() ? " (VIP)" : ""), 2, height - Huzuni.getStringHeight("|"), 0x4fcccccc);
		Huzuni.drawString(versionStatus, width / 2 - Huzuni.getStringWidth(versionStatus) / 2, titleY + 90, 0xCFCCCCCC);


		panoramaRenderer.renderFade();

		GlStateManager.popMatrix();
		super.drawScreen(mouseX, mouseY, partialTicks);
        GLUtils.setupOverlayDefaultScale();
	    glEnable(GL_BLEND);
	    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	    menuManager.render();
	    glDisable(GL_BLEND);
	    mc.entityRenderer.setupOverlayRendering();
		if (Huzuni.getSplash() != null) {
			float splashScale = 2F - MathHelper.abs(MathHelper.sin((float)(Minecraft.getSystemTime() % 1000L) / 1000.0F * (float)Math.PI * 2.0F) * 0.1F);
	        splashScale = splashScale * 100.0F / (float)(mc.fontRenderer.getStringWidth(Huzuni.getSplash()) + 32);
	        GlStateManager.pushMatrix();
	        GlStateManager.translate(titleX + 150, titleY + 20F, 0F);
	        // GlStateManager.rotate(5.0F, 0.0F, 0.0F, 1.0F);
	        GlStateManager.scale(splashScale, splashScale, splashScale);
	        drawCenteredString(mc.fontRenderer, Huzuni.getSplash(), 0, 0, 0xFFFFFF);
	        GlStateManager.popMatrix();
		}
    }
    
    /**
     * @return String representing what the version return code means.
     */
    private String getReturnCodeStatus(int versionReturnCode) {
        switch (versionReturnCode) {
            case -1:
                return "Unable to connect.";
            case 0:
                return Huzuni.isVip() ? "Thank you :)" : ":)";
            case 1:
                return "Out of date!";
            case -2:
                return "Retrieving...";
        }
        return "" + versionReturnCode;
    }

    @Override
    public void updateScreen() {
        super.updateScreen();
        panoramaRenderer.panoramaTick();
        PARTICLE_ENGINE.updateParticles();
    }
    
    @Override
    protected void keyTyped(char c, int keyCode) throws IOException {
        super.keyTyped(c, keyCode);
        menuManager.keyTyped(keyCode, c);
    }

    @Override
    protected void mouseClicked(int x, int y, int buttonId) throws IOException {
        super.mouseClicked(x, y, buttonId);
        menuManager.mouseClicked(GLUtils.getMouseX(), GLUtils.getMouseY(), buttonId);
    }
    
    public void updateChangelog() {
    	float slotWidth = GLUtils.getScreenWidth() / 2;
	    menu.setX(GLUtils.getScreenWidth() / 2 - slotWidth / 2);
	    menu.setY(GLUtils.getScreenHeight() / 2 + 10);
	    menu.setWidth(slotWidth);
	    menu.setHeight(GLUtils.getScreenHeight() / 2 - 20);
		menu.clear();
		SlotText textArea = new SlotText(width - 1, height - 50);
		if(Huzuni.getUpdateList() == null) {
			textArea.add("Unable to connect.");
		} else {
			for (String updateLine : Huzuni.getUpdateList()) {
				for (String formattedLine : Huzuni.fontRenderer.wrapWords(updateLine, width))
					textArea.add(formattedLine);
			}
		}
		menu.add(textArea);
	    menu.layout();
    }
}