package net.halalaboos.network.tcp.client;

import java.io.IOException;
import java.net.Socket;

public class ConnectionHandler extends Thread {

	private final Client client;
	
	private Socket socket;
	
	public ConnectionHandler(Client client, Socket socket) {
		this.client = client;
		this.socket = socket;
	}
	
	@Override
	public void run() {
		try {
			while (client.isConnected()) {
				byte id = client.getInputStream().readByte();
				client.getProtocolHandler().read(id, client);
			}
		} catch (Exception e) {
			System.err.println("READ ERR: " + e.getMessage());
		} finally {
			cleanup();
		}
	}
	
	private void cleanup() {
		try {
			close();
		} catch (IOException e) {
			System.err.println("CLEANUP ERR: " + e.getMessage());
		}
	}

	public void close() throws IOException {
		if (client.isConnected()) {
			socket.close();
		}
	}
	
}
