package net.halalaboos.huzuni.plugin.value;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.util.FileUtils;

public final class ValueManager {

	private static final List<Value> values = new ArrayList<Value>();

	public static final File VALUE_FILE = new File(Huzuni.SAVE_DIRECTORY, "Values.txt");

	private ValueManager() {
		
	}
	
	public static void load() {
		List<String> lines = FileUtils.readFile(VALUE_FILE);
		for (String line : lines) {
			for (Value value : values) {
				if (line.substring(0, line.lastIndexOf(":")).equalsIgnoreCase(value.getName()))
					value.setValue(Float.parseFloat(line.substring(line.lastIndexOf(":") + 1, line.length())));
			}
		}
	}
	
	public static void save() {
		List<String> lines = new ArrayList<String>();
		for (Value value : values)
			lines.add(value.getName() + ":" + value.getValue());
		FileUtils.writeFile(VALUE_FILE, lines);
	}
	
	public static void add(Value value) {
		values.add(value);
	}
	
	public static void remove(Value value) {
		values.remove(value);
	}

	public static List<Value> getValues() {
		return values;
	}

	public static Value getValue(String name) {
		for (Value value : values) {
			if (value.getName().equalsIgnoreCase(name)) {
				return value;
			}
		}
		return null;
	}
}
