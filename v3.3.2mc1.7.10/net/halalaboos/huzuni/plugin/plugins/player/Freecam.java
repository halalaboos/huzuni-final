package net.halalaboos.huzuni.plugin.plugins.player;

import org.lwjgl.input.Keyboard;

import com.mojang.authlib.GameProfile;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.plugin.Category;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent;
import net.halalaboos.huzuni.event.tick.UpdateListener;
import net.halalaboos.huzuni.plugin.DefaultPlugin;
import net.minecraft.client.entity.EntityOtherPlayerMP;

public class Freecam extends DefaultPlugin implements UpdateListener {

	public static final Freecam instance = new Freecam();
	
    private EntityOtherPlayerMP fakePlayer;

	private Freecam() {
		super("Freecam", Keyboard.KEY_U);
		setCategory(Category.PLAYER);
        setDescription("Allows you to temporarily fly out of your body.");

	}

	@Override
	protected void onEnable() {
		Huzuni.registerUpdateListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterUpdateListener(this);
	}
	
	public void toggle() {
		super.toggle();
        if (isEnabled()) {
            fakePlayer = new EntityOtherPlayerMP(mc.theWorld, new GameProfile(mc.thePlayer.getUniqueID(), mc.thePlayer.getCommandSenderName()));
            fakePlayer.copyDataFrom(mc.thePlayer, true);
            fakePlayer.setPositionAndRotation(mc.thePlayer.posX, mc.thePlayer.posY - 1.5, mc.thePlayer.posZ, mc.thePlayer.rotationYaw, mc.thePlayer.rotationPitch);
            fakePlayer.rotationYawHead = mc.thePlayer.rotationYawHead;
            mc.theWorld.addEntityToWorld(-69, fakePlayer);
            mc.thePlayer.capabilities.isFlying = true;
            mc.thePlayer.noClip = true;
        } else {
        	if (fakePlayer != null && mc.thePlayer != null) {
	            mc.thePlayer.setPositionAndRotation(fakePlayer.posX, fakePlayer.posY + 1.5, fakePlayer.posZ, fakePlayer.rotationYaw, fakePlayer.rotationPitch);
	            mc.thePlayer.noClip = false;
	            mc.theWorld.removeEntityFromWorld(-69);
	            mc.thePlayer.capabilities.isFlying = false;
        	}
        }
    }

	@Override
	public void onPreUpdate(MotionUpdateEvent event) {
		mc.thePlayer.capabilities.isFlying = true;
        mc.thePlayer.noClip = true;
        mc.thePlayer.renderArmPitch += 200;
        mc.thePlayer.renderArmYaw += 180;
        event.setCancelled(true);
	}

	@Override
	public void onPostUpdate(MotionUpdateEvent event) {
	}
	
}
