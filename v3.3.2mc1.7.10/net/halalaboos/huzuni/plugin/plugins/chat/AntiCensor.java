package net.halalaboos.huzuni.plugin.plugins.chat;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.plugin.Category;
import net.halalaboos.huzuni.event.packet.PacketEvent;
import net.halalaboos.huzuni.event.packet.PacketListener;
import net.halalaboos.huzuni.plugin.VipPlugin;
import net.minecraft.network.play.client.C01PacketChatMessage;

import java.util.Random;

/**
 * DESCRIPTION
 *
 * @author brudin
 * @version 1.0
 * @since 3/25/14
 */
public class AntiCensor extends VipPlugin implements PacketListener {

	private final Random random = new Random();

	private final String[] badWords = {
			"fuck", "shit", "ass", "bastard", "bitch", "nigger", "cock", "cunt", "dick", "damn", "douche", "penis", "vagina", "gay", "pussy" };


	private final String[] replacementWords = {
			"darn", "poppy", "fudge", "frick", "lint", "fiddle", "feces", "kitty", "dong", "doorknob",
			"rebecca", "lasagna", "loo", "cat", "violin", "bacon", "powder", "refrigerator", "ribbit",
			"fetus", "shrek", "daughter", "waffle", "rosie o donald", "obama", "jonah"
	};

	public AntiCensor() {
		super("AntiCensor");
		setAuthor("Halalaboos");
		setCategory(Category.CHAT);
		setDescription("Stops you from being muted on hackercraft.");
	}

	@Override
	public void onPacket(PacketEvent event, PacketEvent.PacketType type) {
		if(type == PacketEvent.PacketType.SENT) {
			if(event.getPacket() instanceof C01PacketChatMessage) {
				C01PacketChatMessage packetChatMessage = (C01PacketChatMessage)event.getPacket();
				String message = packetChatMessage.func_149439_c();
				event.setPacket(new C01PacketChatMessage(replaceBadWords(message)));
			}
		}
	}

	@Override
	protected void onEnable() {
		Huzuni.registerPacketListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterPacketListener(this);
	}

	private String replaceBadWords(String message) {
		message = message.replaceAll("(?i)douche?|fag(\b|[^e])|[s$]lut|whore|((\b)cum*(\b|(ing)))|hand[^a-z]*j[o0]b|bl[o0]w[^a-z]*j[o0]b|dildo|jack[^a-z ]*[o0]ff|pecker|pen[i!][s$]|b[o0]ner", getRandomReplacement());
		for (String badWord : this.badWords) {
			message = message.replaceAll("(?i)" + badWord, getRandomReplacement());
		}
		return message;
	}

	private String getRandomReplacement() {
		return replacementWords[random.nextInt(replacementWords.length)];
	}
}
