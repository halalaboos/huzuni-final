/**
 * 
 */
package net.halalaboos.huzuni.plugin.plugins.world;

import static org.lwjgl.opengl.GL11.*;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.util.vector.Vector3f;
import org.w3c.dom.Element;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.gui.render.Theme;
import net.halalaboos.huzuni.api.plugin.Category;
import net.halalaboos.huzuni.api.plugin.Option;
import net.halalaboos.huzuni.api.plugin.ValueOption;
import net.halalaboos.huzuni.event.render.RenderEvent;
import net.halalaboos.huzuni.event.render.Renderable;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent;
import net.halalaboos.huzuni.event.tick.UpdateListener;
import net.halalaboos.huzuni.plugin.VipPlugin;
import net.halalaboos.huzuni.plugin.value.Value;
import net.halalaboos.huzuni.plugin.value.ValueManager;
import net.halalaboos.huzuni.rendering.Box;
import net.halalaboos.huzuni.util.GLUtils;
import net.halalaboos.huzuni.util.GameUtils;
import net.halalaboos.huzuni.util.MathUtils;
import net.halalaboos.huzuni.util.Timer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;

/**
 * @author Halalaboos
 *
 */
public class AutoBuild extends VipPlugin implements UpdateListener, Renderable {

	private boolean[] pattern = new boolean[25];
	
	private float lastYaw, lastPitch;
	
	private List<Vector3f> layout = new ArrayList<Vector3f>();
	
	private Vector3f position = null;
	
	private boolean running = false;
	
	private final Timer timer = new Timer(), placeTimer = new Timer();
	
	private int direction = 0;
	
	private Value delay = new Value("Build Delay", 10F, 500F, 1000F, 10F);
	
	private final Box box;
		
	public AutoBuild() {
		super("Auto Build");
        setCategory(Category.WORLD);
        ValueManager.add(delay);
        setDescription("Places blocks in the pattern made.");
		box = new Box(AxisAlignedBB.getBoundingBox(0, 0, 0, 1, 1, 1));
		this.setOptions(new BuildOption(), new ValueOption(delay));
	}
	
	@Override
	public void toggle() {
		super.toggle();
		//if (isEnabled())
		//	mc.displayGuiScreen(new AutoBuildMenu(this));
	}
	
	@Override
	protected void onEnable() {
		Huzuni.registerUpdateListener(this);
		Huzuni.registerRenderable(this);
		Huzuni.addChatMessage("Press sneak and place a block to start building!");
	}
	
	@Override
	protected void onDisable() {
		Huzuni.unregisterUpdateListener(this);
		Huzuni.unregisterRenderable(this);
		if (running) {
			position = null;
			running = false;
			Huzuni.addChatMessage("Cancelling..");
		}
	}
	
	private void getSelectedBlock() {
		if (mc.objectMouseOver != null && mc.objectMouseOver.typeOfHit == MovingObjectPosition.MovingObjectType.BLOCK && placeTimer.hasReach(500)) {
			if (mc.gameSettings.keyBindUseItem.getIsKeyPressed() && mc.gameSettings.keyBindSneak.getIsKeyPressed()) {
				setupLayout();
				position = new Vector3f(mc.objectMouseOver.blockX, mc.objectMouseOver.blockY + 3, mc.objectMouseOver.blockZ);
				genDirection();
				running = true;
				timer.reset();
				Huzuni.addChatMessage("Placing..");
				placeTimer.reset();
			}
		}
	}
	
	@Override
	public void onPreUpdate(MotionUpdateEvent event) {
		lastYaw = mc.thePlayer.rotationYaw;
		lastPitch = mc.thePlayer.rotationPitch;
		if (running) {
			int index = getNextPiece();
			if (index == -1) {
				running = false;
				position = null;
				Huzuni.addChatMessage("Done!");
				return;
			}
			
			Vector3f nextPiece = layout.get(index);
			
			int[] positions = getAdjustedVector(nextPiece);
            int x = positions[0], y = positions[1], z = positions[2];
			float offset = 0.5F;
			int side = getSide((int) position.x + x, (int) position.y + y, (int) position.z + z);
			GameUtils.face(position.x + x + offset, position.y + y + offset, position.z + z + offset);
			
			positions = getPositionsFromSide(positions, side);
			x = positions[0]; y = positions[1]; z = positions[2];
			
			if (GameUtils.getDistance(position.x + x, position.y + y, position.z + z) < 4.5F && mc.thePlayer.getCurrentEquippedItem() != null && mc.thePlayer.getCurrentEquippedItem().canEditBlocks()) {
				if (timer.hasReach((int) delay.getValue())) {
					placeBlock(position.x + x, position.y + y, position.z + z, side);
					mc.thePlayer.swingItem();
					removeAndContinue(index);
					timer.reset();
				}
			}
		} else
			getSelectedBlock();
	}

	@Override
	public void onPostUpdate(MotionUpdateEvent event) {
		mc.thePlayer.rotationYaw = lastYaw;
		mc.thePlayer.rotationPitch = lastPitch;
	}
	
	@Override
	public void render(RenderEvent event) {
		if (running) {
			for (int i = 0; i < layout.size(); i++) {
				Vector3f position = layout.get(i);
				int[] positions = getAdjustedVector(position);
	            int x = positions[0], y = positions[1], z = positions[2];
				double renderX = (this.position.x + x) - RenderManager.renderPosX;
				double renderY = (this.position.y + y) - RenderManager.renderPosY;
				double renderZ = (this.position.z + z) - RenderManager.renderPosZ;
				glPushMatrix();
				glTranslated(renderX, renderY, renderZ);
				glColor4f(0, 1, 0, 0.2F);
				box.render();
				glPopMatrix();
			}
		}
    }
	
	protected void placeBlock(double x, double y, double z, int side) {
		float offset = 0F;
        mc.getNetHandler().addToSendQueue(new C08PacketPlayerBlockPlacement((int) x, (int) y, (int) z, side, mc.thePlayer.inventory.getCurrentItem(), offset, offset, offset));
        mc.thePlayer.getCurrentEquippedItem().tryPlaceItemIntoWorld(mc.thePlayer, mc.theWorld, (int) x, (int) y, (int) z, side, offset, offset, offset);
	}
	
	protected boolean removeAndContinue(int index) {
		if (layout.size() > index) {
			layout.remove(index);
			return false;
		} else
			return true;
	}
	
	protected int getNextPiece() {
		if (layout.size() < 1)
			return -1;
		else {
			for (int i = 0; i < layout.size(); i++) {
				Vector3f piece = layout.get(i);
				int[] positions = getAdjustedVector(piece);
				
	            int x = positions[0], y = positions[1], z = positions[2];
				if (getSide((int) (position.x + x), (int) (position.y + y), (int) (position.z + z)) != -1) {
					return i;
				}
			}
			return -1;
		}
	}
	
	/**
	 * Adjusts the given vector according to our direction.
	 * */
	protected int[] getAdjustedVector(Vector3f position) {
		int x = 0, y = (int) (position.y), z = 0;
        switch (direction) {
        case 0:
        	x = (int) -position.x;
        	z = (int) position.z;
        	break;
        case 1:
        	x = (int) -position.z;
        	z = (int) -position.x;
        	break;
        case 2:
        	x = (int) position.x;
        	z = (int) -position.z;
        	break;
        case 3:
        	x = (int) position.z;
        	z = (int) position.x;
        	break;
        default:
        	x = (int) position.x;
        	z = (int) position.z;
        }
        return new int[] { x, y, z };
	}
	
	/**
	 * Finds a block that is adjacent to the coordinates, if there is a block it will give the side corresponding.
	 * */
	protected int getSide(int x, int y, int z) {
		if (!isAir(x, y, z))
			return -1;
		if (!isAir(x, y - 1, z))
			return 1;
		else if (!isAir(x, y + 1, z))
			return 0;
		else if (!isAir(x - 1, y, z))
			return 5;
		else if (!isAir(x + 1, y, z))
			return 4;
		else if (!isAir(x, y, z - 1))
			return 3;
		else if (!isAir(x, y, z + 1))
			return 2;
		else
			return -1;
	}
	
	/**
	 * From constant testing, I figured out that when you place a block,
	 * the coordinates sent is the coordinates to the block corresponding to the side. 
	 * They go as follow.
	 * */
	protected int[] getPositionsFromSide(int[] positions, int side) {
		switch (side) {
		case 0:
			positions[1] += 1;
			return positions;
		case 1:
			positions[1] -= 1;
			return positions;
		case 2:
			positions[2] += 1;
			return positions;
		case 3:
			positions[2] -= 1;
			return positions;
		case 4:
			positions[0] += 1;
			return positions;
		case 5:
			positions[0] -= 1;
			return positions;
			default:
				return positions;
		}
	}

	/**
	 * @return If the coordinates are air.
	 * */
	protected boolean isAir(double x, double y, double z) {
		return mc.theWorld.getBlock((int) x, (int) y, (int) z).getMaterial() == Material.air;
	}
	
	protected void genDirection() {
        direction = MathHelper.floor_double((double) (mc.thePlayer.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3;
	}
	
	/**
	 * Sets up the layout from the pattern.
	 * */
	protected void setupLayout() {
		
		this.layout = genLayout();
		/*
		 * SWASTIKA
		layout.clear();

		// TOP ARM
		layout.add(new Vector3f(2, 2, 0));
		layout.add(new Vector3f(1, 2, 0));
		layout.add(new Vector3f(0, 2, 0));
		layout.add(new Vector3f(0, 1, 0));
		
		// CENTER
		layout.add(new Vector3f(0, 0, 0));
		
		// BOTTOM ARM
		layout.add(new Vector3f(0, -1, 0));
		layout.add(new Vector3f(0, -2, 0));
		layout.add(new Vector3f(-1, -2, 0));
		layout.add(new Vector3f(-2, -2, 0));
		
		// RIGHT ARM
		layout.add(new Vector3f(1, 0, 0));
		layout.add(new Vector3f(2, 0, 0));
		layout.add(new Vector3f(2, -1, 0));
		layout.add(new Vector3f(2, -2, 0));
		
		// LEFT ARM
		layout.add(new Vector3f(-1, 0, 0));
		layout.add(new Vector3f(-2, 0, 0));
		layout.add(new Vector3f(-2, 1, 0));
		layout.add(new Vector3f(-2, 2, 0));
		*/
	}
	/**
	 * LAYOUT {
	 *	1  2  3  4 	5
	 *	6  7  8  9 	10
	 *	11 12 13 14 15
	 *	16 17 18 19 20
	 *	21 22 23 24 25
	 * }
	 * Generates a list from the pattern set.
	 * @return
	 */
	private List<Vector3f> genLayout() {
		List<Vector3f> list = new ArrayList<Vector3f>();
		for (int i = 0; i < pattern.length; i++) {
			boolean enabled = pattern[i];
			if (enabled) {
				list.add(new Vector3f(getX(i), getY(i), 0));
			}
		}
		return list;
	}

	public boolean[] getPattern() {
		return pattern;
	}
	
	/**
	 * Awful method of getting x offset from the id.
	 * */
	private int getX(int id) {
		id += 1;
		if (id == 1 || id == 6 || id == 11 || id == 16 || id == 21) {
			return -2;
		} else if (id == 2 || id == 7 || id == 12 || id == 17 || id == 22) {
			return -1;
		} else if (id == 3 || id == 8 || id == 13 || id == 18 || id == 23) {
			return 0;
		} else if (id == 4 || id == 9 || id == 14 || id == 19 || id == 24) {
			return 1;
		} else if (id == 5 || id == 10 || id == 15 || id == 20 || id == 25) {
			return 2;
		} else 
			return 6;
	}
	
	/**
	 * Really bad method of getting the y offset from the id.
	 * */
	private int getY(int id) {
		id += 1;
		if (id <= 5) {
			return 2;
		} else if (id > 5 && id <= 10) {
			return 1;
		} else if (id > 10 && id <= 15) {
			return 0;
		} else if (id > 15 && id <= 20) {
			return -1;
		} else if (id > 20 && id <= 25) {
			return -2;
		} else
			return 6;
	}
	
	public class BuildOption extends Option {

		private final RenderItem itemRenderer = new RenderItem();
		
	    private final ItemStack dirt = new ItemStack(Blocks.dirt),
	    grass = new ItemStack(Blocks.grass);
	    
		public BuildOption() {
			super("Build Option");	
		}

		@Override
		public boolean mouseClicked(int x, int y, int buttonId) {
			return this.isPointInside(x, y);
		}

		@Override
		public void mouseReleased(int x, int y, int buttonId) {
			int size = (int) ((getWidth() - 2) / 5F);
			int count = 0, yPos = 0, xPos = 0;
			for (int i = 0; i < 25; i++) {
				if (count >= 5) {
					yPos += size + 1;
					xPos = 0;
					count = 0;
				}
				float[] boxArea = new float[] { area[0] + xPos, area[1] + yPos, size, size };
				if (MathUtils.inside(x, y, boxArea)) {
					getPattern()[i] = !getPattern()[i];
				}
				xPos += size + 1;
				count++;
			}
		}

		@Override
		public void render(Theme theme, int index, float[] area,
				boolean mouseOver, boolean mouseDown) {
			int size = (int) ((getWidth() - 2) / 5F);
			int count = 0, yPos = 0, xPos = 0;
			for (int i = 0; i < 25; i++) {
				if (count >= 5) {
					yPos += size + 1;
					xPos = 0;
					count = 0;
				}
				float[] boxArea = new float[] { area[0] + xPos, area[1] + yPos, size, size };
				theme.renderSlot(0, 0, index, boxArea, getPattern()[i], mouseOver && MathUtils.inside(GLUtils.getMouseX(), GLUtils.getMouseY(), boxArea), mouseDown);
				renderItem(getPattern()[i] ? grass : dirt, (int) boxArea[0] + 1, (int) boxArea[1] + 1, 0F);
				xPos += size + 1;
				count++;
			}
		}
	    
		private void renderItem(ItemStack itemStack, int x, int y,
				float delta) {
			GL11.glPushMatrix();
            GL11.glDisable(GL11.GL_BLEND);
            GL11.glEnable(GL12.GL_RESCALE_NORMAL);
            RenderHelper.enableGUIStandardItemLighting();
            GL11.glDepthMask(true);
            GL11.glDisable(GL11.GL_DEPTH_TEST);
            try {
            	itemRenderer.renderItemIntoGUI(mc.fontRenderer, mc.getTextureManager(), itemStack, x, y);
            	itemRenderer.renderItemOverlayIntoGUI(mc.fontRenderer, mc.getTextureManager(), itemStack, x, y);
            } catch (Exception e) {
            	e.printStackTrace();
            }
            RenderHelper.disableStandardItemLighting();
            GL11.glDisable(GL12.GL_RESCALE_NORMAL);
            GL11.glEnable(GL11.GL_DEPTH_TEST);
            GL11.glDepthMask(true);
            GL11.glPopMatrix();
		}
		
		@Override
		public void setArea(float[] area) {
			super.setArea(area);
			area[3] = ((area[2] - 2) / 5F) * 5 + 2;
		}

		@Override
		public void load(Element element) {
		}

		@Override
		public void save(Element element) {
		}
	}
	
}
