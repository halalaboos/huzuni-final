package net.halalaboos.huzuni.plugin.plugins.world;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.plugin.Category;
import net.halalaboos.huzuni.event.packet.PacketEvent;
import net.halalaboos.huzuni.event.packet.PacketEvent.PacketType;
import net.halalaboos.huzuni.event.packet.PacketListener;
import net.halalaboos.huzuni.plugin.DefaultPlugin;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityBoat;
import net.minecraft.entity.item.EntityMinecart;
import net.minecraft.network.play.client.C02PacketUseEntity;

public class VehicleOneHit extends DefaultPlugin implements PacketListener {

    public VehicleOneHit() {
        super("Vehicle 1-Hit", -1);
        setCategory(Category.WORLD);
        setDescription("Breaks vehicles in one hit.");
		setVisible(false);
    }

    @Override
    protected void onEnable() {
    	Huzuni.registerPacketListener(this);
    }

    @Override
    protected void onDisable() {
    	Huzuni.unregisterPacketListener(this);
    }
    
    private boolean isVehicle(Entity entity) {
        return entity != null && (entity instanceof EntityBoat || entity instanceof EntityMinecart);
    }
    
    int count = 0;

	@Override
	public void onPacket(PacketEvent event, PacketType type) {
		if (type == PacketType.SENT) {
			if (event.getPacket() instanceof C02PacketUseEntity) {
				if (count > 0)
					count--;
				else {
					Entity entity = ((C02PacketUseEntity) event.getPacket()).func_149564_a(mc.theWorld);
			    	if (isVehicle(entity)) {
			    		count = 6;
			    		for (byte b = 0; b < 6; b++)
			    			mc.getNetHandler().addToSendQueue(new C02PacketUseEntity(entity, C02PacketUseEntity.Action.ATTACK));
			    	}
				}
			}
		}
	}

}
