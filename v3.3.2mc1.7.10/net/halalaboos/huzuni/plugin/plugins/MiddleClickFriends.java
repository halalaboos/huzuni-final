package net.halalaboos.huzuni.plugin.plugins;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.plugin.Category;
import net.halalaboos.huzuni.plugin.DefaultPlugin;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.MovingObjectPosition;

/**
 * @author brudin
 * @version 1.0
 * @since 12:48 PM on 8/20/2014
 */
public class MiddleClickFriends extends DefaultPlugin {

	public static MiddleClickFriends instance = new MiddleClickFriends();

	public MiddleClickFriends() {
		super("Middle Click Friends");
		setEnabled(true);
		setVisible(false);
		setCategory(Category.WORLD);
		setDescription("Adds players as friends when you middle click them.");
	}

	@Override
	protected void onEnable() {

	}

	@Override
	protected void onDisable() {

	}

	public void onClick(int buttonID) {
		if (buttonID == 2) {
			if (mc.objectMouseOver != null) {
				if (mc.objectMouseOver.typeOfHit == MovingObjectPosition.MovingObjectType.ENTITY && mc.objectMouseOver.entityHit instanceof EntityPlayer) {
					if (Huzuni.isFriend(mc.objectMouseOver.entityHit.getCommandSenderName())) {
						Huzuni.addChatMessage(String.format("Removed %s as a friend.", mc.objectMouseOver.entityHit.getCommandSenderName()));
						Huzuni.removeFriend(mc.objectMouseOver.entityHit.getCommandSenderName());
					} else {
						Huzuni.addFriend(mc.objectMouseOver.entityHit.getCommandSenderName());
						Huzuni.addChatMessage(String.format("Added %s as a friend.", mc.objectMouseOver.entityHit.getCommandSenderName()));
					}
				}
			}
		}
	}
}
