package net.halalaboos.huzuni.plugin.plugins.render;

import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.NameProtect;
import net.halalaboos.huzuni.api.plugin.Category;
import net.halalaboos.huzuni.api.plugin.ToggleOption;
import net.halalaboos.huzuni.api.plugin.ValueOption;
import net.halalaboos.huzuni.plugin.DefaultPlugin;
import net.halalaboos.huzuni.plugin.value.Value;
import net.halalaboos.huzuni.plugin.value.ValueManager;
import static org.lwjgl.opengl.GL11.*;

public class Nametags extends DefaultPlugin {

	public static final Nametags instance = new Nametags();

	private final Value scale = new Value("Nametag Scale", 0F, 1F, 5F);
	
	private final RenderItem itemRenderer = new RenderItem();
	
	private final ToggleOption armor = new ToggleOption("Show Armor"),
			doScale = new ToggleOption("Scale");
	
	/***
	 * RendererLivingEntity - Removed rendering tags if this is enabled.  Above calls
	 * aren't necessary anymore.
	 * */
	public Nametags() {
		super("Nametags", Keyboard.KEY_P);
		setCategory(Category.RENDER);
		setDescription("Scales nametags when they're far.");
        setVisible(false);
        ValueManager.add(scale);
        this.setOptions(new ValueOption(scale), armor, doScale);
        doScale.setEnabled(true);
	}

	@Override
	protected void onEnable() {
	}

	@Override
	protected void onDisable() {
	}

	public boolean renderNameplate(Entity entity, String name, double x, double y, double z) {
		if (entity instanceof EntityPlayer && name.toLowerCase().contains(entity.getCommandSenderName().toLowerCase())) {
			name = NameProtect.replace(name);
            float scale = 0.016666668F * 1.6F;
            glPushMatrix();
            glTranslatef((float) x, (float) y + entity.height + 0.5F, (float) z);
            glNormal3f(0.0F, 1.0F, 0.0F);
            glRotatef(-RenderManager.instance.playerViewY, 0.0F, 1.0F, 0.0F);
            glRotatef(RenderManager.instance.playerViewX, 1.0F, 0.0F, 0.0F);
            glScalef(-scale, -scale, scale);
            glDisable(GL_FOG);
            glEnable(GL_POLYGON_OFFSET_FILL);
            glPolygonOffset(1, -10000000);
            mc.entityRenderer.disableLightmap(0);
            glDisable(GL_LIGHTING);
            glEnable(GL_BLEND);
            OpenGlHelper.glBlendFunc(770, 771, 1, 0);
            byte offset = 2;
			float distance = mc.thePlayer.getDistanceToEntity(entity);
            float nametagScale = distance / (2F + (5F - (this.scale.getValue())));
            if (nametagScale > 1 && doScale.isEnabled())
            	glTranslatef(0F, (float) -(((8 + offset * 2) / 2) * nametagScale), 0F);
            boolean friend = Huzuni.isFriend(entity.getCommandSenderName()), sneaking = entity.isSneaking();
			int color = friend ? 0xFF5ABDFC : sneaking ? 0xFF0080 : -1;
			
			if (nametagScale > 1 && doScale.isEnabled())
				glScalef(nametagScale, nametagScale, nametagScale);

            glDisable(GL_TEXTURE_2D);
            Tessellator tesselator = Tessellator.instance;
            int width = mc.fontRenderer.getStringWidth(name) / 2;
            tesselator.setColorRGBA_F(0.0F, 0.0F, 0.0F, 0.25F);
            /* POINT ORDER (x, y), (x, y1), (x1, y1), (x1, y) */
            double rX = -width - offset, rY = -offset, rX1 = width + offset, rY1 = 8 + offset, border = 1F;
            
            /* border */
            if (color != -1) {
	            tesselator.startDrawingQuads();
	            tesselator.setColorRGBA_F((float) (color >> 16 & 255) / 255.0F, (float) (color >> 8 & 255) / 255.0F, (float) (color & 255) / 255.0F, 0.25F);
	            tesselator.addVertex(rX - border, rY - border, 0.0D);
	            tesselator.addVertex(rX - border, rY1 + border, 0.0D);
	            tesselator.addVertex(rX1 + border, rY1 + border, 0.0D);
	            tesselator.addVertex(rX1 + border, rY - border, 0.0D);
	            tesselator.draw();
            }
            
            /* inside */
            tesselator.startDrawingQuads();
            tesselator.setColorRGBA_F(0.0F, 0.0F, 0.0F, 0.25F);
            tesselator.addVertex(rX, rY, 0.0D);
            tesselator.addVertex(rX, rY1, 0.0D);
            tesselator.addVertex(rX1, rY1, 0.0D);
            tesselator.addVertex(rX1, rY, 0.0D);
            tesselator.draw();
            glEnable(GL_TEXTURE_2D);
            mc.fontRenderer.drawStringWithShadow(name, -mc.fontRenderer.getStringWidth(name) / 2, 0, 0xFFFFFF);
            if (armor.isEnabled()) {
            	EntityPlayer player = (EntityPlayer) entity;
            	int totalItems = 0;
            	for (int i = 0; i < 4; i++)
            		if (player.getCurrentArmor(i) != null)
            			totalItems++;
            	if (player.getCurrentEquippedItem() != null)
            		totalItems++;
            	
            	int itemSize = 18, center = (-itemSize / 2), halfTotalSize = ((totalItems * itemSize) / 2 - itemSize) + (itemSize / 2), count = 0;
            	
            	if (player.getCurrentEquippedItem() != null) {
        			renderItem(player.getCurrentEquippedItem(), (int) (center - halfTotalSize) + itemSize * count, (int) rY - 16, 0F);
        			count++;
            	}
            	
            	for (int i = 0; i < 4; i++) {
            		if (player.getCurrentArmor(i) != null) {
            					renderItem(player.getCurrentArmor(i), (int) (center - halfTotalSize) + itemSize * count, (int) rY - 16, 0F);
            			count ++;
            		}
            	}
            }
            glEnable(GL_LIGHTING);
            glDisable(GL_BLEND);
            glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            mc.entityRenderer.enableLightmap(0);
            glDisable(GL_POLYGON_OFFSET_FILL);
            glPolygonOffset(1, 10000000);
        	glEnable(GL_FOG);
            glPopMatrix();
            return true;
		}
		return false;
	}
	
	private void renderItem(ItemStack itemStack, int x, int y,
			float delta) {
		GL11.glPushMatrix();
		try {
			itemRenderer.renderItemIntoGUI(mc.fontRenderer, mc.getTextureManager(), itemStack, x, y);
			itemRenderer.renderItemOverlayIntoGUI(mc.fontRenderer, mc.getTextureManager(), itemStack, x, y);
		} catch (Exception e) {
			e.printStackTrace();
		}
		GL11.glPopMatrix();
	}
	
}
