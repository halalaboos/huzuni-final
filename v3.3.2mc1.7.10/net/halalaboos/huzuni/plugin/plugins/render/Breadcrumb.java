package net.halalaboos.huzuni.plugin.plugins.render;

import static org.lwjgl.opengl.GL11.GL_LINE_STRIP;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glColor4f;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glVertex3d;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import net.halalaboos.huzuni.plugin.PluginManager;
import org.lwjgl.util.vector.Vector3f;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.plugin.Category;
import net.halalaboos.huzuni.api.console.Command;
import net.halalaboos.huzuni.command.CommandManager;
import net.halalaboos.huzuni.event.render.RenderEvent;
import net.halalaboos.huzuni.event.render.Renderable;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent;
import net.halalaboos.huzuni.event.tick.UpdateListener;
import net.halalaboos.huzuni.plugin.DefaultPlugin;
import net.minecraft.client.renderer.entity.RenderManager;

public class Breadcrumb extends DefaultPlugin implements Renderable, UpdateListener {

	private final List<Vector3f> vertices = new CopyOnWriteArrayList<Vector3f>();

    private int movementCount = 0;
    
	public Breadcrumb() {
		super("Breadcrumb");
		setCategory(Category.RENDER);
		setDescription("Draws a line behind your ass.");
		Command clear = new Command() {

            @Override
            public String[] getAliases() {
                return new String[] {"breadclear", "breadcrumbclear", "bc"};
            }

            @Override
            public String[] getHelp() {
                return new String[] {"You don't need help with this command."};
            }

            @Override
            public String getDescription() {
                return "Clears the breadcrumb lines.";
            }

            @Override
            public void run(String input, String[] args) {
                vertices.clear();
                Huzuni.addChatMessage("Cleared!");
            }

        };
        CommandManager.registerCommand(clear);
	}

	@Override
	public void onPreUpdate(MotionUpdateEvent event) {
        Vector3f position = new Vector3f((float) mc.thePlayer.posX, (float) mc.thePlayer.posY, (float) mc.thePlayer.posZ);
		if(PluginManager.getPlugin("Freecam").isEnabled()) {
			return;
		}
        if (vertices.isEmpty())
            vertices.add(position);
        else {
            double diffX = mc.thePlayer.prevPosX - mc.thePlayer.posX;
            double diffY = mc.thePlayer.prevPosY - mc.thePlayer.posY;
            double diffZ = mc.thePlayer.prevPosZ - mc.thePlayer.posZ;
            if (diffX != 0 || diffY != 0 || diffZ != 0) {
            	movementCount++;
            	if (movementCount >= 5) {
            		vertices.add(position);
            		movementCount = 0;
            	}
            }
        }
    }

	@Override
	public void onPostUpdate(MotionUpdateEvent event) {
	}

	@Override
	public void render(RenderEvent event) {
        glBegin(GL_LINE_STRIP);
        glColor4f(1F, 0F, 0F, 1F);
        glVertex3d(0, -mc.thePlayer.height, 0);
        for (int i = vertices.size() - 1; i >= 0; i--) {
            Vector3f vertex = vertices.get(i);
            glVertex3d(vertex.x - RenderManager.renderPosX, vertex.y - RenderManager.renderPosY - mc.thePlayer.height, vertex.z - RenderManager.renderPosZ);
        }
        glEnd();
    }

	@Override
	protected void onEnable() {
		Huzuni.registerUpdateListener(this);
		Huzuni.registerRenderable(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterUpdateListener(this);
		Huzuni.unregisterRenderable(this);
	}

}
