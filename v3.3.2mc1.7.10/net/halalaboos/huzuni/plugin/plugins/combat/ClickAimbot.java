package net.halalaboos.huzuni.plugin.plugins.combat;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.plugin.Category;
import net.halalaboos.huzuni.api.plugin.ToggleOption;
import net.halalaboos.huzuni.api.plugin.ValueOption;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent;
import net.halalaboos.huzuni.event.tick.UpdateListener;
import net.halalaboos.huzuni.plugin.DefaultPlugin;
import net.halalaboos.huzuni.plugin.value.Value;
import net.halalaboos.huzuni.plugin.value.ValueManager;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.IMob;
import net.minecraft.entity.passive.IAnimals;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.MathHelper;

public class ClickAimbot extends DefaultPlugin implements UpdateListener {
	
	private EntityLivingBase entity;
	
	private final Value reach = new Value("Click Reach", 3F, 3.8F, 6F);
    private final Value yawRate = new Value("Click Yaw Rate", 10F, 25F, 180F, 1F);
    
	private final ToggleOption players = new ToggleOption("Players"), 
			mobs = new ToggleOption("Mobs"), 
			animals = new ToggleOption("Animals");
	
	public ClickAimbot() {
		super("Click Aimbot");
		setAuthor("Halalaboos");
		setDescription("Aims at entities you're trying to kill");
		ValueManager.add(reach);
		ValueManager.add(yawRate);
		setCategory(Category.COMBAT);
		setOptions(new ValueOption(reach), new ValueOption(yawRate), players, mobs, animals);
		players.setEnabled(true);
	}

	@Override
	public void onPreUpdate(MotionUpdateEvent event) {
		entity = getClosestEntity(reach.getValue(), 2.5F);
		if (entity != null && isWithinDistance(entity, reach.getValue()) && mc.gameSettings.keyBindAttack.getIsKeyPressed()) {
			float[] rotations = getRotationsNeeded(entity);
	        if (rotations != null) {
	        	if (Math.abs(mc.thePlayer.rotationYaw - rotations[0]) <= yawRate.getValue()) {
	        		mc.thePlayer.rotationYaw = rotations[0];
	            	mc.thePlayer.rotationPitch = rotations[1];
	        	}
	        }
		}
	}

	@Override
	public void onPostUpdate(MotionUpdateEvent event) {
	}

	@Override
	protected void onEnable() {
		Huzuni.registerUpdateListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterUpdateListener(this);
	}
	
	private EntityLivingBase getClosestEntity(float distance, float extender) {
		EntityLivingBase currentEntity = null;
		for (int i = 0; i < mc.theWorld.loadedEntityList.size(); i++) {
    		if (mc.theWorld.loadedEntityList.get(i) instanceof EntityLivingBase) {
    			EntityLivingBase entity = (EntityLivingBase) mc.theWorld.loadedEntityList.get(i);
    			if (isAliveNotUs(entity) && checkType(entity, mobs.isEnabled(), animals.isEnabled(), players.isEnabled())) {
    				if (entity instanceof EntityPlayer && Huzuni.isFriend(entity.getCommandSenderName()))
    					continue;
    				if (currentEntity != null) {
    	                if (isClosestToMouse(currentEntity, entity, distance, extender) && isWithinDistance(entity, distance + extender))
        					currentEntity = entity;

    				} else {
    					if (isWithinDistance(entity, distance + extender))
    						currentEntity = entity;
    				}
    			}
    		}
    	}
		return currentEntity;
	}
	
	 /**
     * @return the closest entity to your mouse.
     */
    private boolean isClosestToMouse(EntityLivingBase currentEntity,
                                   EntityLivingBase otherEntity, float distance, float extender) {
        // If we can't reach the other entity, even with our extender, don't return true.
        if (!isWithinDistance(otherEntity, distance + extender))
            return false;

        // If we can't reach our current entity without the extender, but we CAN with our OTHER entity, return true.
        if (!isWithinDistance(currentEntity, distance) && isWithinDistance(otherEntity, distance))
            return true;

        float otherDist = getDistanceFromMouse(otherEntity),
        		currentDist = getDistanceFromMouse(currentEntity);
        return (otherDist < currentDist || currentDist == -1) && otherDist != -1;
    }
	
    private boolean isWithinDistance(EntityLivingBase entity, float distance) {
        return mc.thePlayer.getDistanceToEntity(entity) < distance;
    }

    private boolean isAliveNotUs(EntityLivingBase entity) {
		return (entity == null) ? false : (entity.isEntityAlive() && entity != mc.thePlayer && entity.getAge() != 7);
	}
	
	private boolean checkType(EntityLivingBase entity, boolean mob, boolean animal, boolean player) {
    	if (mob && entity instanceof IMob)
    		return true;
    	if (animal && entity instanceof IAnimals)
    		return true;
    	if (player && entity instanceof EntityPlayer)
    		return true;
    	return false;
    }
	
	/**
     * @return Distance the entity is from our mouse.
     */
    private float getDistanceFromMouse(EntityLivingBase entity) {
        float[] neededRotations = getRotationsNeeded(entity);
        if (neededRotations != null) {
            float neededYaw = mc.thePlayer.rotationYaw - neededRotations[0],
                    neededPitch = mc.thePlayer.rotationPitch - neededRotations[1];
            float distanceFromMouse = MathHelper.sqrt_float(neededYaw * neededYaw + neededPitch * neededPitch);
            return distanceFromMouse;
        }
        return -1F;
    }

    /**
     * @return Rotations needed to face the entity.
     */
	private float[] getRotationsNeeded(EntityLivingBase entity) {
        if (entity == null)
            return null;
        
        double xSize = entity.posX - mc.thePlayer.posX;
        double ySize = entity.posY + (double) entity.getEyeHeight() - (mc.thePlayer.posY + (double) mc.thePlayer.getEyeHeight());
        double zSize = entity.posZ - mc.thePlayer.posZ;
        
        double theta = (double) MathHelper.sqrt_double(xSize * xSize + zSize * zSize);
        float yaw = (float) (Math.atan2(zSize, xSize) * 180.0D / Math.PI) - 90.0F;
        float pitch = (float) (-(Math.atan2(ySize, theta) * 180.0D / Math.PI));
        return new float[] {
                mc.thePlayer.rotationYaw + MathHelper.wrapAngleTo180_float(yaw - mc.thePlayer.rotationYaw),
                mc.thePlayer.rotationPitch + MathHelper.wrapAngleTo180_float(pitch - mc.thePlayer.rotationPitch)
        };
	}

}
