package net.halalaboos.huzuni.plugin.plugins.combat;

import java.util.ArrayList;
import java.util.List;

import net.halalaboos.huzuni.ClickQueue;
import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.plugin.Category;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent;
import net.halalaboos.huzuni.event.tick.UpdateListener;
import net.halalaboos.huzuni.plugin.DefaultPlugin;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;

public class AutoArmor extends DefaultPlugin implements UpdateListener {
        
	public AutoArmor() {
		super("Auto Armor");
		setAuthor("Halalaboos");
		setDescription("Automagically places the best armor onto your armor thing.");
		setCategory(Category.COMBAT);

	}

	@Override
	public void onPreUpdate(MotionUpdateEvent event) {
		if (mc.currentScreen != null)
    		return;
        List<Integer> armors = getArmor();
        for (int i : armors) {
            Slot slot = mc.thePlayer.inventoryContainer.getSlot(i);
            int replacement = getReplacementSlot(slot);
            if (replacement != -1) {
            	ClickQueue.add(i, 0, 0);
            	ClickQueue.add(replacement, 0, 0);
            	ClickQueue.add(i, 0, 0);
                break;
            }
        }
    }
    
    /**
     * @return the wearable armor slot that associates with the armor in the slot specified.
     * */
    private int getReplacementSlot(Slot slot) {
        ItemArmor armor = (ItemArmor) slot.getStack().getItem();
        Slot wearingSlot = getWearingArmor(armor.armorType);
        if (wearingSlot.getHasStack()) {
            ItemArmor wearingArmor = (ItemArmor) wearingSlot.getStack().getItem();
            if (armor.damageReduceAmount > wearingArmor.damageReduceAmount) {
                return wearingSlot.slotNumber;
            } else if (armor.damageReduceAmount == wearingArmor.damageReduceAmount) {
            	int[] enchantments = new int[] {
            			Enchantment.protection.effectId,
            			Enchantment.fireProtection.effectId,
            			Enchantment.featherFalling.effectId,
            			Enchantment.thorns.effectId,
            			Enchantment.blastProtection.effectId,
            			Enchantment.projectileProtection.effectId
            	};
            	int stackLevels = 0, wearingLevels = 0;
                for (int enchantment : enchantments) {
                	int stackEnchantmentLevel = getEnchantmentLevel(enchantment, slot.getStack()), wearingEnchantmentLevel = getEnchantmentLevel(enchantment, wearingSlot.getStack());
                	stackLevels += stackEnchantmentLevel;
                    wearingLevels += wearingEnchantmentLevel;
                }
            	if (stackLevels > wearingLevels)
            		return wearingSlot.slotNumber;
            }
        } else
            return wearingSlot.slotNumber;

        return -1;
    }
    
    private int getEnchantmentLevel(int id, ItemStack itemStack) {
    	NBTTagList list = itemStack.getEnchantmentTagList();
    	for (int i = 0; i < list.tagCount(); i++) {
    		NBTTagCompound compound = list.getCompoundTagAt(i);
    		if (compound.getByte("id") == id)
    			return compound.getByte("lvl");
    	}
    	return 0;
    }

    /**
     * Finds all pieces of armor within the inventory.
     */
    private List<Integer> getArmor() {
        List<Integer> list = new ArrayList<Integer>();
        for (int o = 9; o < 45; o++) {
            if (mc.thePlayer.inventoryContainer.getSlot(o).getHasStack()) {
                ItemStack item = mc.thePlayer.inventoryContainer.getSlot(o).getStack();
                if (item != null)
                    if (item.getItem() instanceof ItemArmor)
                    	list.add(o);
            }
        }
        return list;
    }

	@Override
	public void onPostUpdate(MotionUpdateEvent event) {
	}

	@Override
	protected void onEnable() {
		Huzuni.registerUpdateListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterUpdateListener(this);
	}

	private Slot getWearingArmor(int armorType) {
		return mc.thePlayer.inventoryContainer.getSlot(5 + armorType);
	}
}
