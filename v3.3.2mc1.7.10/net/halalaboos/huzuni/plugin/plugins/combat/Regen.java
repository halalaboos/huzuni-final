package net.halalaboos.huzuni.plugin.plugins.combat;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.plugin.Category;
import net.halalaboos.huzuni.event.packet.PacketEvent;
import net.halalaboos.huzuni.event.packet.PacketListener;
import net.halalaboos.huzuni.plugin.DefaultPlugin;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.network.play.server.S06PacketUpdateHealth;

/**
 * @author brudin
 * @version 1.0
 * @since 12:24 AM on 8/6/2014
 */
public class Regen extends DefaultPlugin implements PacketListener {

	/**
	 * Just adding this because people claim it works on some servers, so yeah.
	 * I think it's dumb
	 */

	private boolean shouldRunThread = true;

	public Regen() {
		super("Regen");
		setCategory(Category.COMBAT);
		setAuthor("brudin");
		setDescription("Makes your health regenerate much faster.");
	}

	@Override
	public void onPacket(PacketEvent event, PacketEvent.PacketType type) {
		if(type == PacketEvent.PacketType.READ) {
			if(event.getPacket() instanceof S06PacketUpdateHealth) {
				S06PacketUpdateHealth packetUpdateHealth = (S06PacketUpdateHealth)event.getPacket();
				if(packetUpdateHealth.func_149332_c() <= 18F) {
					if(shouldRegenerate() && shouldRunThread) {
						new Thread() {
							@Override
							public void run() {
								shouldRunThread = false;
								for(short packets = 0; packets <= 2500; packets++) {
									mc.thePlayer.sendQueue.addToSendQueue(new C03PacketPlayer(true));
								}
								shouldRunThread = true;
							}
						}.start();
					}
				}
			}
		}
	}

	private boolean shouldRegenerate() {
		return mc.thePlayer.onGround && mc.thePlayer.getFoodStats().getFoodLevel() > 17 && !mc.thePlayer.isInWater() && !mc.thePlayer.isOnLadder();
	}

	@Override
	protected void onEnable() {
		Huzuni.registerPacketListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterPacketListener(this);
	}
}
