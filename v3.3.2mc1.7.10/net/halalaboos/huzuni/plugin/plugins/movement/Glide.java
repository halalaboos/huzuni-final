package net.halalaboos.huzuni.plugin.plugins.movement;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.plugin.Category;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent;
import net.halalaboos.huzuni.event.tick.UpdateListener;
import net.halalaboos.huzuni.plugin.DefaultPlugin;
import net.minecraft.block.material.Material;

/**
 * @author brudin
 * @version 1.0
 * @since 4/10/14
 */
public class Glide extends DefaultPlugin implements UpdateListener {

	public Glide() {
		super("Glide");
		setDescription("Makes you fall slower.");
		setAuthor("brudin");
		setCategory(Category.MOVEMENT);
	}

	@Override
	protected void onEnable() {
		Huzuni.registerUpdateListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterUpdateListener(this);
	}

	@Override
	public void onPreUpdate(MotionUpdateEvent event) {
		if (mc.thePlayer.motionY <= -0.125 && mc.thePlayer.isAirBorne && !mc.thePlayer.isInWater() && !mc.thePlayer.isOnLadder() && !mc.thePlayer.isInsideOfMaterial(Material.lava)) {
			mc.thePlayer.motionY = -0.125F;
			mc.thePlayer.jumpMovementFactor *= 1.21337f;
		}
	}

	@Override
	public void onPostUpdate(MotionUpdateEvent event) {

	}
}
