/**
 * 
 */
package net.halalaboos.huzuni.plugin.plugins.movement;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.plugin.Category;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent;
import net.halalaboos.huzuni.event.tick.UpdateListener;
import net.halalaboos.huzuni.plugin.VipPlugin;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.util.MathHelper;

/**
 * @author Halalaboos
 *
 * @since Oct 10, 2013
 */
public class FastLadder extends VipPlugin implements UpdateListener {

	public FastLadder() {
		super("Fast Ladder");
		setCategory(Category.MOVEMENT);
		setAuthor("brudin"); //tyga stole it from me >:(
		setDescription("Climbs ladders faster.");
		setVisible(false);
	}

	@Override
	protected void onEnable() {
		Huzuni.registerUpdateListener(this);
	}

	@Override
	protected void onDisable() {
		Huzuni.unregisterUpdateListener(this);
	}

	@Override
	public void onPreUpdate(MotionUpdateEvent event) {
        float multiplier = 0.25F;
        if(isOnLadder()) {
            mc.thePlayer.motionY = multiplier;
        }
    }

	private boolean isOnLadder() {
		int x = MathHelper.floor_double(mc.thePlayer.posX);
		int y = MathHelper.floor_double(mc.thePlayer.boundingBox.minY);
		int z = MathHelper.floor_double(mc.thePlayer.posZ);
		Block collidedAbove = mc.theWorld.getBlock(x, y + 2, z);
		Block collided = mc.theWorld.getBlock(x, y + 1, z);
		Block collidedBelow = mc.theWorld.getBlock(x, y, z);
		return collided == Blocks.ladder || collidedAbove == Blocks.ladder || collidedBelow == Blocks.ladder;
	}

	@Override
	public void onPostUpdate(MotionUpdateEvent event) {
	}

}
