package net.halalaboos.huzuni;

import java.util.ArrayList;
import java.util.List;

import net.halalaboos.huzuni.util.Timer;
import net.minecraft.client.Minecraft;

public class ClickQueue {
	
    private static final Timer timer = new Timer();

    private static final List<Integer[]> clicks = new ArrayList<Integer[]>();

    
    public static void add(int slot, int mouse, int shift) {
    	clicks.add(new Integer[] { slot, mouse, shift });
    }
    
    public static void add(int windowId, int slot, int mouse, int shift) {
    	clicks.add(new Integer[] { windowId, slot, mouse, shift });
    }
    
    public static void runClicks() {
    	if (hasQueue()) {
    		if (timer.hasReach(80)) {
    			Integer[] clickData = clicks.get(0);
    			if (clickData.length > 3)
    				clickSlot(clickData[0], clickData[1], clickData[2], clickData[3]);
    			else
    				clickSlot(Minecraft.getMinecraft().thePlayer.inventoryContainer.windowId, clickData[0], clickData[1], clickData[2]);
    			clicks.remove(clickData);
    			timer.reset();
    		}
    	}
    }
    
    public static boolean hasQueue() {
    	return !clicks.isEmpty();
    }
    
    private static void clickSlot(int windowId, int slot, int mouse, int shift) {
        Minecraft.getMinecraft().playerController.windowClick(windowId, slot, mouse, shift,
        		Minecraft.getMinecraft().thePlayer);
    }
}
