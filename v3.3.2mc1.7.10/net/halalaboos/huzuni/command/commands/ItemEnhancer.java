/**
 * 
 */
package net.halalaboos.huzuni.command.commands;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.console.Command;
import net.minecraft.client.Minecraft;

/**
 * @author Halalaboos
 *
 */
public class ItemEnhancer implements Command {
	
	@Override
	public String[] getAliases() {
		return new String[] { "enhance" };
	}

	@Override
	public String[] getHelp() {
		return new String[] { "enhance <length>" };
	}
	
	@Override
	public String getDescription() {
		return "Makes the name of your currently held item extremely large";
	}
	
	@Override
	public void run(String input, String[] args) {
		if (Minecraft.getMinecraft().playerController.isInCreativeMode()) {
			int length = Integer.parseInt(args[0]);
			String name = "";
	        for(int i = 0; i < length; i++)
	        	name = name + "\247k\247lg";
	        
	        Minecraft.getMinecraft().thePlayer.getCurrentEquippedItem().setStackDisplayName(name);
	       	Huzuni.addChatMessage("Now, drop the item and pick it back up again.");
		} else
			Huzuni.addChatMessage("You must be in creative mode!");
	}

}
