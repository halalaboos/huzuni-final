package net.halalaboos.huzuni.hook;

import net.halalaboos.huzuni.ClickQueue;
import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.command.CommandManager;
import net.halalaboos.huzuni.event.tick.MotionUpdateEvent;
import net.halalaboos.huzuni.gui.screen.HuzuniEditSign;
import net.halalaboos.huzuni.plugin.PluginManager;
import net.halalaboos.huzuni.plugin.plugins.combat.AntiKnockback;
import net.halalaboos.huzuni.plugin.plugins.player.Freecam;
import net.halalaboos.huzuni.plugin.plugins.world.AutoSign;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.stats.StatFileWriter;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntitySign;
import net.minecraft.util.Session;
import net.minecraft.world.World;

/**
 * PlayerControllerMP.func_147493_a replaced with our hook
 * */
public class EntityClientHook extends EntityClientPlayerMP {

	private static final MotionUpdateEvent preMotionUpdateEvent = new MotionUpdateEvent(MotionUpdateEvent.Type.PRE),
			postMotionUpdateEvent = new MotionUpdateEvent(MotionUpdateEvent.Type.POST);
	
	public EntityClientHook(Minecraft mc, World world,
							Session session, NetHandlerPlayClient netHandler,
							StatFileWriter statFileWriter) {
		super(mc, world, session, netHandler, statFileWriter);

	}

	@Override
	public void onUpdate() {
		super.onUpdate();
	}

	@Override
	public void func_146100_a(TileEntity entity)
	{
		if (Huzuni.isEnabled()) {
			if (entity instanceof TileEntitySign) {
				if (AutoSign.instance.isEnabled())
					AutoSign.instance.placeSign((TileEntitySign) entity);
				else
					this.mc.displayGuiScreen(new HuzuniEditSign((TileEntitySign) entity));
			} else {
				super.func_146100_a(entity);;
			}
		} else {
			super.func_146100_a(entity);;
		}
	}

	@Override
	public void sendMotionUpdates() {
		if (Huzuni.isEnabled()) {
			ClickQueue.runClicks();
			if (!ClickQueue.hasQueue()) {
				preMotionUpdateEvent.setCancelled(false);
				Huzuni.fireUpdateEvent(preMotionUpdateEvent);
				if (preMotionUpdateEvent.isCancelled())
					return;
				super.sendMotionUpdates();
				postMotionUpdateEvent.setCancelled(false);
				Huzuni.fireUpdateEvent(postMotionUpdateEvent);
			} else
				super.sendMotionUpdates();
		} else
			super.sendMotionUpdates();
		
	}

	@Override
	public void sendChatMessage(String par1Str) {
		if (par1Str.startsWith(Huzuni.COMMAND_PREFIX) && Huzuni.isEnabled()) {
			CommandManager.processCommand(par1Str.substring(1));
			return;
		} else {
			super.sendChatMessage(par1Str);
		}
	}

	@Override
	public void moveEntity(double motionX, double motionY, double motionZ) {
		super.moveEntity(motionX, motionY, motionZ);
	}

	@Override
	public boolean isEntityInsideOpaqueBlock() {
		if (Freecam.instance.isEnabled() && Huzuni.isEnabled())
			return false;
		return super.isEntityInsideOpaqueBlock();
	}

	@Override
	protected boolean func_145771_j(double par1, double par3, double par5) {
		if (Freecam.instance.isEnabled() && Huzuni.isEnabled()) {
			return false;
		}
		return super.func_145771_j(par1, par3, par5);
	}

	@Override
	public boolean isPushedByWater() {
		return Huzuni.isEnabled() ? (AntiKnockback.instance.isEnabled() ? AntiKnockback.instance.isPushedByWater() : super.isPushedByWater()) : super.isPushedByWater();
	}
}
