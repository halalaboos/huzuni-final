package net.halalaboos.huzuni.hook;

import org.lwjgl.opengl.GL11;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.NameProtect;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.util.ResourceLocation;

public class FontRendererHook extends FontRenderer {

	public FontRendererHook(GameSettings par1GameSettings,
			ResourceLocation par2ResourceLocation,
			TextureManager par3TextureManager, boolean par4) {
		super(par1GameSettings, par2ResourceLocation, par3TextureManager, par4);
		
	}

	@Override
	public int drawString(String text, int x, int y, int color, boolean shadow) {
		GL11.glEnable(GL11.GL_ALPHA_TEST);
		text = NameProtect.replace(text);
		if (Huzuni.isEnabled() && Huzuni.isCustomFont())
			return (int) (shadow ? Huzuni.drawStringWithShadow(text, x, y, color) : Huzuni.drawString(text, x, y, color));
		else
			return super.drawString(text, x, y, color, shadow);
	}
	
	@Override
	public int getStringWidth(String text) {
		text = NameProtect.replace(text);
		if (Huzuni.isEnabled() && Huzuni.isCustomFont())
			return Huzuni.getStringWidth(text);
		else
			return super.getStringWidth(text);
    }
}
