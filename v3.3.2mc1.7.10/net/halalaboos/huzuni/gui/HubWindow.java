package net.halalaboos.huzuni.gui;

import java.util.List;

import net.halalaboos.huzuni.api.gui.event.ActionListener;
import net.halalaboos.huzuni.api.gui.render.Theme;
import net.halalaboos.huzuni.gui.clickable.dropdown.Dropdown;
import net.halalaboos.huzuni.gui.clickable.slot.SlotComponent;
import net.halalaboos.huzuni.gui.clickable.window.Window;

public class HubWindow extends Window implements ActionListener <Dropdown> {

	private SlotComponent<Window> slotComponent;
	
	public HubWindow() {
		super("Hub");
		setup();
		this.layout();
	}

	
	public void setup() {
		slotComponent = new SlotComponent<Window>(100, 54) {

			@Override
			protected void onReleased(int index, Window window,
					int mouseX, int mouseY) {
				window.setActive(!window.isActive());
			}

			@Override
			protected void onClicked(int index, Window window,
					float[] area, int mouseX, int mouseY, int buttonId) {
			}
			
			@Override
			protected void render(Theme theme, int index, Window window, float[] area, boolean highlight, boolean mouseOver,
					boolean mouseDown) {
				highlight = window.isActive();
				super.render(theme, index, window, area, highlight, mouseOver, mouseDown);
			}
		};
		add(slotComponent);
		Dropdown dropdown = new Dropdown("Theme");
		
		String[] themes = new String[GuiManager.getWindowThemes().length];
		for (int i = 0; i < themes.length; i++)
			themes[i] = GuiManager.getWindowThemes()[i].getName();
		
		dropdown.setComponents(themes);
		dropdown.setSelectedComponent(GuiManager.getSelectedWindowThemeId());
		dropdown.addActionListener(this);
		add(dropdown);
		layout();
	}


	@Override
	public void onAction(Dropdown dropdown) {
		int selected = dropdown.getSelectedComponent();
		GuiManager.setSelectedWindowTheme(selected);
	}
	
	public void copyWindows(List<Window> windows) {
		for (Window window : windows) {
			if (window != this && !slotComponent.contains(window))
				slotComponent.add(window);
		}
	}
}
