package net.halalaboos.huzuni.gui.screen;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.gui.GuiManager;
import net.halalaboos.huzuni.gui.screen.builder.WindowBuilder;
import net.halalaboos.huzuni.gui.screen.keybinds.KeybindManager;
import net.halalaboos.huzuni.gui.screen.macro.MacroManager;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;

import org.lwjgl.input.Keyboard;

public class HuzuniOptions extends GuiScreen {

	private GuiScreen lastScreen;

	private GuiButton lastMousedButton;

	public HuzuniOptions(GuiScreen lastScreen) {
		this.lastScreen = lastScreen;
	}


	@Override
	public void initGui() {
		Keyboard.enableRepeatEvents(true);
		buttonList.clear();

		int leftAlign = this.width / 2 - 152,
				rightAlign = this.width / 2 + 2,
				bottomTop = this.height / 6 + 96 - 6,
				bottomCenter = this.height / 6 + 120 - 6,
				bottomBottom = this.height / 6 + 144 - 6,
				topExtraTop = this.height / 6 - 12,
				topTop = height / 6 - 12 + 24,
				topCenter = height / 6 - 12 + 48,
				topBottom = topCenter + 24;

		// Top portion
		buttonList.add(new GuiButton(4, rightAlign, topTop, 150, 20, "Theme: " + GuiManager.getIngameTheme().getName()));
		buttonList.add(new GuiButton(3, rightAlign, topCenter, 150, 20, "Huzuni: " + parseState(Huzuni.isEnabled())));
		buttonList.add(new GuiButton(5, leftAlign, topTop, 150, 20, "Custom Font: " + parseState(Huzuni.isCustomFont())));
		buttonList.add(new GuiButton(13, leftAlign, topCenter, 150, 20, "Reset Windows"));
		buttonList.add(new GuiButton(14, leftAlign, topBottom, 150, 20, "Macros"));
        GuiButton windowBuilder = new GuiButton(1, rightAlign, topBottom, 150, 20, "Window Builder (unfinished)");
        windowBuilder.enabled = false;
		buttonList.add(windowBuilder);

		// Bottom portion
		buttonList.add(new HuzuniSlider(12, rightAlign, bottomTop, "Line Size", "Line Size", 0.25F, Huzuni.getLineSize(), 10F));
		buttonList.add(new GuiButton(11, leftAlign, bottomTop, 150, 20, "Antialias: " + parseState(Huzuni.isAntialias())));
		buttonList.add(new GuiButton(6, rightAlign, bottomCenter, 150, 20, "Font"));
		buttonList.add(new GuiButton(7, leftAlign, bottomCenter, 150, 20, "Keybinds"));

		buttonList.add(new GuiButton(0, this.width / 2 - 100, this.height / 6 + 168, "Done"));
	}

	@Override
	protected void actionPerformed(GuiButton button) {
		lastMousedButton = button;
		switch (button.id) {
			case 0:
				mc.displayGuiScreen(lastScreen);
				break;
			case 1:
				mc.displayGuiScreen(new WindowBuilder(this));
				initGui();
				break;
			case 2:
				Huzuni.setAntialias(!Huzuni.isAntialias());
				initGui();
				break;
			case 3:
				Huzuni.setEnabled(!Huzuni.isEnabled());
				initGui();
				break;
			case 4:
				GuiManager.nextTheme();
				initGui();
				break;
			case 5:
				Huzuni.setCustomFont(!Huzuni.isCustomFont());
				initGui();
				break;
			case 6:
				mc.displayGuiScreen(new FontChanger(this));
				break;
			case 7:
				mc.displayGuiScreen(new KeybindManager(this));
				break;
			case 8:
				initGui();
				break;
			case 10:
				initGui();
				break;
			case 11:
				Huzuni.setAntialias(!Huzuni.isAntialias());
				initGui();
				break;
			case 13:
				GuiManager.resetWindows();
				break;
			case 14:
				mc.displayGuiScreen(new MacroManager(this));
				break;
			default: {

			}
		}
	}

	@Override
	protected void mouseMovedOrUp(int x, int y, int mouseMoveOrUp) {
		super.mouseMovedOrUp(x, y, mouseMoveOrUp);
		if (this.lastMousedButton != null) {
			if (lastMousedButton instanceof HuzuniSlider) {
				HuzuniSlider slider = (HuzuniSlider) lastMousedButton;
				switch (slider.id) {
					case 12:
						Huzuni.setLineSize(slider.getSliderValue());
						break;
				}
				lastMousedButton = null;
			}
		}
	}

	private String parseState(boolean state) {
		return state ? "On" : "Off";
	}

	@Override
	public void onGuiClosed() {
		Keyboard.enableRepeatEvents(false);
	}

	@Override
	public void drawScreen(int par1, int par2, float par3) {
		super.drawDefaultBackground();
		super.drawScreen(par1, par2, par3);

		drawCenteredString(this.fontRendererObj, Huzuni.TITLE + " Options", width / 2, 16, 0xFFFFFF);
	}

	@Override
	protected void keyTyped(char c, int keyCode) {
		super.keyTyped(c, keyCode);
	}

	@Override
	protected void mouseClicked(int x, int y, int buttonID) {
		super.mouseClicked(x, y, buttonID);
	}
}
