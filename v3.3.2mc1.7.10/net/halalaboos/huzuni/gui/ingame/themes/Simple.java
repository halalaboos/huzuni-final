package net.halalaboos.huzuni.gui.ingame.themes;

import net.halalaboos.huzuni.gui.ingame.IngameTheme;
import net.halalaboos.huzuni.plugin.DefaultPlugin;
import net.halalaboos.huzuni.plugin.PluginManager;
import net.minecraft.client.Minecraft;
import org.lwjgl.opengl.GL11;

public class Simple implements IngameTheme {
    @Override
    public void render(Minecraft mc, int screenWidth, int screenHeight) {
		int posY = 2;
		for (DefaultPlugin plugin : PluginManager.getDefaultPlugins()) {
			if (plugin.isEnabled() && plugin.getCategory() != null && plugin.getVisible()) {
				mc.fontRenderer.drawStringWithShadow(plugin.getRenderName(), 2, posY, 0xDFCCCCCC);
				posY += 10;
			}
		}
    }

    @Override
    public String getName() {
        return "Simple";
    }

    @Override
    public void onKeyTyped(int keyCode) {
    }
}
