package net.halalaboos.huzuni.gui.ingame.themes;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.gui.ingame.IngameTheme;
import net.halalaboos.huzuni.plugin.DefaultPlugin;
import net.halalaboos.huzuni.plugin.PluginManager;
import net.halalaboos.huzuni.util.GLUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.util.StringUtils;

public class Skidded implements IngameTheme {
    int width, height;

    @Override
    public void render(Minecraft mc, int screenWidth, int screenHeight) {
        mc.fontRenderer.drawStringWithShadow(Huzuni.TITLE + " v" + Huzuni.VERSION, 2, 2, 0xFFFF);
        GLUtils.drawRect(1, 1, width, height, 0xAF424242);
        height = 12;
        width = 0;

        for (DefaultPlugin plugin : PluginManager.getDefaultPlugins()) {
            if (plugin.isBound()) {
                String renderString = plugin.getKeyName() + " = \247" + (plugin.isEnabled() ? "2" : "4") + plugin.getName() + "\247f = " + (plugin.isEnabled() ? "on" : "off");
                mc.fontRenderer.drawStringWithShadow(renderString, 2, height, 0xFFFF);
                height += 12;

                int stringWidth = mc.fontRenderer.getStringWidth(StringUtils.stripControlCodes(renderString)) + 4;
                if (stringWidth > width)
                    width = stringWidth;
            }
        }
    }

    @Override
    public String getName() {
        return "Skidded";
    }

    @Override
    public void onKeyTyped(int keyCode) {
    }

}
