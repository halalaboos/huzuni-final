/**
 *
 */
package net.halalaboos.huzuni.gui.ingame.themes;

import net.halalaboos.huzuni.gui.ingame.IngameTheme;
import net.halalaboos.huzuni.plugin.DefaultPlugin;
import net.halalaboos.huzuni.plugin.PluginManager;
import net.halalaboos.huzuni.util.GLUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.util.MathHelper;
import net.minecraft.world.chunk.Chunk;
import org.lwjgl.opengl.GL11;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Halalaboos
 * @since Aug 3, 2013
 */
public class Informative implements IngameTheme {

	private final Minecraft mc = Minecraft.getMinecraft();
	
    @Override
    public void render(Minecraft mc, int screenWidth, int screenHeight) {
        String coords = "XYZ: " + (int) mc.thePlayer.posX + ", " + (int) mc.thePlayer.posY + ", " + (int) mc.thePlayer.posZ;
        String biome = "Biome: " + getBiome();
        String time = "Time: " + getTime();
        String fps = "FPS: " + mc.debug.split(" ")[0];
        String[] labels = new String[] { coords, biome, time, fps};

        int currentFPS = Integer.parseInt(mc.debug.split(" ")[0]);
        int width = getWidest(labels);
        int yPos = 1;

        if(width < getWidest()) {
            width = getWidest();
        }

        GLUtils.drawRect(0, 0, width, 40, 0x9f000000);
        yPos = renderLabel(labels, 2, yPos);
        GLUtils.drawRect(0, yPos, width, yPos + 2, 0x5f000000);
        GLUtils.drawRect(0, yPos, currentFPS > width ? width : currentFPS, yPos + 2, 0x8fff00ff);
        yPos += 4;
        GLUtils.drawRect(0, yPos, width, yPos + 2, 0xffffff00);
        GLUtils.drawRect(0, 42, width, yPos + getArrayHeight(), 0x8f000000);
        yPos += 4;

        for (DefaultPlugin plugin : PluginManager.getDefaultPlugins()) {
            if (plugin.getVisible() && plugin.isEnabled()) {
                mc.fontRenderer.drawStringWithShadow(plugin.getRenderName(), 2, yPos, 0x525252);
                yPos += 9;
            }
        }
		drawGradientRect(0, yPos, width, yPos + 4, 0x8f000000, 0);
    }

	private void drawGradientRect(float x, float y, float x2, float y2, int col1, int col2) {
		float f = (float)(col1 >> 24 & 0xFF) / 255F;
		float f1 = (float)(col1 >> 16 & 0xFF) / 255F;
		float f2 = (float)(col1 >> 8 & 0xFF) / 255F;
		float f3 = (float)(col1 & 0xFF) / 255F;

		float f4 = (float)(col2 >> 24 & 0xFF) / 255F;
		float f5 = (float)(col2 >> 16 & 0xFF) / 255F;
		float f6 = (float)(col2 >> 8 & 0xFF) / 255F;
		float f7 = (float)(col2 & 0xFF) / 255F;

		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_ALPHA_TEST);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glShadeModel(GL11.GL_SMOOTH);

		GL11.glPushMatrix();
		GL11.glBegin(GL11.GL_QUADS);
		GL11.glColor4f(f1, f2, f3, f);
		GL11.glVertex2d(x2, y);
		GL11.glVertex2d(x, y);

		GL11.glColor4f(f5, f6, f7, f4);
		GL11.glVertex2d(x, y2);
		GL11.glVertex2d(x2, y2);
		GL11.glEnd();
		GL11.glPopMatrix();

		GL11.glDisable(GL11.GL_BLEND);
		GL11.glEnable(GL11.GL_ALPHA_TEST);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
	}

    private int getWidest(String[] s) {
        int curWidth = 20;
        for(String string : s) {
            if(mc.fontRenderer.getStringWidth(string) > curWidth) {
                curWidth = mc.fontRenderer.getStringWidth(string) + 15;
            }
        }
        return curWidth;
    }

    private int renderLabel(String[] label, int x, int y) {
        for(String s : label) {
            mc.fontRenderer.drawStringWithShadow(s, x, y, 0xffffff);
            y += 9;
        }
        return y;
    }

    private int getWidest() {
    	int width = 50;
    	for (DefaultPlugin plugin : PluginManager.getDefaultPlugins()) {
    		if (plugin.isEnabled() && plugin.getVisible()) {
    			if (width < mc.fontRenderer.getStringWidth(plugin.getRenderName()) + 6)
    				width = mc.fontRenderer.getStringWidth(plugin.getRenderName()) + 6;

    		}
    	}
    	return width;
    }

    private int getArrayHeight() {
        int yPos = 4;
        for (DefaultPlugin plugin : PluginManager.getDefaultPlugins()) {
    		if (plugin.isEnabled() && plugin.getVisible()) {
                yPos += 9;
            }
        }
        return yPos;
    }

    /**
     * @return Biome name we're in.
     */
    private String getBiome() {
        int x = MathHelper.floor_double(this.mc.thePlayer.posX);
        int z = MathHelper.floor_double(this.mc.thePlayer.posZ);
        Chunk currentChunk = this.mc.theWorld.getChunkFromBlockCoords(x, z);
        return currentChunk.getBiomeGenForWorldCoords(x & 15, z & 15, this.mc.theWorld.getWorldChunkManager()).biomeName;
    }

    /**
     * @return Current formatted time.
     */
    private String getTime() {
        SimpleDateFormat date = new SimpleDateFormat("h:mm a");
        return date.format(new Date());
    }

    @Override
    public String getName() {
        return "Informative";
    }

    @Override
    public void onKeyTyped(int keyCode) {
    }

}
