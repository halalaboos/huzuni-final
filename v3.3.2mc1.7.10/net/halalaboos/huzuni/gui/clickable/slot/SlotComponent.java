package net.halalaboos.huzuni.gui.clickable.slot;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import net.halalaboos.huzuni.Huzuni;
import net.halalaboos.huzuni.api.gui.render.Theme;
import net.halalaboos.huzuni.gui.clickable.DefaultComponent;
import net.halalaboos.huzuni.util.GLUtils;
import net.halalaboos.huzuni.util.MathUtils;

import org.lwjgl.input.Mouse;

public abstract class SlotComponent <T> extends DefaultComponent {
	private final int SLIDER_POINT_WIDTH = 6;
	private final int SLIDER_POINT_HEIGHT = 12;
	private final int SLIDER_PADDING = 1;

	private int componentPadding = 1;
	private List<T> components = new CopyOnWriteArrayList<T>();
	private float scrollPercentage = 0;
	private T clickedComponent;
	private int selectedComponent = -1;

	protected boolean dragging = false, renderBackground = true, hasSlider = true;

	public SlotComponent(float width, float height) {
		super();
		this.setWidth(width);
		this.setHeight(height);
	}
	
	@Override
	public void update() {
		super.update();
		handleScrollWheel();
		handleDragging();
	}

	@Override
	public void mouseReleased(int x, int y, int buttonId) {
		if (isMouseOver() && clickedComponent != null) {

			int index = components.indexOf(clickedComponent);

			int yPos = /* This represents 'i' */(index - getScrolledIndex()) * (getComponentHeight() + componentPadding);

			if (MathUtils.inside(x, y, (int) (this.getOffsetX() + this.getX() + 2), (int) (this.getOffsetY() + this.getY() + yPos), (int) (getWidth() - (hasEnoughToScroll() && hasSlider ? (getSliderPoint()[3]) : 0) - 4), getComponentHeight())) {
				selectedComponent = index;
				onReleased(index, clickedComponent, x, y);
			}

			clickedComponent = null;
		}
		dragging = false;
	}

	@Override
	public boolean mouseClicked(int x, int y, int buttonId) {
		if (isMouseOver()) {
			float[] sliderPoint = getSliderPoint();
			if (MathUtils.inside(x, y, sliderPoint)) {
				dragging = true;
			}
			int yPos = 2;
			if (!components.isEmpty()) {
				int scrolledIndex = getScrolledIndex();
				for (int i = 0; scrolledIndex + i < components.size() && i < getMaxRenderableObjects() && scrolledIndex + i >= 0; i++) {
					T component = components.get(scrolledIndex + i);
					float[] componentArea = new float[] { (int) (this.getOffsetX() + this.getX() + 2), (int) (this.getOffsetY() + this.getY() + yPos), (int) (getWidth() - (hasEnoughToScroll() && hasSlider ? (sliderPoint[3]) : 0) - 4), getComponentHeight() };
					if (MathUtils.inside(x, y, componentArea)) {
						clickedComponent = component;
						onClicked(scrolledIndex + i, component, componentArea, x, y, buttonId);
						break;
					}
					yPos += componentArea[3] + componentPadding;
				}
			}
		}
		return super.mouseClicked(x, y, buttonId);
	}

	public void renderComponents(Theme theme, float offsetX, float offsetY, boolean mouseOver, boolean mouseDown) {
		if (!components.isEmpty()) {
			int yPos = 2;
			float[] sliderPoint = getSliderPoint();
			int scrolledIndex = getScrolledIndex();
			float[] mouse = new float[] { GLUtils.getMouseX(), GLUtils.getMouseY() };
			for (int i = 0; scrolledIndex + i < components.size() && i < getMaxRenderableObjects() && scrolledIndex + i >= 0; i++) {
				T component = components.get(scrolledIndex + i);
				float[] componentArea = new float[] { getX() + offsetX + 2, getY() + offsetY + yPos, getWidth() - (hasEnoughToScroll() && hasSlider ? (sliderPoint[2]) : 0) - 4, getComponentHeight() };
				render(theme, scrolledIndex + i, component, componentArea, selectedComponent == scrolledIndex + i, (mouseOver && MathUtils.inside(mouse[0], mouse[1], componentArea)) && (clickedComponent == null ? true : clickedComponent == component), mouseDown && clickedComponent == component);
				yPos += componentArea[3] + componentPadding;
			}
		}
	}

	private void handleScrollWheel() {
		if (this.isMouseOver()) {
			if (Mouse.hasWheel()) {
				int amount = Mouse.getDWheel();
				if (amount > 0)
					amount = -1;
				else if (amount < 0)
					amount = 1;
				else if (amount == 0)
					return;
				if (clickedComponent == null) {
					if (hasEnoughToScroll()) {
						scrollPercentage += ((float) amount / (float) getObjectsNotRendered());
						keepSafe();
					}
				}
			}
		}
	}

	private void handleDragging() {
		if (dragging) {
			if (!hasEnoughToScroll() || !hasSlider) {
				dragging = false;
				return;
			}
			//get the difference (in pixels) from the cursor and the beginning of the slider boundaries ( (mouse X) / (slider X) ) and subtract the width of the slider.
			float differenceWithMouseAndSliderBase = (float) (GLUtils.getMouseY() - (this.getY() + this.getOffsetY())) - (SLIDER_POINT_HEIGHT / 2F);

			//converted into 0.0F ~ 1.0F percentage
			scrollPercentage = differenceWithMouseAndSliderBase / getHeightForPoint();
			this.keepSafe();
		}
	}

	public float[] getSliderPoint() {
		int scrollbarY = (int) (this.scrollPercentage * getHeightForPoint());
		return new float[] { getOffsetX() + getX() + getWidth() - SLIDER_POINT_WIDTH - SLIDER_PADDING,
				getOffsetY() + getY() + scrollbarY + SLIDER_PADDING,
				SLIDER_POINT_WIDTH,
				SLIDER_POINT_HEIGHT };
	}

	public boolean hasEnoughToScroll() {
		return getMaxRenderableObjects() < components.size();
	}

	protected float getHeightForPoint() {
		float maxPointForRendering = (float) (getHeight() - SLIDER_POINT_HEIGHT - SLIDER_PADDING),
				beginPoint = (SLIDER_PADDING);
		return maxPointForRendering - beginPoint;
	}

	private int getMaxRenderableObjects() {
		return (int) Math.floor(getHeight() / (getComponentHeight() + componentPadding));
	}

	private int getObjectsNotRendered() {
		return components.size() - getMaxRenderableObjects();
	}

	private void keepSafe() {
		if (scrollPercentage > 1)
			scrollPercentage = 1;
		if (scrollPercentage < 0)
			scrollPercentage = 0;
	}

	private int getScrolledIndex() {
		int maxSize = getObjectsNotRendered();
		return (int) (scrollPercentage * maxSize);
	}

	public void add(T component) {
		components.add(component);
	}
	
	public void remove(T component) {
		components.add(component);
	}
	
	public boolean contains(T component) {
		return components.contains(component);
	}
	
	public void clear() {
		components.clear();
		this.scrollPercentage = 0F;
	}
	
	public int getComponentPadding() {
		return componentPadding;
	}

	public void setComponentPadding(int componentPadding) {
		this.componentPadding = componentPadding;
	}

	public List<T> getComponents() {
		return components;
	}

	public void setComponents(List<T> components) {
		this.components = components;
		this.scrollPercentage = 0F;
	}

	public int getSelectedComponent() {
		return selectedComponent;
	}
	
	public T getSelectedComponentO() {
		return components.get(selectedComponent);
	}

	public void setSelectedComponent(int selectedComponent) {
		this.selectedComponent = selectedComponent;
	}

	public boolean isRenderBackground() {
		return renderBackground;
	}

	public void setRenderBackground(boolean renderBackground) {
		this.renderBackground = renderBackground;
	}

	public boolean hasSlider() {
		return hasSlider;
	}

	public void setHasSlider(boolean hasSlider) {
		this.hasSlider = hasSlider;
	}
	

	public boolean isDragging() {
		return dragging;
	}

	public void setDragging(boolean dragging) {
		this.dragging = dragging;
	}

	public float getScrollPercentage() {
		return scrollPercentage;
	}

	public void setScrollPercentage(float scrollPercentage) {
		this.scrollPercentage = scrollPercentage;
	}

	public boolean hasSelectedComponent() {
		return selectedComponent >= 0 && selectedComponent < components.size();
	}
	
	protected abstract void onReleased(int index, T component, int mouseX, int mouseY);

	protected abstract void onClicked(int index, T component, float[] area, int mouseX, int mouseY, int buttonId);

	protected void render(Theme theme, int index, T component, float[] area, boolean highlight, boolean mouseOver,
			boolean mouseDown) {
		theme.renderSlot(this.getOffsetX(), this.getOffsetY(), index, area, highlight, mouseOver, mouseDown);
		Huzuni.drawString(component.toString(), area[0] + 2, area[1] + 2, 0xFFFFFF);
	}

	protected int getComponentHeight() {
		return 12;
	}

}
