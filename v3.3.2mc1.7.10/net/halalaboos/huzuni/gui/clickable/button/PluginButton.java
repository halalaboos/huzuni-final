package net.halalaboos.huzuni.gui.clickable.button;

import org.lwjgl.input.Mouse;
import net.halalaboos.huzuni.api.gui.render.Theme;
import net.halalaboos.huzuni.api.plugin.Option;
import net.halalaboos.huzuni.api.plugin.Plugin;
import net.halalaboos.huzuni.util.GLUtils;
import net.halalaboos.huzuni.util.MathUtils;

public class PluginButton extends Button {

	public static final int DROPDOWN_WIDTH = 8; 
	
	private final Plugin plugin;
	
	private boolean down = false;
	
	private int textPadding = 1, preferredComponents = 6;
	
	private float scrollPercentage = 0;
	
	private Option lastClicked = null;

	public PluginButton(Plugin plugin) {
		super(plugin.getName());
		this.plugin = plugin;
		this.setTooltip(plugin.getDescription());
	}

	@Override
	public boolean isHighlight() {
		return plugin.isEnabled();
	}

	@Override
	public String getTitle() {
		return plugin.getName();
	}
	
	@Override
	public void update() {
		super.update();
		handleScrollWheel();
	}
	
	private void handleScrollWheel() {
		if (this.isMouseOver()) {
			if (Mouse.hasWheel()) {
				int amount = Mouse.getDWheel();
				if (amount > 0)
					amount = -1;
				else if (amount < 0)
					amount = 1;
				else if (amount == 0)
					return;
				if (hasEnoughToScroll()) {
					scrollPercentage += ((float) amount / (float) getObjectsNotRendered());
					keepSafe();
				}
				
			}
		}
	}
	
	public boolean hasEnoughToScroll() {
		return preferredComponents < plugin.length();
	}
	
	private int getObjectsNotRendered() {
		return plugin.length() - preferredComponents;
	}
	
	public int getDownHeight() {
		int totalHeight = 0, scrolledIndex = getScrolledIndex();
		for (int i = 0; scrolledIndex + i < plugin.length() && i < preferredComponents && scrolledIndex + i >= 0; i++) 
			totalHeight +=  getComponentHeight(plugin.get(scrolledIndex + i)) + textPadding;
		return totalHeight;
	}
	
	private void keepSafe() {
		if (scrollPercentage > 1)
			scrollPercentage = 1;
		if (scrollPercentage < 0)
			scrollPercentage = 0;
	}
	
	private int getScrolledIndex() {
		return (int) (scrollPercentage * getObjectsNotRendered());
	}

	@Override
	protected void invokeActionListeners() {
		super.invokeActionListeners();
		plugin.toggle();
	}
		
	@Override
	public boolean mouseClicked(int x, int y, int buttonId) {
		if (hasDropdown() && down) {
			int yPos = (int) (getHeight() + 2);
			int scrolledIndex = getScrolledIndex();
			for (int i = 0; scrolledIndex + i < plugin.length() && i < preferredComponents && scrolledIndex + i >= 0; i++) {
				float[] componentBoundaries = new float[] { this.getX() + this.getOffsetX() + 1, this.getY() + this.getOffsetY() + yPos, this.getWidth() - (this.hasEnoughToScroll() ? 9 : 4), getComponentHeight(plugin.get(scrolledIndex + i)) };
				if (MathUtils.inside(x, y, componentBoundaries)) {
					plugin.get(scrolledIndex + i).mouseClicked(x, y, buttonId);
					lastClicked = plugin.get(scrolledIndex + i);
					return true;
				}
				yPos += componentBoundaries[3] + textPadding;
			}
			down = !super.isPointInside(x, y) && this.isPointInside(x, y);
		} else {
			down = this.isPointInsideDropdown(x, y);
			if (down)
				return true;
		}
		return !isPointInsideDropdown(x, y) && super.mouseClicked(x, y, buttonId);
	}
	
	@Override
	public void mouseReleased(int x, int y, int buttonId) {
		if (hasDropdown() && down) {
			if (lastClicked != null) {
				lastClicked.mouseReleased(x, y, buttonId);
				lastClicked = null;
			}
			return;
		} else
			super.mouseReleased(x, y, buttonId);
	}
	
	public void renderComponents(Theme theme, float offsetX, float offsetY, boolean mouseOver, boolean mouseDown) {
		int yPos = (int) (getHeight() + 2);
		int scrolledIndex = getScrolledIndex();
		float[] mouse = new float[] { GLUtils.getMouseX(), GLUtils.getMouseY() };
		for (int i = 0; scrolledIndex + i < plugin.length() && i < preferredComponents && scrolledIndex + i >= 0; i++) {
			Option option = plugin.get(scrolledIndex + i);
			float[] componentArea = new float[] { getX() + offsetX + 2, getY() + offsetY + yPos, getWidth() - (this.hasEnoughToScroll() ? 9 : 4), getComponentHeight(option) };
			option.setArea(componentArea);
			option.render(theme, scrolledIndex + i, componentArea, (mouseOver && MathUtils.inside(mouse[0], mouse[1], componentArea)), mouseDown);
			yPos += componentArea[3] + textPadding;
		}
	}
	
	private int getComponentHeight(Option option) {
		return (int) option.getHeight();
	}
	
	@Override
	public boolean isPointInside(int x, int y) {
		if (hasDropdown() && down) {
			float x1 = this.getX() + this.getOffsetX();
			float y1 = this.getY() + this.getOffsetY();
			float height = this.getHeight() + this.getDownHeight();
			float width = getWidth() - 2;
			return x1 < x && x1 + width > x && y1 < y && y1 + height > y;
		} else
			return super.isPointInside(x, y);
	}
	
	public float[] getSliderPoint() {
		int scrollbarY = (int) (this.scrollPercentage * getHeightForPoint());
		return new float[] { getOffsetX() + getX() + getWidth() - 4 - 1,
				getOffsetY() + getY() + getHeight() + scrollbarY + 2,
				4,
				8 };
	}
	
	protected float getHeightForPoint() {
		float maxPointForRendering = (float) (getDownHeight() - 8 - 1),
				beginPoint = (1);
		return maxPointForRendering - beginPoint;
	}
	
	public float[] getDropdown() {
		float x1 = this.getX() + getWidth() - DROPDOWN_WIDTH + this.getOffsetX();
		float y1 = this.getY() + this.getOffsetY();
		float height = getHeight();
		float width = DROPDOWN_WIDTH;
		return new float[] { x1, y1, width, height };
	}
	
	public boolean isPointInsideDropdown(int x, int y) {
		return hasDropdown() && MathUtils.inside(x, y, getDropdown());
	}

	public Plugin getPlugin() {
		return plugin;
	}
	
	@Override
	public String getCommand() {
		return "t " + plugin.getName();
	}
	
	public boolean hasDropdown() {
		return plugin.length() > 0;
	}

	public boolean isDown() {
		return down;
	}

	public void setDown(boolean down) {
		this.down = down;
	}

	public int getTextPadding() {
		return textPadding;
	}

	public void setTextPadding(int textPadding) {
		this.textPadding = textPadding;
	}
}
