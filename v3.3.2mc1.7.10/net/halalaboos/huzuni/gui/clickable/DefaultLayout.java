package net.halalaboos.huzuni.gui.clickable;

import java.awt.Rectangle;
import java.util.List;

import net.halalaboos.huzuni.api.gui.Component;
import net.halalaboos.huzuni.api.gui.Container;
import net.halalaboos.huzuni.api.gui.layout.Layout;


public class DefaultLayout implements Layout {
    
	private final int COMPONENT_PADDING = 1;

    @Override
    public Rectangle layout(Container container, List<Component> components) {
        float biggestWidth = container.getWidth();
        float y = COMPONENT_PADDING;
        for (Component component : components) {
            if (biggestWidth < component.getWidth())
                biggestWidth = component.getWidth();
        }
        
        for (Component component : components) {
        	component.layout(COMPONENT_PADDING, y, biggestWidth - COMPONENT_PADDING, component.getHeight());
            if ((int) (y + component.getHeight() + COMPONENT_PADDING) > y)
                y += component.getHeight() + COMPONENT_PADDING;
        }
        return new Rectangle(0, 0, (int) (biggestWidth + COMPONENT_PADDING), (int) y);
    }

}
