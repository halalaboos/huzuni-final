package net.halalaboos.huzuni.gui.clickable.slider;

import net.halalaboos.huzuni.plugin.value.Value;

public class ValueSlider extends Slider {

	private final Value value;
	
	public ValueSlider(Value value) {
		super(value.getName(), value.getMinValue(), value.getValue(), value.getMaxValue(), value.getIncrementValue());
		this.value = value;
	}
	
	@Override
	protected void invokeActionListeners() {
		super.invokeActionListeners();
		value.setValue(getValue());
	}

	public Value getValueObj() {
		return value;
	}
}
