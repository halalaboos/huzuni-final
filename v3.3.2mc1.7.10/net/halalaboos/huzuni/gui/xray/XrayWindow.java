package net.halalaboos.huzuni.gui.xray;

import java.util.ArrayList;
import java.util.List;

import net.halalaboos.huzuni.api.gui.Component;
import net.halalaboos.huzuni.api.gui.event.KeyListener;
import net.halalaboos.huzuni.gui.clickable.slot.SlotComponent;
import net.halalaboos.huzuni.gui.clickable.textfield.TextField;
import net.halalaboos.huzuni.gui.clickable.window.Window;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class XrayWindow extends Window implements KeyListener {
	
	private final int[] badBlocks = new int[] {
    		31, 32, 34, 36, 50, 54, 55, 59, 63, 64, 68, 71, 93, 94, 95, 104, 105, 118, 119, 127, 131, 132, 140, 141, 142, 146, 149, 150
    };
    
	private final SlotComponent <ItemStack[]> slotBlocks;
   
    private final TextField textField;
    
	public XrayWindow() {
		super("Xray Manager");
		textField = new TextField();
        textField.addKeyListener(this);
        textField.setTooltip("Search for blocks.");
        add(textField);
        slotBlocks = new SlotBlocks(136, 68);
        slotBlocks.setComponents(setupBlocks());
        add(slotBlocks);
        this.layout();
	}
	
	/**
     * Finds blocks and sets up a list with an array of itemstacks in it.
     * */
    private List<ItemStack[]> searchBlock(String blockName) {
        List<ItemStack[]> goodBlocks = new ArrayList<ItemStack[]>();
        List<ItemStack> tempBlocks = new ArrayList<ItemStack>();;
    	 for (Object o : Block.blockRegistry) {
    		 Block block = (Block) o;
    		 if (block != null && Item.getItemFromBlock(block) != null) {
    			 boolean bad = false;
         		if (!bad) {
         			ItemStack itemStack = new ItemStack(block);
         			if (itemStack.getDisplayName().toLowerCase().contains(blockName.toLowerCase())) {
         				tempBlocks.add(itemStack);
    	        		if (tempBlocks.size() >= 6) {
    	        			goodBlocks.add(tempBlocks.toArray(new ItemStack[tempBlocks.size()]));
    	        			tempBlocks.clear();
    	        		}	
         			}
         		}
    		 }
    	 }
    	 if (!tempBlocks.isEmpty())
    		 goodBlocks.add(tempBlocks.toArray(new ItemStack[tempBlocks.size()]));
    	 tempBlocks.clear();
    	 return goodBlocks;
    }
    
    /**
     * Sets up all blocks for the x-ray window.
	 * @return
	 */
	private List<ItemStack[]> setupBlocks() {
        List<ItemStack[]> goodBlocks = new ArrayList<ItemStack[]>();
        List<ItemStack> tempBlocks = new ArrayList<ItemStack>();;
        
        for (Object o : Block.blockRegistry) {
        	Block block = (Block) o;
        	if (block != null && Item.getItemFromBlock(block) != null) {
        		boolean bad = false;
        		if (!bad) {
	        		tempBlocks.add(new ItemStack(block));
	        		if (tempBlocks.size() >= 6) {
	        			goodBlocks.add(tempBlocks.toArray(new ItemStack[tempBlocks.size()]));
	        			tempBlocks.clear();
	        		}
        		}
        	}
        }
        if (!tempBlocks.isEmpty())
        	goodBlocks.add(tempBlocks.toArray(new ItemStack[tempBlocks.size()]));
        tempBlocks.clear();
        return goodBlocks;
	}

	@Override
	public void keyTyped(Component component, int keyCode, char c) {
		if (textField.getText().length() > 0) {
			slotBlocks.setComponents(searchBlock(textField.getText()));
		} else {
    		slotBlocks.setComponents(setupBlocks());
		}
	}
}
