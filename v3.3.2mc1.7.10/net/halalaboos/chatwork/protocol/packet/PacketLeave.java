package net.halalaboos.chatwork.protocol.packet;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import net.halalaboos.chatwork.protocol.data.Player;
import net.halalaboos.chatwork.protocol.data.User;
import net.halalaboos.chatwork.protocol.data.UserInfo;
import net.halalaboos.network.api.Handler;
import net.halalaboos.network.api.Packet;

public class PacketLeave implements Packet<User> {

	@Override
	public void write(Handler handler, User user,
			DataOutputStream outputStream) throws IOException {
		outputStream.writeInt((int) user.getId());
	}

	@Override
	public User read(Handler handler, DataInputStream inputStream)
			throws IOException {
		int id = inputStream.readInt();
		return new Player(id, new UserInfo(null, null, null, null, 0));
	}

}
