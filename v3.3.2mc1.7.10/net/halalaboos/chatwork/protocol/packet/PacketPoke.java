package net.halalaboos.chatwork.protocol.packet;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import net.halalaboos.network.api.Handler;
import net.halalaboos.network.api.Packet;

public class PacketPoke implements Packet<Integer> {

	@Override
	public Integer read(Handler handler, DataInputStream inputStream)
			throws IOException {
		return inputStream.readInt();
	}

	@Override
	public void write(Handler handler, Integer id,
			DataOutputStream outputStream) throws IOException {
		outputStream.writeInt(id);
	}

}