package net.halalaboos.chatwork.server;

import java.io.IOException;
import java.net.Socket;

import net.halalaboos.chatwork.protocol.data.UserInfo;
import net.halalaboos.chatwork.protocol.data.User;
import net.halalaboos.huzuni.util.Timer;
import net.halalaboos.network.tcp.server.ClientHandler;
import net.halalaboos.network.tcp.server.Server;

public class ChatworkClient extends ClientHandler implements User {
	
	private final int id;
	
	private boolean authenticated = false;
	
	private UserInfo info;
	
	private final Timer timer = new Timer();
		
	private int messageCount = 0;
	
	public ChatworkClient(Server<ChatworkClient> server, Socket socket)
			throws IOException {
		super(server, socket);
		this.id = ((ChatworkServer) server).nextId();
	}
	
	public boolean throttleMessaging(String message) {
		if (timer.getTimePassed() > 1000) {
			messageCount -= (int) (timer.getTimePassed() / 1000);
			if (messageCount < 0)
				messageCount = 0;
		} else
			messageCount++;
		timer.reset();
		if (messageCount >= 5)
			return true;
		else
			return false;
	}
	
	@Override
	public long getId() {
		return id;
	}
	
	public String getUsername() {
		return info.getUsername();
	}
	
	public String getLocation() {
		return info.getLocation();
	}

	public UserInfo getInfo() {
		return info;
	}

	public void setInfo(UserInfo info) {
		this.info = info;
	}

	public boolean isAuthenticated() {
		return authenticated;
	}
	
	public void setAuthenticated(boolean authenticated) {
		this.authenticated = authenticated;
	}
	
	public void sendMessage(String message) {
		this.write(0, message);
	}

	public String getIngameUsername() {
		return info.getIngameUsername();
	}
	
}
