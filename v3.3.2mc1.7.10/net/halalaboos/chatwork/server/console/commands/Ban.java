package net.halalaboos.chatwork.server.console.commands;

import net.halalaboos.chatwork.server.Bans;
import net.halalaboos.chatwork.server.ChatworkClient;
import net.halalaboos.chatwork.server.Permissions;
import net.halalaboos.chatwork.server.Server;
import net.halalaboos.chatwork.server.console.Command;
import net.halalaboos.chatwork.utils.StringUtils;

public class Ban implements Command {

	@Override
	public String getDescription() {
		return "Bans a client.";
	}

	@Override
	public String[] getAliases() {
		return new String[] { "ban" };
	}

	@Override
	public boolean isValid(String input) {
		return input.contains(" ");
	}

	@Override
	public void execute(ChatworkClient sender, String input, String[] args) {
		String username = args[0];
		String banReason = args.length > 1 ? StringUtils.getAfter(input, 2) : "";
		ChatworkClient client = Server.getUser(username);
		if (client != null) {
			client.sendMessage("You have been banned for '" + banReason + "'.");
			Bans.ban(client);
			client.close();
		}
	}

	@Override
	public void help(ChatworkClient sender) {
		sender.sendMessage("/ban <username>");
	}

	@Override
	public int getPermission() {
		return Permissions.ADMIN;
	}

}
