package net.halalaboos.chatwork.server.console.commands;

import net.halalaboos.chatwork.server.ChatworkClient;
import net.halalaboos.chatwork.server.Server;
import net.halalaboos.chatwork.server.console.Command;

public class Unmute implements Command {

	@Override
	public String getDescription() {
		return "Unmutes another user.";
	}

	@Override
	public String[] getAliases() {
		return new String[] { "unmute" };
	}

	@Override
	public boolean isValid(String input) {
		return input.contains(" ");
	}

	@Override
	public void execute(ChatworkClient sender, String input, String[] args) {
		ChatworkClient reciever = Server.getUser(args[0]);
		reciever.getInfo().setMuted(false);
		sender.sendMessage(reciever.getInfo().getUsername() + " is no longer muted.");
		reciever.sendMessage("You are no longer muted!");
	}

	@Override
	public void help(ChatworkClient sender) {
		sender.sendMessage("/unmute <username>");
	}

	@Override
	public int getPermission() {
		return 10;
	}

}

